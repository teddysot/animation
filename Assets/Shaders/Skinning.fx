//====================================================================================================
// Filename:	Skinning.fx
// Created by:	Peter Chan
// Description:	Shader performing hardware skinning.
//====================================================================================================

//====================================================================================================
// Constant Buffers
//====================================================================================================

cbuffer ConstantBuffer : register( b0 )
{
	matrix World;
	matrix WVP;
	matrix WVPLight;
	float3 ViewPosition;
	float3 LightDirection;
	float4 LightAmbient;
	float4 LightDiffuse;
	float4 LightSpecular;
	float4 MaterialAmbient;
	float4 MaterialDiffuse;
	float4 MaterialSpecular;
	float MaterialPower;
	float DisplacementScale;
};

cbuffer BoneConstantBuffer : register(b1)
{
	matrix boneTransforms[64];
}

static matrix Identity =
{
	1, 0, 0, 0,
	0, 1, 0 ,0,
	0, 0, 1, 0,
	0, 0, 0, 1
};

Texture2D diffuseMap : register(t0);
Texture2D specularMap : register(t1);
Texture2D normalMap : register(t2);
Texture2D displacementMap : register(t3);
Texture2D shadowMap : register(t4);
SamplerState textureSampler : register(s0);
SamplerState shadowSampler : register(s1);

//====================================================================================================
// Helpers
//====================================================================================================

matrix GetBoneTransform(int4 indices, float4 weights)
{
	if (length(weights) <= 0.0f)
		return Identity;

	matrix transform;
	transform = boneTransforms[indices[0]] * weights[0];
	transform += boneTransforms[indices[1]] * weights[1];
	transform += boneTransforms[indices[2]] * weights[2];
	transform += boneTransforms[indices[3]] * weights[3];
	return transform;
}

//====================================================================================================
// Structs
//====================================================================================================

struct VS_INPUT
{
	float4 position		: POSITION;
	float3 normal		: NORMAL;
	float3 tangent		: TANGENT;
	float4 color		: COLOR;
	float2 texCoord		: TEXCOORD;
	int4 blendIndices	: BLENDINDICES;
	float4 blendWeight	: BLENDWEIGHT;
};

//----------------------------------------------------------------------------------------------------

struct VS_OUTPUT
{
	float4 position		: SV_POSITION;
	float3 normal		: NORMAL;
	float3 tangent		: TANGENT;
	float3 binormal		: BINORMAL;
	float3 dirToLight	: TEXCOORD0;
	float3 dirToView	: TEXCOORD1;
	float2 texCoord		: TEXCOORD2;
	float4 shadowCoord	: TEXCOORD3;
	float3 worldPos		: TEXCOORD4;
};

//====================================================================================================
// Vertex Shader
//====================================================================================================

VS_OUTPUT VS(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0;

	matrix boneTransform = GetBoneTransform(input.blendIndices, input.blendWeight);
	float4 posBone = input.position;
	float4 posLocal = mul(posBone, boneTransform);
	float4 posWorld = mul(posLocal, World);
	float4 posProj = mul(posLocal, WVP);
	
	output.worldPos = posWorld.xyz;
	output.position = posProj;

	float3 normalBone = input.normal;
	float3 normalLocal = mul(float4(normalBone, 0.0f), boneTransform);

	float3 normal = mul(float4(normalLocal, 0.0f), World).xyz;
	float3 tangent = mul(float4(input.tangent, 0.0f), World).xyz;
	float3 binormal = cross(normal, tangent);
	
	//float3 dirToLight = -normalize(LightDirection.xyz);
	//float3 dirToView = normalize(ViewPosition.xyz - posWorld.xyz);

	output.normal = normal;
	output.tangent = tangent;
	output.binormal = binormal;
	//output.dirToLight = dirToLight;
	//output.dirToView = dirToView;
	output.texCoord = input.texCoord;
	output.shadowCoord = mul(output.position, WVPLight);
	
	return output;
}

//====================================================================================================
// Pixel Shader
//====================================================================================================

float4 PS(VS_OUTPUT input) : SV_Target
{
	// Sample normal from normal map
	//float4 normalColor = normalMap.Sample(textureSampler, input.texCoord);
	//float3 sampledNormal = float3((2.0f * normalColor.xy) - 1.0f, normalColor.z);

	// Transform to world space
	/*float3 t = normalize(input.tangent);
	float3 b = normalize(input.binormal);
	float3 n = normalize(input.normal);
	float3x3 tbn = float3x3(t, b, n);
	float3 normal = mul(sampledNormal, tbn);*/
	float3 normal = normalize(input.normal);

	float3 dirToLight = normalize(input.dirToLight);
	float3 dirToView = normalize(input.dirToView);
	
	// Ambient
	float4 ambient = LightAmbient * MaterialAmbient;
	
	// Diffuse
	float d = saturate(dot(dirToLight, normal));
	float4 diffuse = d * LightDiffuse * MaterialDiffuse;
		
	// Specular
	float3 h = normalize((dirToLight + dirToView) * 0.5f);
	float s = saturate(dot(h, normal));
	float4 specular = pow(s, MaterialPower) * LightSpecular * MaterialSpecular;

	float4 diffuseColor = diffuseMap.Sample(textureSampler, input.texCoord);
	float4 specularColor = specular; //specularMap.Sample(textureSampler, input.texCoord).rrrr;
	
	float4 finalColor = ((ambient + diffuse) * diffuseColor) * 0.35f;
	if (input.shadowCoord.w >= 0.0f)
	{
		float3 shadowCoord = input.shadowCoord.xyz / input.shadowCoord.w;
		shadowCoord.x = (shadowCoord.x + 1)* 0.5;
		shadowCoord.y = (shadowCoord.y - 1)*(-0.5);
	
		float pixelDepth = shadowCoord.z;
		float sampledDepth = shadowMap.Sample(shadowSampler, shadowCoord.xy).r + 0.001f;
		if (pixelDepth < sampledDepth)
		{
			finalColor = ((ambient + diffuse) * diffuseColor) + (specular * specularColor);
		}
	}
	return finalColor;
}
