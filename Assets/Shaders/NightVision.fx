//====================================================================================================
// Filename:	Texturing.fx
// Created by:	
// Description: Standard Shader for most situations.
//====================================================================================================

struct VSInput
{
	float4 position : POSITION;
	float2 textcoord : TEXTCOORD;
};

struct VSOutput
{
	float4 position : SV_POSITION;
	float2 textcoord : TEXTCOORD;
};

Texture2D RenderTargetMap : register(t0);
SamplerState Sampler : register(s0);

//====================================================================================================
// Vertex Shader
//====================================================================================================

VSOutput VS(VSInput input)
{
	VSOutput output = (VSOutput)0;

	output.position = input.position;
	output.textcoord = input.textcoord;

	return output;
}

//====================================================================================================
// Pixel Shader
//====================================================================================================

float4 PS(VSOutput input) : SV_Target
{
	float4 RenderTarget = RenderTargetMap.Sample(Sampler, input.textcoord);
	RenderTarget.r -= RenderTargetMap.Sample(Sampler, input.textcoord + (2 / 100)).r;
	RenderTarget.g += RenderTargetMap.Sample(Sampler, input.textcoord + (2 / 200)).g;
	RenderTarget.b -= RenderTargetMap.Sample(Sampler, input.textcoord + (2 / 300)).b;

	return RenderTarget;
}
