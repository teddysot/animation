//====================================================================================================
// Filename:	SimpleDraw.fx
// Created by:	
// Description: Shader for SimpleDraw.
//====================================================================================================

cbuffer ConstantBuffer : register(b0)
{
	matrix vp;
}

struct VSInput
{
	float4 position : POSITION;
	float4 color : COLOR;
};

struct VSOutput
{
	float4 position : SV_POSITION;
	float4 color : COLOR;
};

//====================================================================================================
// Vertex Shader
//====================================================================================================

VSOutput VS(VSInput input)
{
	VSOutput output = (VSOutput)0;
	output.position = mul(input.position, vp);
	output.color = input.color;
	return output;
}

//====================================================================================================
// Pixel Shader
//====================================================================================================

float4 PS(VSOutput input) : SV_Target
{
	return input.color;
}
