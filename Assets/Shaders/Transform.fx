//====================================================================================================
// Filename:	Transform.fx
// Created by:	
// Description: Shader that does basic transform.
//====================================================================================================

cbuffer ConstantBuffer : register(b0)
{
	matrix wvp;
}

struct VSInput
{
	float4 position : POSITION;
	float4 color : COLOR;
};

struct VSOutput
{
	float4 position : SV_POSITION;
	float4 color : COLOR;
};

//====================================================================================================
// Vertex Shader
//====================================================================================================

VSOutput VS(VSInput input)
{
	VSOutput output = (VSOutput)0;
	output.position = mul(input.position, wvp);
	output.color = input.color;
	return output;
}

//====================================================================================================
// Pixel Shader
//====================================================================================================

float4 PS(VSOutput input) : SV_Target
{
	return input.color;
}
