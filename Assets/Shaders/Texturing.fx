//====================================================================================================
// Filename:	Texturing.fx
// Created by:	
// Description: Shader that does texture mapping.
//====================================================================================================

cbuffer ConstantBuffer : register(b0)
{
	matrix wvp;
}

struct VSInput
{
	float4 position : POSITION;
	float2 textcoord : TEXTCOORD;
};

struct VSOutput
{
	float4 position : SV_POSITION;
	float2 textcoord : TEXTCOORD;
};

Texture2D DiffuseMap : register(t0);
SamplerState Sampler : register(s0);

//====================================================================================================
// Vertex Shader
//====================================================================================================

VSOutput VS(VSInput input)
{
	VSOutput output = (VSOutput)0;
	output.position = mul(input.position, wvp);
	output.textcoord = input.textcoord;
	return output;
}

//====================================================================================================
// Pixel Shader
//====================================================================================================

float4 PS(VSOutput input) : SV_Target
{
	return DiffuseMap.Sample(Sampler, input.textcoord);
	
}
