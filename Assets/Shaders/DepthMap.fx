//====================================================================================================
// Filename:	Texturing.fx
// Created by:	
// Description: Standard Shader for most situations.
//====================================================================================================

cbuffer ConstantBuffer : register(b0)
{
	matrix wvp;
	float displacementScale;
}

struct VSInput
{
	float4 position : POSITION;
	float3 normal : NORMAL;
	float3 tangent : TANGENT;
	float4 color : COLOR;
	float2 textcoord : TEXTCOORD;
};

struct VSOutput
{
	float4 position : SV_POSITION;
};

Texture2D DisplacementMap : register(t3);
SamplerState Sampler : register(s0);

//====================================================================================================
// Vertex Shader
//====================================================================================================

VSOutput VS(VSInput input)
{
	VSOutput output = (VSOutput)0;
	
	float displacement = DisplacementMap.SampleLevel(Sampler, input.textcoord, 0).x;
	float4 position = input.position + (float4(input.normal, 0.0f) * displacementScale * displacement);
	float4 posProj = mul(position, wvp);

	output.position = posProj;

	return output;
}
