//====================================================================================================
// Filename:	Texturing.fx
// Created by:	
// Description: Standard Shader for most situations.
//====================================================================================================

struct VSInput
{
	float4 position : POSITION;
	float2 textcoord : TEXTCOORD;
};

struct VSOutput
{
	float4 position : SV_POSITION;
	float2 textcoord : TEXTCOORD;
};

Texture2D RenderTargetMap : register(t0);
SamplerState Sampler : register(s0);

//====================================================================================================
// Vertex Shader
//====================================================================================================

VSOutput VS(VSInput input)
{
	VSOutput output = (VSOutput)0;

	output.position = input.position;
	output.textcoord = input.textcoord;

	return output;
}

//====================================================================================================
// Pixel Shader
//====================================================================================================

float4 PS(VSOutput input) : SV_Target
{
	float4 RenderTarget = RenderTargetMap.Sample(Sampler, input.textcoord);
	RenderTarget.rgb = (RenderTarget.r + RenderTarget.g + RenderTarget.b) / 3.0f;

	if (RenderTarget.r < 0.2 || RenderTarget.r > 0.9)
		RenderTarget.r = 0;
	else
		RenderTarget.r = 1.0f;

	if (RenderTarget.g < 0.2 || RenderTarget.g > 0.9)
		RenderTarget.g = 0;
	else
		RenderTarget.g = 1.0f;

	if (RenderTarget.b < 0.2 || RenderTarget.b > 0.9)
		RenderTarget.b = 0;
	else 
		RenderTarget.b = 1.0f;

	return RenderTarget;
}
