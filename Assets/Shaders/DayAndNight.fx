//====================================================================================================
// Filename:	Texturing.fx
// Created by:	
// Description: Shader that does texture mapping.
//====================================================================================================

cbuffer ConstantBuffer : register(b0)
{
	matrix wvp;
	matrix world;
	float3 viewPosition;	//world space
	float3 lightDirection;	//world space
	float4 lightAmbient;
	float4 lightDiffuse;
	float4 lightSpecular;
	float4 MaterialAmbient;
	float4 materialDiffuse;
	float4 MaterialSpecular;
	float power;
}

struct VSInput
{
	float4 position : POSITION;
	float3 normal : NORMAL;
	float3 tangent : TANGENT;
	float4 color : COLOR;
	float2 textcoord : TEXTCOORD;
};

struct VSOutput
{
	float4 position : SV_POSITION;
	float3 worldPos : TEXTCOORD0;
	float3 normal : TEXTCOORD1;
	float3 tangent : TEXTCOORD2;
	float2 textcoord : TEXTCOORD3;
};

Texture2D DiffuseMap : register(t0);
Texture2D SpecMap : register(t1);
Texture2D DarkMap : register(t2);
Texture2D NormalMap : register(t3);
Texture2D BumpMap : register(t4);
SamplerState Sampler : register(s0);

//====================================================================================================
// Vertex Shader
//====================================================================================================

VSOutput VS(VSInput input)
{
	VSOutput output = (VSOutput)0;
	output.position = mul(input.position, wvp);
	output.worldPos = mul(input.position, world).xyz;

	//we need to convert a 3d vector to a homogenous vector
	//using w = 0 means we dont care about translation
	//The result of a 1by4 * 4by4 multiplication gives a 1by4
	//and we only need the xyz values so we use HLSL's swizzling
	//syntax to extract just the x, y and z
	output.normal = mul(float4(input.normal, 0.0f), world).xyz;
	output.textcoord = input.textcoord;
	return output;
}

//====================================================================================================
// Pixel Shader
//====================================================================================================

float4 PS(VSOutput input) : SV_Target
{

	float bumpLevel = BumpMap.Sample(Sampler, input.textcoord).x;

	float4 normalTexture = NormalMap.Sample(Sampler, input.textcoord);
	float3 texNorm = normalTexture.xyz;


	//Re-normalize normals
	float3 normal = normalize(input.normal);
	float3 dirToLight = -lightDirection;
	float3 dirToView = normalize(viewPosition - input.worldPos);

	//ambient

	float4 ambientColor = lightAmbient * MaterialAmbient;

	//diffuse
	//float normalDiffuse = saturate(dot(dirToLight, texNorm));
	float diffuse = saturate(dot(dirToLight, dot(texNorm,normal) * bumpLevel));
	float4 diffuseColor = diffuse * lightDiffuse * materialDiffuse;

	//Specular
	float3 halfVector = normalize((dirToLight + dirToView) * 0.5f);
	float specular = saturate(dot(halfVector, dot(texNorm, normal) * bumpLevel));
	float4 specularColor = pow(specular, power) * lightSpecular * MaterialSpecular;

	//texture values
	 float4 diffuseTexture = DiffuseMap.Sample(Sampler, input.textcoord);
	 float4 darkDiffuseTexture = DarkMap.Sample(Sampler, input.textcoord);
	 
	 float specAmount = SpecMap.Sample(Sampler, input.textcoord).x;

	 float4 textureMiddle = darkDiffuseTexture + specular * (diffuseTexture - darkDiffuseTexture);

	 return (ambientColor + diffuseColor) * textureMiddle + (specularColor * specAmount);
}
