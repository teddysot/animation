//====================================================================================================
// Filename:	Starndard.fx
// Created by:	Peter Chan
// Description: Post processing shader.
//====================================================================================================

cbuffer ConstantBuffer : register (b0)
{
	int fnum;
}

Texture2D RenderTargetMap : register(t0);
SamplerState Sampler : register(s0);

struct VSInput // Vertex Shader
{
	// local space
	float4 position : POSITION;
	float2 texcoord	: TEXCOORD;
};

struct VSOutput
{
	float4 position : SV_POSITION; 	// System variable position
	float2 texcoord	: TEXCOORD;
};

//====================================================================================================
// Vertex Shader
//====================================================================================================

VSOutput VS(VSInput input)
{
	// HLSL Initialize it
	VSOutput output = (VSOutput)0;	

	output.position = input.position;
	output.texcoord = input.texcoord;
	return output;
}

//====================================================================================================
// Pixel Shader
//====================================================================================================

float4 PS(VSOutput input) : SV_Target
{
		float4 color = RenderTargetMap.Sample(Sampler, input.texcoord);
	if (fnum == 0)
	{
	}
	else if (fnum == 1)
	{
		input.texcoord.y = input.texcoord.y + (sin(input.texcoord.y * 100)*0.03);
		color = RenderTargetMap.Sample(Sampler, input.texcoord);
	}
	else if (fnum == 2)
	{
		color.r = color.r*sin(input.texcoord.x * 100) * 2;
		color.g = color.g*cos(input.texcoord.x * 150) * 2;
		color.b = color.b*sin(input.texcoord.x * 50) * 2;
	}
	else if (fnum == 3)
	{
		color -= RenderTargetMap.Sample(Sampler, input.texcoord.xy - 0.003)*2.7f;
		color += RenderTargetMap.Sample(Sampler, input.texcoord.xy + 0.003)*2.7f;
		color.rgb = (color.r + color.g + color.b) / 3.0f;
	}
	else if (fnum == 4)
	{
		color.rgb = (color.r + color.g + color.b) / 3.0f;
		if (color.r<0.2 || color.r>0.9) color.r = 0; else color.r = 1.0f;
		if (color.g<0.2 || color.g>0.9) color.g = 0; else color.g = 1.0f;
		if (color.b<0.2 || color.b>0.9) color.b = 0; else color.b = 1.0f;
	}
	else if (fnum == 5)
	{
		color.r -= RenderTargetMap.Sample(Sampler, input.texcoord + (10 / 100)).r;
		color.g += RenderTargetMap.Sample(Sampler, input.texcoord + (10 / 200)).g;
		color.b -= RenderTargetMap.Sample(Sampler, input.texcoord + (10 / 300)).b;
	}
	else if (fnum == 6)
	{
		color = RenderTargetMap.Sample(Sampler, input.texcoord) * 3;
	}
	else if (fnum == 7)
	{
		color = RenderTargetMap.Sample(Sampler, input.texcoord) * input.texcoord.x;
	}
	else if (fnum == 8)
	{
		color.b = color.b * 2;
	}
    else if (fnum == 9)
    {
        color = RenderTargetMap.Sample(Sampler, input.texcoord.xy) * .6f;
        color += RenderTargetMap.Sample(Sampler, input.texcoord.xy + (0.005)) * .1f;
		color += RenderTargetMap.Sample(Sampler, input.texcoord.xy - (0.005)) * .1f;
		color += RenderTargetMap.Sample(Sampler, float2(input.texcoord.x + 0.005, input.texcoord.y - 0.005)) * .1f;
		color += RenderTargetMap.Sample(Sampler, float2(input.texcoord.x - 0.005, input.texcoord.y + 0.005)) * .1f;
    }
	return color;
}
