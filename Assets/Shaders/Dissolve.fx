//====================================================================================================
// Filename:	Texturing.fx
// Created by:	
// Description: Standard Shader for most situations.
//====================================================================================================

struct VSInput
{
	float4 position		: POSITION;
	float3 normal		: NORMAL;
	float3 tangent		: TANGENT;
	float4 color		: COLOR;
	float2 textcoord	: TEXCOORD;
};

struct VSOutput
{
	float4 position : SV_POSITION;
	float2 textcoord : TEXTCOORD;
};

Texture2D RenderTargetMap : register(t0);
SamplerState Sampler : register(s0);

//====================================================================================================
// Vertex Shader
//====================================================================================================

VSOutput VS(VSInput input)
{
	VSOutput output = (VSOutput)0;

	output.position = input.position;
	output.textcoord = input.textcoord;

	return output;
}

//====================================================================================================
// Pixel Shader
//====================================================================================================

float4 PS(VSOutput input) : SV_Target
{
	input.textcoord.y = input.textcoord.y + (sin(input.textcoord.y * 100) * 0.008);
	float4 RenderTarget = RenderTargetMap.Sample(Sampler, input.textcoord.xy);

	return RenderTarget;
}
