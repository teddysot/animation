//====================================================================================================
// Filename:	DoSomething.fx
// Created by:	Peter Chan
// Description: Shader that does nothing.
//====================================================================================================

struct VSInput
{
	float4 position : POSITION;
	float4 color : COLOR;
};

struct VSOutput
{
	float4 position : SV_POSITION;
	float4 color : COLOR;
};

//====================================================================================================
// Vertex Shader
//====================================================================================================

VSOutput VS(VSInput input)
{
	VSOutput output = (VSOutput)0;
	output.position = input.position;
	output.color = input.color;
	return output;
}

//====================================================================================================
// Pixel Shader
//====================================================================================================

float4 PS(VSOutput input) : SV_Target
{
	return input.color;
}
