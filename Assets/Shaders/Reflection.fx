//====================================================================================================
// Filename:	Texturing.fx
// Created by:	
// Description: Standard Shader for most situations.
//====================================================================================================

struct VSInput
{
	float4 position : POSITION;
	float2 textcoord : TEXTCOORD;
};

struct VSOutput
{
	float4 position : SV_POSITION;
	float2 textcoord : TEXTCOORD;
};

Texture2D RenderTargetMap : register(t0);
SamplerState Sampler : register(s0);

//====================================================================================================
// Vertex Shader
//====================================================================================================

VSOutput VS(VSInput input)
{
	VSOutput output = (VSOutput)0;

	output.position = input.position;
	output.textcoord = input.textcoord;

	return output;
}

//====================================================================================================
// Pixel Shader
//====================================================================================================

float4 PS(VSOutput input) : SV_Target
{
	float4 RenderTarget = RenderTargetMap.Sample(Sampler, input.textcoord);
	
	RenderTarget -= RenderTargetMap.Sample(Sampler, input.textcoord.xy - 0.003) * 2.7f;
	RenderTarget += RenderTargetMap.Sample(Sampler, input.textcoord.xy + 0.003) * 2.7f;
	RenderTarget.rgb = (RenderTarget.r + RenderTarget.g + RenderTarget.b) / 3.0f;

	return RenderTarget;
}
