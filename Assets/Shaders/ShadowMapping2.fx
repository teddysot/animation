//====================================================================================================
// Filename:	ShadowMapping.fx
// Created by:	Peter Chan
// Description:	Shader performing shadow mapping.
//====================================================================================================

//====================================================================================================
// Constant Buffers
//====================================================================================================

cbuffer ConstantBuffer : register( b0 )
{
	matrix wvp;
	matrix world;
	matrix wvpLight;
	float3 viewPosition;
    float3 lightDirection;
    float4 lightAmbient;
    float4 lightDiffuse;
    float4 lightSpecular;
    float4 materialAmbient;
    float4 materialDiffuse;
    float4 materialSpecular;
	//vector MaterialEmissive;
	float power;
	float displacementScale;
};

Texture2D DiffuseMap		: register(t0);
Texture2D SpecularMap		: register(t1);
Texture2D NormalMap			: register(t2);
Texture2D DisplacementMap	: register(t3);
Texture2D ShadowMap			: register(t4);
SamplerState TextureSampler : register(s0);
SamplerState ShadowSampler	: register(s1);

//====================================================================================================
// Structs
//====================================================================================================

struct VS_INPUT
{
	float4 position	: POSITION;
	float3 normal	: NORMAL;
	float3 tangent	: TANGENT;
    float4 color	: COLOR;
	float2 texCoord	: TEXCOORD;
	int4 blendIndices	: BLENDINDICES;
	float4 blendWeight	: BLENDWEIGHT;
};

//----------------------------------------------------------------------------------------------------

struct VS_OUTPUT
{
	float4 position		: SV_POSITION;
    float3 worldPos		: TEXCOORD0;
    float3 normal		: NORMAL;
    float3 tangent		: TANGENT;
	float3 binormal		: BINORMAL;
	float3 dirToLight	: TEXCOORD1;
	float3 dirToView	: TEXCOORD2;
    float2 texCoord		: TEXCOORD3;
    float4 shadowCoord	: TEXCOORD4;
};

//====================================================================================================
// Vertex Shader
//====================================================================================================

VS_OUTPUT VS(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0;

	float displacement = DisplacementMap.SampleLevel(TextureSampler, input.texCoord, 0).x;
    float4 position = input.position + (float4(input.normal, 0.0f) * displacementScale * displacement);

	float4 posWorld = mul(position, world);
	float4 posProj = mul(position, wvp);
	
	float3 normal = mul(float4(input.normal, 0.0f), world).xyz;
	float3 tangent = mul(float4(input.tangent, 0.0f), world).xyz;
	float3 binormal = cross(normal, tangent);
	
	float3 dirToLight = -normalize(lightDirection.xyz);
	float3 dirToView = normalize(viewPosition.xyz - posWorld.xyz);

	output.position = posProj;
	output.normal = normal;
	output.tangent = tangent;
	output.binormal = binormal;
	output.dirToLight = dirToLight;
	output.dirToView = dirToView;
	output.texCoord = input.texCoord;
	output.shadowCoord = mul(position, wvpLight);
	
	return output;
}

//====================================================================================================
// Pixel Shader
//====================================================================================================

float4 PS(VS_OUTPUT input) : SV_Target
{
	// Sample normal from normal map
	float4 normalColor = NormalMap.Sample(TextureSampler, input.texCoord);
	float3 sampledNormal = float3((2.0f * normalColor.xy) - 1.0f, normalColor.z);

	// Transform to world space
	float3 t = normalize(input.tangent);
	float3 b = normalize(input.binormal);
	float3 n = normalize(input.normal);
	float3x3 tbn = float3x3(t, b, n);
	float3 normal = mul(sampledNormal, tbn);

	float3 dirToLight = normalize(input.dirToLight);
	float3 dirToView = normalize(input.dirToView);
	
	// Ambient
	float4 ambient = lightAmbient * materialAmbient;
	
	// Diffuse
	float d = saturate(dot(dirToLight, normal));
	float4 diffuse = d * lightDiffuse * materialDiffuse;
		
	// Specular
	float3 h = normalize((dirToLight + dirToView) * 0.5f);
	float s = saturate(dot(h, normal));
	float4 specular = pow(s, power) * lightSpecular * materialSpecular;

	float4 diffuseColor = DiffuseMap.Sample(TextureSampler, input.texCoord);
	float4 specularColor = SpecularMap.Sample(TextureSampler, input.texCoord).rrrr;
	
	float4 finalColor = ((ambient + diffuse) * diffuseColor) * 0.35f;
	if (input.shadowCoord.w >= 0.0f)
	{
		float3 shadowCoord = input.shadowCoord.xyz / input.shadowCoord.w;
        shadowCoord.x = (shadowCoord.x + 1) * 0.5;
        shadowCoord.y = (shadowCoord.y - 1) * (-0.5);
		float pixelDepth = shadowCoord.z;
        float sampledDepth = ShadowMap.Sample(ShadowSampler, shadowCoord.xy).r + 0.001f;
		if (pixelDepth < sampledDepth)
		{
			finalColor = ((ambient + diffuse) * diffuseColor) + (specular * specularColor);
		}
    }
	return finalColor;
}
