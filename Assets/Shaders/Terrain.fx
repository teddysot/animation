//====================================================================================================
// Filename:	Texturing.fx
// Created by:	
// Description: Standard Shader for most situations.
//====================================================================================================

cbuffer ConstantBuffer : register(b0)
{
	matrix wvp;
	matrix world;
	float3 viewPosition;	//world space
	float3 lightDirection;	//world space
	float4 lightAmbient;
	float4 lightDiffuse;
	float4 lightSpecular;
	float4 MaterialAmbient;
	float4 materialDiffuse;
	float4 MaterialSpecular;
	float power;
	float displacementScale;
}

struct VSInput
{
	float4 position : POSITION;
	float3 normal : NORMAL;
	float3 tangent : TANGENT;
	float4 color : COLOR;
	float2 textcoord : TEXTCOORD;
};

struct VSOutput
{
	float4 position : SV_POSITION;
	float3 worldPos : TEXTCOORD0;
	float3 normal : TEXTCOORD1;
	float3 tangent : TEXTCOORD2;
	float2 textcoord : TEXTCOORD3;
};

Texture2D DiffuseMap : register(t0);
SamplerState Sampler : register(s0);

//====================================================================================================
// Vertex Shader
//====================================================================================================

VSOutput VS(VSInput input)
{
	//we need to convert a 3d vector to a homogenous vector
	//using w = 0 means we dont care about translation
	//The result of a 1by4 * 4by4 multiplication gives a 1by4
	//and we only need the xyz values so we use HLSL's swizzling
	//syntax to extract just the x, y and z

	VSOutput output = (VSOutput)0;
	//output.worldPos = mul(input.position, world).xyz;
	output.normal = mul(float4(input.normal, 0.0f), world).xyz;
	output.tangent = mul(float4(input.tangent, 0.0f), world).xyz;

	output.position = mul(input.position, wvp);
	output.worldPos = mul(input.position, world).xyz;

	output.textcoord = input.textcoord;
	return output;
}

//====================================================================================================
// Pixel Shader
//====================================================================================================

float4 PS(VSOutput input) : SV_Target
{
	float3 normal = normalize(input.normal);
	float3 tangent = normalize(input.tangent);

	//Direction
	float3 dirToLight = -lightDirection;
	float3 dirToView = normalize(viewPosition - input.worldPos);

	//Ambient
	float4 ambientColor = lightAmbient * MaterialAmbient;

	//Diffuse
	float diffuse = saturate(dot(dirToLight, normal));
	float4 diffuseColor = diffuse * lightDiffuse * materialDiffuse;

	//texture values
	 float4 diffuseTexture = DiffuseMap.Sample(Sampler, input.textcoord);

	 return (ambientColor + diffuseColor) * diffuseTexture;
}
