//====================================================================================================
// Filename:	Texturing.fx
// Created by:	
// Description: Standard Shader for most situations.
//====================================================================================================

cbuffer ConstantBuffer : register(b0)
{
	matrix wvp;
}

struct VSInput
{
	float4 position : POSITION;
	float2 textcoord : TEXTCOORD3;
};

struct VSOutput
{
	float4 position : SV_POSITION;
	float2 textcoord : TEXTCOORD3;
};

TextureCube EnvironmentMap : register(t0);
SamplerState Sampler : register(s0);

//====================================================================================================
// Vertex Shader
//====================================================================================================

VSOutput VS(VSInput input)
{
	VSOutput output = (VSOutput)0;
	output.position = mul(input.position, wvp);
	output.textcoord = input.position;
	return output;
}

//====================================================================================================
// Pixel Shader
//====================================================================================================

float4 PS(VSOutput input) : SV_Target
{
	 return EnvironmentMap.Sample(Sampler, input.position);
}
