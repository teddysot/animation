#include "Precompiled.h"
#include "RenderTargetMesh.h"
#include "GraphicsSystem.h"
#include "VertexTypes.h"

using namespace Graphics;

Graphics::RenderTargetMesh::RenderTargetMesh()
{
}

Graphics::RenderTargetMesh::~RenderTargetMesh()
{
	SafeRelease(mVertexBuffer);
}

void Graphics::RenderTargetMesh::Initialize()
{
	// Left hand 1,2,0,3
	const Graphics::VertexPT vertices[] =
	{
		{Math::Vector3(-1.0f, 1.0f, 0.0f),		Math::Vector2(0.0f, 0.0f)},
		{Math::Vector3(1.0f, 1.0f, 0.0f),		Math::Vector2(1.0f, 0.0f)},
		{Math::Vector3(-1.0f, -1.0f, 0.0f),		Math::Vector2(0.0f, 1.0f)},
		{Math::Vector3(1.0f, -1.0f, 0.0f),		Math::Vector2(1.0f, 1.0f)},
	};
	const int vertexCount = sizeof(vertices) / sizeof(vertices[0]);
	mVertexSize = sizeof(Graphics::VertexPT);
	mVertexCount = vertexCount;
	mVertexCapacity = vertexCount;
	// Create and fill vertex buffer
	D3D11_BUFFER_DESC bd = {};
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = mVertexSize * vertexCount;  // memory size in VRAM
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;
	// ------------------------------Create Buffer----------------------------------------------------------
	// Output vertices into NDC space (normalized device coordinate) -1 to 1
	// Clock-wise order
	D3D11_SUBRESOURCE_DATA initData = {};
	initData.pSysMem = vertices;
	// "new" and delete on OnTermiate() (SafeRelease());
	// Allocate at VRAM
	Graphics::GraphicsSystem::Get()->GetDevice()->CreateBuffer(&bd, &initData, &mVertexBuffer);

}

void Graphics::RenderTargetMesh::Render()
{
	// Rendering goes here ...
	// Bind the input layout, vertex shader, and pixel shader
	ID3D11DeviceContext* context = Graphics::GraphicsSystem::Get()->GetContext();
	// Set vertex buffer
	UINT stride = mVertexSize;
	UINT offset = 0;
	context->IASetVertexBuffers(0, 1, &mVertexBuffer, &stride, &offset);
	context->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
	//Draw MeshBuffer (if you use vertex buffer only)
	context->Draw(mVertexCount, 0);
}
