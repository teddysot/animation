#include "Precompiled.h"
#include "GraphicsSystem.h"
#include "VertexShader.h"
#include <vector>
#include <VertexTypes.h>

using namespace Graphics;

namespace
{
	std::vector<D3D11_INPUT_ELEMENT_DESC> GetVertexDescription(uint32_t vertexFormat)
	{
		std::vector<D3D11_INPUT_ELEMENT_DESC> desc;

		if (vertexFormat & VF_POSITION) //has a position
		{
			desc.push_back({ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 });
		}
		if (vertexFormat & VF_NORMAL) //has a normal
		{
			desc.push_back({ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 });
		}
		if (vertexFormat & VF_Tangent) //has a Tangent
		{
			desc.push_back({ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 });
		}
		if (vertexFormat & VF_Color) //has a Color
		{
			desc.push_back({ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 });
		}
		if (vertexFormat & VF_UV) //has a UV
		{
			desc.push_back({ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 });
		}
		if (vertexFormat & VF_BINDEX)
		{
			desc.push_back({ "BLENDINDICES", 0, DXGI_FORMAT_R32G32B32A32_SINT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 });
		}
		if (vertexFormat & VF_BWEIGHT)
		{
			desc.push_back({ "BLENDWEIGHT", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 });
		}

		return desc;
	}
}

VertexShader::VertexShader()
	:mVertexShader(nullptr),
	mInputLayout(nullptr)
{
}
VertexShader::~VertexShader()
{

}

void VertexShader::Initialize(const wchar_t* fileName, uint32_t vertexFormat)
{


	DWORD shaderFlags = D3DCOMPILE_ENABLE_STRICTNESS | D3DCOMPILE_DEBUG;


	ID3DBlob* shaderBlob = nullptr;
	ID3DBlob* errorBlob = nullptr;
	HRESULT hr;
	hr = D3DCompileFromFile
	(
		fileName,
		nullptr,
		nullptr,
		"VS",
		"vs_5_0",
		shaderFlags,
		0,
		&shaderBlob,
		&errorBlob
	);

	ASSERT(SUCCEEDED(hr), "Failed to compile shader. ERROR: %s", (char*)errorBlob->GetBufferPointer());
	SafeRelease(errorBlob);

	Graphics::GraphicsSystem::Get()->GetDevice()->CreateVertexShader
	(
		shaderBlob->GetBufferPointer(),
		shaderBlob->GetBufferSize(),
		nullptr,
		&mVertexShader
	);

	std::vector<D3D11_INPUT_ELEMENT_DESC> vertexDesc = GetVertexDescription(vertexFormat);

	//creates the input layout based on the vertex layout, and the loaded shader
	Graphics::GraphicsSystem::Get()->GetDevice()->CreateInputLayout
	(
		vertexDesc.data(),
		vertexDesc.size(),
		shaderBlob->GetBufferPointer(),
		shaderBlob->GetBufferSize(),
		&mInputLayout
	);

	SafeRelease(shaderBlob);

}

void VertexShader::Terminate()
{
	SafeRelease(mVertexShader);
	SafeRelease(mInputLayout);
}


void VertexShader::Bind()
{
	Graphics::GraphicsSystem* gs = Graphics::GraphicsSystem::Get();
	ID3D11DeviceContext* context = gs->GetContext();
	context->IASetInputLayout(mInputLayout);
	context->VSSetShader(mVertexShader, nullptr, 0);
}
