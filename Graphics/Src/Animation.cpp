#include "Precompiled.h"
#include "Animation.h"

using namespace Graphics;

namespace
{
	inline Math::Matrix ComputeTransform(const Math::Vector3& pos, const Math::Vector3& scale, const Math::Quaternion& rot)
	{
		Math::Matrix translation = Math::Matrix::Translation(pos);
		Math::Matrix scaling = Math::Matrix::Scaling(scale);
		Math::Matrix rotation = Math::Matrix::RotationQuaternion(rot);
		return rotation * scaling * translation;
	}

	inline Math::Matrix ComputeTransform(const Keyframe& keyframe)
	{
		return ComputeTransform(keyframe.position, keyframe.scale, keyframe.rotation);
	}
}

Animation::Animation()
	: mLooping(false)
{

}

Animation::~Animation()
{

}

void Animation::AddKeyframe(Keyframe keyframe)
{
	if (mKeyframes.empty())
	{
		ASSERT(keyframe.time == 0.0f, "[Animation] Keyframe time is invalid.");
	}
	else
	{
		ASSERT(mKeyframes.back().time < keyframe.time, "[Animation] Keyframe time is invalid.");
	}
	mKeyframes.emplace_back(keyframe);
}

void Animation::SetLooping(bool loop)
{
	mLooping = loop;
}

Math::Matrix Animation::GetTransform(float time)
{
	ASSERT(time >= 0.0f, "[Animation] Invalid time value!");
	//ASSERT(!mKeyframes.empty(), "[Animation] Not enough keyframes!");

	if (mKeyframes.empty())
	{
		return Math::Matrix::Identity();
	}

	// Are we before the start frame of the animation?
	if (time <= mKeyframes.front().time)
	{
		return ComputeTransform(mKeyframes.front());
	}

	// Are we beyond the last frame of the animation?
	if (mLooping)
	{
		time = std::fmodf(time, mKeyframes.back().time);
	}
	if (time >= mKeyframes.back().time)
	{
		return ComputeTransform(mKeyframes.back());
	}

	// Otherwise, look for the two frames to blend
	size_t index = 0;
	for (; index < mKeyframes.size(); ++index)
	{
		if (mKeyframes[index].time > time)
		{
			break;
		}
	}

	const Keyframe& startFrame = mKeyframes[index - 1];
	const Keyframe& endFrame = mKeyframes[index];
	float lerpTime = (time - startFrame.time) / (endFrame.time - startFrame.time);

	const Math::Vector3& startPos = startFrame.position;
	const Math::Vector3& startScale = startFrame.scale;
	const Math::Quaternion& startRot = startFrame.rotation;

	const Math::Vector3& endPos = endFrame.position;
	const Math::Vector3& endScale = endFrame.scale;
	const Math::Quaternion& endRot = endFrame.rotation;

	const Math::Vector3 pos = Math::Lerp(startPos, endPos, lerpTime);
	const Math::Vector3 scale = Math::Lerp(startScale, endScale, lerpTime);
	const Math::Quaternion rot = Math::Slerp(startRot, endRot, lerpTime);
	return ComputeTransform(pos, scale, rot);
}

std::vector<Math::Matrix> Animation::GetWorldTransform(std::vector<Bone*> bones)
{
	// Transform matrix
	// Current bone matrix multiply by the transform of the parent

	for (int i = 0; i < bones.size(); ++i)
	{
		// Check bones position
		//if (bones[i]->transform != )
		// Check if it has parent or not
		// If no, push back position, transform
		// If yes, multiply by it's transform and it parent's transform and parent's height
		// Do while parent not null
	}

	// For child. multiply it's own transform, then the height and the transform of it's parent, then the transform and height of it's grandparent
	return std::vector<Math::Matrix>();
}
