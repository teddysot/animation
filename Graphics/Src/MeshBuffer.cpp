#include "Precompiled.h"
#include "Graphics\Inc\GraphicsSystem.h"
#include "MeshBuffer.h"


namespace Graphics
{

	MeshBuffer::MeshBuffer()
		:mVertexBuffer(nullptr)
		, mIndexBuffer(nullptr)
		, mIndexCount(0)
		, mVertexCapacity(0)
		, mVertexCount(0)
		, mVertexSize(0)
		, mTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST)
	{
	}

	MeshBuffer::~MeshBuffer()
	{

	}



	void MeshBuffer::Initialize(const void* vertices, uint32_t vertexSize, uint32_t vertexCount)
	{
		//this is the description for the buffer, so how large it is, and other attributes
		D3D11_BUFFER_DESC bd = {};

		mVertexSize = vertexSize;
		mVertexCount = vertexCount;
		mVertexCapacity = vertexCount;


		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = vertexSize * vertexCount; //Memory size in VRAM
		bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bd.CPUAccessFlags = 0;
		bd.MiscFlags = 0;


		D3D11_SUBRESOURCE_DATA initData = {};
		initData.pSysMem = vertices;

		Graphics::GraphicsSystem::Get()->GetDevice()->CreateBuffer(&bd, &initData, &mVertexBuffer);

	}

	void MeshBuffer::Initialize(
		const void* vertices,
		uint32_t vertexSize,
		uint32_t vertexCount,
		const uint32_t* indices,
		uint32_t indexCount)
	{

		mIndexCount = indexCount;

		Initialize(vertices, vertexSize, vertexCount);

		////////////////
		D3D11_BUFFER_DESC bd = {};


		ZeroMemory(&bd, sizeof(bd));
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(uint32_t) * indexCount; //Memory size in VRAM
		bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
		bd.CPUAccessFlags = 0;
		bd.MiscFlags = 0;

		D3D11_SUBRESOURCE_DATA initData = {};
		ZeroMemory(&initData, sizeof(initData));
		initData.pSysMem = indices;

		Graphics::GraphicsSystem::Get()->GetDevice()->CreateBuffer(&bd, &initData, &mIndexBuffer);

	}

	void MeshBuffer::Initialize(const Mesh& mesh)
	{
		Initialize(mesh.GetVertices(), mesh.GetVertexSize(), mesh.GetVertexCount(), mesh.GetIndices(), mesh.GetIndexCount());
	}

	void MeshBuffer::Initialize(const SkinnedMesh& mesh)
	{
		Initialize(mesh.GetVertices(), mesh.GetVertexSize(), mesh.GetVertexCount(), mesh.GetIndices(), mesh.GetIndexCount());
	}

	void MeshBuffer::InitializeDynamic(uint32_t vertexSize, uint32_t vertexCapacity)
	{

		mVertexSize = vertexSize;
		mVertexCount = 0;
		mVertexCapacity = vertexCapacity;


		D3D11_BUFFER_DESC bd = {};

		bd.Usage = D3D11_USAGE_DYNAMIC;
		bd.ByteWidth = vertexSize * vertexCapacity; //Memory size in VRAM
		bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		bd.MiscFlags = 0;


		Graphics::GraphicsSystem::Get()->GetDevice()->CreateBuffer(&bd, nullptr, &mVertexBuffer);

	}

	void MeshBuffer::Terminate()
	{
		SafeRelease(mVertexBuffer);
		SafeRelease(mIndexBuffer);
	}

	void MeshBuffer::SetVertexBuffer(const void* mVertices, uint32_t vertexCount)
	{
		ASSERT(mVertices != nullptr && mVertexCount >= 0, "[MeshBuffer] Invalid parameters.");
		ASSERT(vertexCount < mVertexCapacity, "[MeshBuffer] Too many vertices.");


		mVertexCount = vertexCount;


		if (vertexCount > 0)
		{
			ID3D11DeviceContext* context = GraphicsSystem::Get()->GetContext();
			D3D11_MAPPED_SUBRESOURCE resource;
			context->Map(mVertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &resource);
			memcpy(resource.pData, mVertices, vertexCount * mVertexSize);
			context->Unmap(mVertexBuffer, 0);
		}
	}

	void MeshBuffer::SetTopology(Topology topology)
	{
		switch (topology)
		{
		case Topology::PointList:
			mTopology = D3D_PRIMITIVE_TOPOLOGY_POINTLIST;
			break;
		case Topology::LineList:
			mTopology = D3D_PRIMITIVE_TOPOLOGY_LINELIST;
			break;
		case Topology::LineStrip:
			mTopology = D3D_PRIMITIVE_TOPOLOGY_LINESTRIP;
			break;
		case Topology::TriangleList:
			mTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
			break;
		case Topology::TriangleStrip:
			mTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP;
			break;
		default:
			ASSERT(false, "[MeshBuffer] Unexpected Topology")
				break;
		}
	}



	void MeshBuffer::Render()
	{
		//bind: conecting to the graphics pipeline,which creates a context(kinda an easy way to access the data)
		Graphics::GraphicsSystem* gs = Graphics::GraphicsSystem::Get();
		ID3D11DeviceContext* context = gs->GetContext();

		//adddaSDADAW

		//set vertex buffer
		UINT stride = mVertexSize;
		UINT offset = 0;
		context->IASetVertexBuffers(0, 1, &mVertexBuffer, &stride, &offset);


		//set priority Topology
		context->IASetPrimitiveTopology(mTopology);


		if (mIndexBuffer == nullptr)
		{
			context->Draw(mVertexCount, 0);
		}
		else
		{
			context->IASetIndexBuffer(mIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

			context->DrawIndexed(mIndexCount, 0, 0);
		}

		//if you use vertex buffer only
		////draw MeshBuffer
		//context->Draw(kVertexCount, 0);

		//for index, and vertex buffers
	}



}