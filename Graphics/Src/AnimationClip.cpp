#include "Precompiled.h"
#include "AnimationClip.h"

using namespace Graphics;

AnimationClip::AnimationClip()
	: mTicks(0.0f)
	, mDuration(0.0f)
	, mTicksPerSecond(0.0f)
	, mPlaying(false)
{
}

AnimationClip::~AnimationClip()
{
}

void AnimationClip::Play(bool looping)
{
	mPlaying = true;
	mLooping = looping;
}

void AnimationClip::Stop()
{
	mPlaying = false;
}

void AnimationClip::Reset()
{
	mTicks = 0.0f;
}

void AnimationClip::Update(float deltaTime)
{
	if (mPlaying)
	{
		mTicks = Math::Min(mTicks + (mTicksPerSecond * deltaTime), mDuration);
	}
}

std::vector<Math::Matrix> AnimationClip::GetTransform()
{
	std::vector<Math::Matrix> transform;

	for (uint32_t i = 0; i < mBoneAnimations.size(); ++i)
	{
		transform.push_back(mBoneAnimations[i].GetTransform(mTicks));
	}

	return transform;
}