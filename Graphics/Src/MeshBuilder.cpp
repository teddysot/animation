#include "Precompiled.h"

#include "MeshBuilder.h"
#include "VertexTypes.h"

using namespace Graphics;

void MeshBuilder::CreateSphere(float radius, uint32_t rings, uint32_t slices, Mesh& mesh)
{
	const float kSliceStep = Math::kTwoPi / slices;
	const float kRingStep = Math::kPi / (rings - 1);
	// Fill vertex data
	float uStep = 1.0f / slices;
	float vStep = 1.0f / rings;
	uint32_t index = 0;
	for (uint32_t j = 0; j < rings; ++j)
	{
		const float phi = j * kRingStep;
		for (uint32_t i = 0; i <= slices; ++i)
		{
			const float theta = i * kSliceStep;
			const float y = cos(phi);
			const float r = sqrt(1.0f - (y * y));
			const float s = sin(theta);
			const float c = cos(theta);
			const float x = r * c;
			const float z = r * s;
			Math::Vector3 p(x, y, z);
			Math::Vector3 n(x, y, z);
			Math::Vector3 t(-s, 0.0f, c);
			Math::Vector2 uv(i *(1.0f / slices), j*(1.0f / rings));
			mesh.AddVertex(p * radius, uv, n, t);
			mesh.GetVertex().color = Math::Vector4(1.0f, 1.0f, 1.0f, 1.0f);
			mesh.GetVertex().tangent = t;
			mesh.GetVertex().normal = n;

		}
	}
	// Fill index data
	for (uint32_t j = 0; j < rings - 1; ++j)
	{
		for (uint32_t i = 0; i <= slices; ++i)
		{
			const uint32_t a = i % (slices + 1);
			const uint32_t b = (i + 1) % (slices + 1);
			const uint32_t c = j * (slices + 1);
			const uint32_t d = (j + 1) * (slices + 1);
			mesh.AddIndices(a + c);
			mesh.AddIndices(b + c);
			mesh.AddIndices(a + d);
			mesh.AddIndices(b + c);
			mesh.AddIndices(b + d);
			mesh.AddIndices(a + d);
		}
	}

}

void MeshBuilder::CreateCylinder(float radius, float height, uint32_t slices, uint32_t rings, Mesh & mesh)
{
	mesh.Instantiate(slices*rings * 6);

	const float kSliceStep = Math::kTwoPi / slices;
	const float kRingStep = 2.0f / (rings - 1);

	// Fill vertex data
	float uStep = 1.0f / slices;
	float vStep = 1.0f / rings;
	for (uint32_t j = 0; j < rings; ++j)
	{
		for (uint32_t i = 0; i <= slices; ++i)
		{
			const float theta = i * kSliceStep;
			const float y = 1.0f - (j * kRingStep);
			const float s = sin(theta);
			const float c = cos(theta);
			const float x = radius * c;
			const float z = radius * s;
			Math::Vector3 p(x, y, z);
			Math::Vector3 n(x, y, z);
			Math::Vector3 t(-s, 0.0f, c);
			Math::Vector2 uv(i *(1.0f / slices), j*(1.0f / rings));
			mesh.AddVertex(Math::Vector3(x, y, z) * radius, uv, n, t);
			mesh.GetVertex().color = Math::Vector4(1.0f, 1.0f, 1.0f, 1.0f);
		}
	}

	// Fill index data
	for (uint32_t j = 0; j < rings - 1; ++j)
	{
		for (uint32_t i = 0; i < slices; ++i)
		{
			const uint32_t a = i % (slices + 1);
			const uint32_t b = (i + 1) % (slices + 1);
			const uint32_t c = j * (slices + 1);
			const uint32_t d = (j + 1) * (slices + 1);
			mesh.AddIndices(a + c);
			mesh.AddIndices(b + c);
			mesh.AddIndices(a + d);
			mesh.AddIndices(b + c);
			mesh.AddIndices(b + d);
			mesh.AddIndices(a + d);
		}
	}
}

void MeshBuilder::CreateSkybox(float size, Mesh& mesh)
{
	Math::Vector3 topLeft = Math::Vector3((-size / 2), (size / 2), (size / 2));
	Math::Vector3 bottomRight = Math::Vector3((size / 2), (-size / 2), (-size / 2));


	mesh.AddVertex(topLeft, Math::Vector2(0, (float)(1.0f / 3.0f)));//0
	mesh.AddVertex(topLeft, Math::Vector2(1, (float)(1.0f / 3.0f)));//1

	mesh.AddVertex(topLeft + Math::Vector3(size, 0.0f, 0.0f), Math::Vector2(0.25f, (float)(1.0f / 3.0f)));//2

	mesh.AddVertex(topLeft + Math::Vector3(size, -size, 0.0f), Math::Vector2(0.25f, (float)(2.0f / 3.0f)));//3

	mesh.AddVertex(topLeft + Math::Vector3(0.0f, -size, 0.0f), Math::Vector2(0, (float)(2.0f / 3.0f)));//4
	mesh.AddVertex(topLeft + Math::Vector3(0.0f, -size, 0.0f), Math::Vector2(1, (float)(2.0f / 3.0f)));//5



	mesh.AddVertex(bottomRight + Math::Vector3(-size, size, 0.0f), Math::Vector2(0.75f, (float)(1.0f / 3.0f)));//6

	mesh.AddVertex(bottomRight + Math::Vector3(0.0f, size, 0.0f), Math::Vector2(0.50f, (float)(1.0f / 3.0f)));//7

	mesh.AddVertex(bottomRight, Math::Vector2(0.50f, (float)(2.0f / 3.0f)));//8

	mesh.AddVertex(bottomRight + Math::Vector3(-size, 0.0f, 0.0f), Math::Vector2(0.75f, (float)(2.0f / 3.0f)));//9

	mesh.AddVertex(topLeft + Math::Vector3(-0.1f, -0.1f, 0.1f), Math::Vector2(0.25f, 0.0f));//10
	mesh.AddVertex(topLeft + Math::Vector3(size + 0.1f, -0.1f, 0.1f), Math::Vector2(0.25f, (float)(1.0f / 3.0f)));//11

	mesh.AddVertex(bottomRight + Math::Vector3(0.1f, size, -0.1f), Math::Vector2(0.50f, (float)(1.0f / 3.0f)));//12
	mesh.AddVertex(bottomRight + Math::Vector3(-size, size, -0.1f), Math::Vector2(0.50f, 0.0f));//13

	mesh.AddVertex(topLeft + Math::Vector3(-0.1f, -size, 0.1f), Math::Vector2(0.25f, 1.0f));//14
	mesh.AddVertex(topLeft + Math::Vector3(size, -size, 0.1f), Math::Vector2(0.25f, (float)(1.0f / 3.0f)));//15

	mesh.AddVertex(bottomRight + Math::Vector3(0.1f, 0.0f, 0.0f), Math::Vector2(0.50f, (float)(1.0f / 3.0f)));//16
	mesh.AddVertex(bottomRight + Math::Vector3(-size, 0.0f, 0.0f), Math::Vector2(0.50f, 0.0f));//17

	mesh.AddIndices(4);// 402 423 327 378 876 869 961 915
	mesh.AddIndices(0);
	mesh.AddIndices(2);

	mesh.AddIndices(4);
	mesh.AddIndices(2);
	mesh.AddIndices(3);

	mesh.AddIndices(3);
	mesh.AddIndices(2);
	mesh.AddIndices(7);

	mesh.AddIndices(3);
	mesh.AddIndices(7);
	mesh.AddIndices(8);

	mesh.AddIndices(8);
	mesh.AddIndices(7);
	mesh.AddIndices(6);

	mesh.AddIndices(8);
	mesh.AddIndices(6);
	mesh.AddIndices(9);

	mesh.AddIndices(9);
	mesh.AddIndices(6);
	mesh.AddIndices(1);

	mesh.AddIndices(9);
	mesh.AddIndices(1);
	mesh.AddIndices(5);

	mesh.AddIndices(4);
	mesh.AddIndices(3);
	mesh.AddIndices(8);

	mesh.AddIndices(5);
	mesh.AddIndices(8);
	mesh.AddIndices(9);

	mesh.AddIndices(10);
	mesh.AddIndices(13);
	mesh.AddIndices(12);

	mesh.AddIndices(10);
	mesh.AddIndices(12);
	mesh.AddIndices(11);

	mesh.AddIndices(14);
	mesh.AddIndices(17);
	mesh.AddIndices(16);

	mesh.AddIndices(14);
	mesh.AddIndices(16);
	mesh.AddIndices(15);

}

void MeshBuilder::CreatePlane(const Math::Vector2& size, Mesh& mesh)
{
	float x = size.x;
	float y = size.y;
	float halfX = size.x / 2;
	float halfY = size.y / 2;

	Math::Vector3 bottomLeft(-halfX, 0, -halfY);
	Math::Vector3 TopLeft(-halfX, 0, halfY);
	Math::Vector3 bottomRight(halfX, 0, -halfY);
	Math::Vector3 TopRight(halfX,0,halfY);

	mesh.AddVertex(bottomLeft, Math::Vector2(0, 1),Math::Vector3::YAxis(), Math::Vector3::XAxis());
	mesh.AddVertex(TopLeft, Math::Vector2(0, 0), Math::Vector3::YAxis(), Math::Vector3::XAxis());
	mesh.AddVertex(TopRight, Math::Vector2(1, 0), Math::Vector3::YAxis(), Math::Vector3::XAxis());
	mesh.AddVertex(bottomRight, Math::Vector2(1, 1), Math::Vector3::YAxis(), Math::Vector3::XAxis());

	mesh.AddIndices(0);
	mesh.AddIndices(1);
	mesh.AddIndices(2);
	mesh.AddIndices(0);
	mesh.AddIndices(2);
	mesh.AddIndices(3);
}

void MeshBuilder::CreateCube(Mesh& mesh)
{
	mesh.Instantiate(100);
	// left 0
	mesh.AddVertex(Math::Vector3(-1.0f, -1.0f, -1.0f), Math::Vector2(0.0f, 0.0f));
	mesh.AddVertex(Math::Vector3(-1.0f, 1.0f, -1.0f), Math::Vector2(0.0f, 1.0f));
	mesh.AddVertex(Math::Vector3(+1.0f, 1.0f, -1.0f), Math::Vector2(1.0f, 1.0f));
	mesh.AddVertex(Math::Vector3(+1.0f, -1.0f, -1.0f), Math::Vector2(1.0f, 0.0f));
	mesh.AddVertex(Math::Vector3(+1.0f, -1.0f, +1.0f), Math::Vector2(0.0f, 0.0f));
	mesh.AddVertex(Math::Vector3(+1.0f, 1.0f, +1.0f), Math::Vector2(0.0f, 1.0f));
	mesh.AddVertex(Math::Vector3(-1.0f, 1.0f, +1.0f), Math::Vector2(1.0f, 1.0f));
	mesh.AddVertex(Math::Vector3(-1.0f, -1.0f, +1.0f), Math::Vector2(1.0f, 0.0f));

	// bottom
	mesh.AddIndices(0);
	mesh.AddIndices(3);
	mesh.AddIndices(4);

	mesh.AddIndices(0);
	mesh.AddIndices(4);
	mesh.AddIndices(7);


	mesh.AddIndices(0);
	mesh.AddIndices(1);
	mesh.AddIndices(2);

	mesh.AddIndices(0);
	mesh.AddIndices(2);
	mesh.AddIndices(3);

	// front
	mesh.AddIndices(3);
	mesh.AddIndices(2);
	mesh.AddIndices(5);

	mesh.AddIndices(3);
	mesh.AddIndices(5);
	mesh.AddIndices(4);

	// right
	mesh.AddIndices(4);
	mesh.AddIndices(5);
	mesh.AddIndices(6);

	mesh.AddIndices(4);
	mesh.AddIndices(6);
	mesh.AddIndices(7);

	// back
	mesh.AddIndices(7);
	mesh.AddIndices(6);
	mesh.AddIndices(1);

	mesh.AddIndices(7);
	mesh.AddIndices(1);
	mesh.AddIndices(0);

	// left
	mesh.AddIndices(1);
	mesh.AddIndices(6);
	mesh.AddIndices(5);

	mesh.AddIndices(1);
	mesh.AddIndices(5);
	mesh.AddIndices(2);
}

void MeshBuilder::CreateQuad(Mesh& mesh)
{
	mesh.AddVertex(Math::Vector3(-1, -1, 0), Math::Vector2(0, 1));
	mesh.AddVertex(Math::Vector3(-1, 1, 0), Math::Vector2(0, 0));
	mesh.AddVertex(Math::Vector3(1, 1, 0), Math::Vector2(1, 0));
	mesh.AddVertex(Math::Vector3(1, -1, 0), Math::Vector2(1, 1));

	mesh.AddIndices(0);
	mesh.AddIndices(1);
	mesh.AddIndices(2);

	mesh.AddIndices(0);
	mesh.AddIndices(2);
	mesh.AddIndices(3);
}