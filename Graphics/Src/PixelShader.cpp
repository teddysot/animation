#include "Precompiled.h"
#include "GraphicsSystem.h"
#include "PixelShader.h"

namespace Graphics
{

	PixelShader::PixelShader() 
		:mPixelShader(nullptr)
	{

	}
	PixelShader::~PixelShader()
	{

	}

	void PixelShader::Initialize(const wchar_t* fileName)
	{
		DWORD shaderFlags = D3DCOMPILE_ENABLE_STRICTNESS | D3DCOMPILE_DEBUG;


		ID3DBlob* shaderBlob = nullptr;
		ID3DBlob* errorBlob = nullptr;
		HRESULT hr;

		hr = D3DCompileFromFile
		(
			fileName,
			nullptr,
			nullptr,
			"PS",
			"ps_5_0",
			shaderFlags,
			0,
			&shaderBlob,
			&errorBlob
		);

		ASSERT(SUCCEEDED(hr), "Failed to compile pixel shader. ERROR: %s", (char*)errorBlob->GetBufferPointer());
		SafeRelease(errorBlob);

		Graphics::GraphicsSystem::Get()->GetDevice()->CreatePixelShader
		(
			shaderBlob->GetBufferPointer(),
			shaderBlob->GetBufferSize(),
			nullptr,
			&mPixelShader
		);

		SafeRelease(shaderBlob);
	}

	void PixelShader::Terminate()
	{
		SafeRelease(mPixelShader);
	}

	void PixelShader::Bind() 
	{
		Graphics::GraphicsSystem* gs = Graphics::GraphicsSystem::Get();
		ID3D11DeviceContext* context = gs->GetContext();
		context->PSSetShader(mPixelShader, nullptr, 0);
	}


}