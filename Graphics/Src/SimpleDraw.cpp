#include "Precompiled.h"
#include "SimpleDraw.h"

#include "ConstantBuffer.h"
#include "MeshBuffer.h"
#include "PixelShader.h"
#include "VertexShader.h"
#include "VertexTypes.h"

using namespace Graphics;

namespace
{
	class SimpleDrawImpl
	{
	public:
		SimpleDrawImpl()
			: mVertices(nullptr)
			, mVertexCount(0)
			, mCapacity(0)
		{}

		~SimpleDrawImpl()
		{
			ASSERT(mVertices == nullptr, "[SimpleDraw] Memory not cleaned up.");
		}

		void Initialize(uint32_t capacity)
		{
			mVertices = new VertexPC[capacity];
			mCapacity = capacity;

			mConstantBuffer.Initialize();
			mVertexShader.Initialize(L"../Assets/Shaders/SimpleDraw.fx", VertexPC::Format);
			mPixelShader.Initialize(L"../Assets/Shaders/SimpleDraw.fx");

			mMeshBuffer.InitializeDynamic(sizeof(VertexPC), mCapacity);
			mMeshBuffer.SetTopology(MeshBuffer::Topology::LineList);
		}

		void Terminate()
		{
			mMeshBuffer.Terminate();

			mPixelShader.Terminate();
			mVertexShader.Terminate();
			mConstantBuffer.Terminate();

			SafeDeleteArray(mVertices);
		}

		void DrawLine(const Math::Vector3& p0, const Math::Vector3& p1, const Math::Vector4& color)
		{
			if (mVertexCount + 2 < mCapacity)
			{
				mVertices[mVertexCount++] = { p0, color };
				mVertices[mVertexCount++] = { p1, color };
			}
		}

		void DrawAABB(const Math::AABB& aabb, const Math::Vector4& color)
		{
			// Check if we have enough space
			if (mVertexCount + 24 <= mCapacity)
			{
				float minX = aabb.center.x - aabb.extend.x;
				float minY = aabb.center.y - aabb.extend.y;
				float minZ = aabb.center.z - aabb.extend.z;
				float maxX = aabb.center.x + aabb.extend.x;
				float maxY = aabb.center.y + aabb.extend.y;
				float maxZ = aabb.center.z + aabb.extend.z;

				// Draw lines
				mVertices[mVertexCount++] = { Math::Vector3(minX, minY, minZ), color };
				mVertices[mVertexCount++] = { Math::Vector3(minX, minY, maxZ), color };

				mVertices[mVertexCount++] = { Math::Vector3(minX, minY, maxZ), color };
				mVertices[mVertexCount++] = { Math::Vector3(maxX, minY, maxZ), color };

				mVertices[mVertexCount++] = { Math::Vector3(maxX, minY, maxZ), color };
				mVertices[mVertexCount++] = { Math::Vector3(maxX, minY, minZ), color };

				mVertices[mVertexCount++] = { Math::Vector3(maxX, minY, minZ), color };
				mVertices[mVertexCount++] = { Math::Vector3(minX, minY, minZ), color };

				mVertices[mVertexCount++] = { Math::Vector3(minX, minY, minZ), color };
				mVertices[mVertexCount++] = { Math::Vector3(minX, maxY, minZ), color };

				mVertices[mVertexCount++] = { Math::Vector3(minX, minY, maxZ), color };
				mVertices[mVertexCount++] = { Math::Vector3(minX, maxY, maxZ), color };

				mVertices[mVertexCount++] = { Math::Vector3(maxX, minY, maxZ), color };
				mVertices[mVertexCount++] = { Math::Vector3(maxX, maxY, maxZ), color };

				mVertices[mVertexCount++] = { Math::Vector3(maxX, minY, minZ), color };
				mVertices[mVertexCount++] = { Math::Vector3(maxX, maxY, minZ), color };

				mVertices[mVertexCount++] = { Math::Vector3(minX, maxY, minZ), color };
				mVertices[mVertexCount++] = { Math::Vector3(minX, maxY, maxZ), color };

				mVertices[mVertexCount++] = { Math::Vector3(minX, maxY, maxZ), color };
				mVertices[mVertexCount++] = { Math::Vector3(maxX, maxY, maxZ), color };

				mVertices[mVertexCount++] = { Math::Vector3(maxX, maxY, maxZ), color };
				mVertices[mVertexCount++] = { Math::Vector3(maxX, maxY, minZ), color };

				mVertices[mVertexCount++] = { Math::Vector3(maxX, maxY, minZ), color };
				mVertices[mVertexCount++] = { Math::Vector3(minX, maxY, minZ), color };
			}

			ASSERT(mVertexCount < mCapacity, "[SimpleDraw] Too many vertices!");
		}

		void DrawOBB(const Math::OBB& obb, const Math::Vector4& color)
		{
			Math::Matrix matTrans = Math::Matrix::Translation(obb.center);
			Math::Matrix matRot = Math::Matrix::RotationQuaternion(obb.rot);
			Math::Matrix matScale = Math::Matrix::Scaling(obb.extend);
			Math::Matrix toWorld = matScale * matRot * matTrans;

			Math::Vector3 points[] =
			{
				Math::Vector3(-1.0f, -1.0f, -1.0f),
				Math::Vector3(-1.0f,  1.0f, -1.0f),
				Math::Vector3(1.0f,  1.0f, -1.0f),
				Math::Vector3(1.0f, -1.0f, -1.0f),
				Math::Vector3(-1.0f, -1.0f,  1.0f),
				Math::Vector3(-1.0f,  1.0f,  1.0f),
				Math::Vector3(1.0f,  1.0f,  1.0f),
				Math::Vector3(1.0f, -1.0f,  1.0f)
			};

			for (uint32_t i = 0; i < 8; ++i)
			{
				points[i] = Math::TransformCoord(points[i], toWorld);
			}

			DrawLine(points[0], points[1], color);
			DrawLine(points[1], points[2], color);
			DrawLine(points[2], points[3], color);
			DrawLine(points[3], points[0], color);

			DrawLine(points[0], points[4], color);
			DrawLine(points[1], points[5], color);
			DrawLine(points[2], points[6], color);
			DrawLine(points[3], points[7], color);

			DrawLine(points[4], points[5], color);
			DrawLine(points[5], points[6], color);
			DrawLine(points[6], points[7], color);
			DrawLine(points[7], points[4], color);
		}

		void DrawPlane(const Math::Plane& plane, const Math::Vector3& referencePoint, float size, float spacing, const Math::Vector4& color)
		{
			float distToPlane = Math::Dot(referencePoint, plane.n) - plane.d;
			Math::Vector3 pointOnPlane = referencePoint - (plane.n * distToPlane);
			Math::Vector3 referenceDir = Math::Abs(Math::Dot(plane.n, Math::Vector3::XAxis())) < 0.95f ? Math::Vector3::XAxis() : Math::Vector3::YAxis();
			Math::Vector3 side = Math::Normalize(Math::Cross(plane.n, referenceDir));
			Math::Vector3 forward = Math::Normalize(Math::Cross(plane.n, side));
			int count = (int)(size / spacing);
			float offset = size * 0.5f;

			for (int i = 0; i <= count; ++i)
			{
				Math::Vector3 center = pointOnPlane - (forward * offset) + (forward * spacing * (float)i);
				Math::Vector3 start = center - (side * offset);
				Math::Vector3 end = center + (side * offset);
				DrawLine(start, end, color);
			}

			for (int i = 0; i <= count; ++i)
			{
				Math::Vector3 center = pointOnPlane - (side * offset) + (side * spacing * (float)i);
				Math::Vector3 start = center - (forward * offset);
				Math::Vector3 end = center + (forward * offset);
				DrawLine(start, end, color);
			}
		}

		void DrawSphere(const Math::Sphere& sphere, const Math::Vector4& color, uint32_t slices, uint32_t rings)
		{
			const float x = sphere.center.x;
			const float y = sphere.center.y;
			const float z = sphere.center.z;
			const float radius = sphere.radius;

			const uint32_t kSlices = Math::Max(3u, slices);
			const uint32_t kRings = Math::Max(2u, rings);
			const uint32_t kLines = (4 * kSlices * kRings) - (2 * kSlices);

			// Check if we have enough space
			if (mVertexCount + kLines <= mCapacity)
			{
				// Draw lines
				const float kTheta = Math::kPi / (float)kRings;
				const float kPhi = Math::kTwoPi / (float)kSlices;
				for (uint32_t j = 0; j < kSlices; ++j)
				{
					for (uint32_t i = 0; i < kRings; ++i)
					{
						const float a = i * kTheta;
						const float b = a + kTheta;
						const float ay = radius * cos(a);
						const float by = radius * cos(b);

						const float theta = j * kPhi;
						const float phi = theta + kPhi;

						const float ar = sqrt(radius * radius - ay * ay);
						const float br = sqrt(radius * radius - by * by);

						const float x0 = x + (ar * sin(theta));
						const float y0 = y + (ay);
						const float z0 = z + (ar * cos(theta));

						const float x1 = x + (br * sin(theta));
						const float y1 = y + (by);
						const float z1 = z + (br * cos(theta));

						const float x2 = x + (br * sin(phi));
						const float y2 = y + (by);
						const float z2 = z + (br * cos(phi));

						mVertices[mVertexCount++] = { Math::Vector3(x0, y0, z0), color };
						mVertices[mVertexCount++] = { Math::Vector3(x1, y1, z1), color };

						if (i < kRings - 1)
						{
							mVertices[mVertexCount++] = { Math::Vector3(x1, y1, z1), color };
							mVertices[mVertexCount++] = { Math::Vector3(x2, y2, z2), color };
						}
					}
				}
			}

			ASSERT(mVertexCount < mCapacity, "[SimpleDraw] Too many vertices!");
		}

		void DrawTransform(const Math::Matrix& transform)
		{
			Math::Vector3 position = Math::GetTranslation(transform);
			Math::Vector3 right = Math::GetRight(transform);
			Math::Vector3 up = Math::GetUp(transform);
			Math::Vector3 forward = Math::GetForward(transform);
			DrawLine(position, position + right, Math::Vector4::Red());
			DrawLine(position, position + up, Math::Vector4::Green());
			DrawLine(position, position + forward, Math::Vector4::Blue());
		}

		void Flush(const Math::Matrix& matViewProj)
		{
			ConstantData data;
			data.vp = Math::Transpose(matViewProj);
			mConstantBuffer.Set(data);
			mConstantBuffer.BindVS();

			mVertexShader.Bind();
			mPixelShader.Bind();

			mMeshBuffer.SetVertexBuffer(mVertices, mVertexCount);
			mMeshBuffer.Render();

			// Rewind the vertex data
			mVertexCount = 0;
		}

	private:
		struct ConstantData
		{
			Math::Matrix vp;
		};

		TypedConstantBuffer<ConstantData> mConstantBuffer;
		VertexShader mVertexShader;
		PixelShader mPixelShader;
		MeshBuffer mMeshBuffer;

		VertexPC* mVertices;
		uint32_t mVertexCount;
		uint32_t mCapacity;
	};

	SimpleDrawImpl* sSimpleDraw = nullptr;
}

void SimpleDraw::StaticInitialize(uint32_t capacity)
{
	ASSERT(sSimpleDraw == nullptr, "[SimpleDraw] Already initialized!");
	sSimpleDraw = new SimpleDrawImpl();
	sSimpleDraw->Initialize(capacity);
}

void SimpleDraw::StaticTerminate()
{
	ASSERT(sSimpleDraw != nullptr, "[SimpleDraw] Not initialized!");
	sSimpleDraw->Terminate();
	SafeDelete(sSimpleDraw);
}

void SimpleDraw::DrawLine(const Math::Vector3& p0, const Math::Vector3& p1, const Math::Vector4& color)
{
	ASSERT(sSimpleDraw != nullptr, "[SimpleDraw] Not initialized!");
	sSimpleDraw->DrawLine(p0, p1, color);
}

void SimpleDraw::DrawLine(float x0, float y0, float z0, float x1, float y1, float z1, const Math::Vector4& color)
{
	DrawLine(Math::Vector3(x0, y0, z0), Math::Vector3(x1, y1, z1), color);
}

void SimpleDraw::DrawAABB(const Math::AABB& aabb, const Math::Vector4& color)
{
	ASSERT(sSimpleDraw != nullptr, "[SimpleDraw] Not initialized.");
	sSimpleDraw->DrawAABB(aabb, color);
}

void SimpleDraw::DrawAABB(const Math::Vector3& min, const Math::Vector3& max, const Math::Vector4& color)
{
	DrawAABB(Math::AABB((min + max) * 0.5f, (max - min) * 0.5f), color);
}

void SimpleDraw::DrawAABB(const Math::Vector3& center, float radius, const Math::Vector4& color)
{
	DrawAABB(Math::AABB(center, Math::Vector3(radius, radius, radius)), color);
}

void SimpleDraw::DrawAABB(float minX, float minY, float minZ, float maxX, float maxY, float maxZ, const Math::Vector4& color)
{
	DrawAABB(Math::Vector3(minX, minY, minZ), Math::Vector3(maxX, maxY, maxZ), color);
}

void SimpleDraw::DrawOBB(const Math::OBB& obb, const Math::Vector4& color)
{
	ASSERT(sSimpleDraw != nullptr, "[SimpleDraw] Not initialized.");
	sSimpleDraw->DrawOBB(obb, color);
}

void SimpleDraw::DrawPlane(const Math::Plane& plane, const Math::Vector3& referencePoint, float size, float spacing, const Math::Vector4& color)
{
	ASSERT(sSimpleDraw != nullptr, "[SimpleDraw] Not initialized.");
	sSimpleDraw->DrawPlane(plane, referencePoint, size, spacing, color);
}

void SimpleDraw::DrawSphere(const Math::Sphere& sphere, const Math::Vector4& color, uint32_t slices, uint32_t rings)
{
	ASSERT(sSimpleDraw != nullptr, "[SimpleDraw] Not initialized.");
	sSimpleDraw->DrawSphere(sphere, color, slices, rings);
}

void SimpleDraw::DrawSphere(const Math::Vector3& center, float radius, const Math::Vector4& color, uint32_t slices, uint32_t rings)
{
	DrawSphere(Math::Sphere(center, radius), color, slices, rings);
}

void SimpleDraw::DrawSphere(float x, float y, float z, float radius, const Math::Vector4& color, uint32_t slices, uint32_t rings)
{
	DrawSphere(Math::Sphere(x, y, z, radius), color, slices, rings);
}

void SimpleDraw::DrawTransform(const Math::Matrix& transform)
{
	ASSERT(sSimpleDraw != nullptr, "[SimpleDraw] Not initialized.");
	sSimpleDraw->DrawTransform(transform);
}

void SimpleDraw::Flush(const Math::Matrix& matViewProj)
{
	ASSERT(sSimpleDraw != nullptr, "[SimpleDraw] Not initialized!");
	sSimpleDraw->Flush(matViewProj);
}