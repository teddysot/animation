#include "Precompiled.h"
#include "Mesh.h"

#include "Math\Inc\EngineMath.h"


using namespace Graphics;

Mesh::Mesh()
	:mVertexCount(0)
	, mIndexCount(0)
	, mVertexSize(sizeof(Graphics::Vertex))
	, mVertices(nullptr)
	, mIndices(nullptr)
{
}

void Mesh::Instantiate(int capacity)
{
	mVertices = new Graphics::Vertex[capacity];
	mIndices = new uint32_t[capacity];
}

void Mesh::Instantiate(uint32_t vertexCount, uint32_t indexCount)
{
	mVertices = new Graphics::Vertex[vertexCount];
	mIndices = new uint32_t[indexCount];
	mIndexCount = indexCount;
	mVertexCount = vertexCount;
}

void Mesh::Terminate()
{
	SafeDeleteArray(mIndices);
	SafeDeleteArray(mIndices);
}

void Mesh::AddVertex(const Math::Vector3& vertex, const Math::Vector2& uv, const Math::Vector3& normal, const Math::Vector3& tangent)
{
	int index = mVertexCount;
	mVertexCount++;

	mVertices[index].position = vertex;
	mVertices[index].uv = uv;
	mVertices[index].normal = normal;
	mVertices[index].tangent = tangent;

}

void Mesh::AddVertex(const Math::Vector3& vertex, const Math::Vector2& uv, const Math::Vector3& normal)
{
	int index = mVertexCount;
	mVertexCount++;

	mVertices[index].position = vertex;
	mVertices[index].uv = uv;
	mVertices[index].normal = normal;

}

void Mesh::AddVertex(const Math::Vector3& vertex, const Math::Vector2& uv)
{
	int index = mVertexCount;
	mVertexCount++;

	mVertices[index].position = vertex;
	mVertices[index].uv = uv;
}

void Mesh::AddIndices(uint32_t index)
{
	mIndices[mIndexCount++] = index;
}

Graphics::Vertex& Mesh::GetVertex(int index)
{
	return mVertices[index];
}

Graphics::Vertex& Mesh::GetVertex()
{
	return mVertices[mVertexCount - 1];
}

uint32_t& Mesh::GetIndex(uint32_t index)
{
	ASSERT(index < mIndexCount, "[Mesh] Invalid index %d. mNumIndices = %d.", index, mIndexCount);
	return mIndices[index];
}



const Graphics::Vertex* Mesh::GetVertices() const
{
	return mVertices;
}

uint32_t Mesh::GetVertexCount() const
{
	return mVertexCount;
}

const uint32_t* Mesh::GetIndices() const
{
	return mIndices;
}

uint32_t Mesh::GetIndexCount() const
{
	return mIndexCount;
}

uint32_t Mesh::GetVertexSize() const
{
	return mVertexSize;
}

void Mesh::SetMaterial(const Material& mat)
{
	mMaterial = mat;
}

Material Mesh::GetMaterial() const
{
	return mMaterial;
}