#include "Precompiled.h"
#include "Transform.h"

namespace Graphics
{


	Transform::Transform()
		:mPosition(Math::Vector3::Zero()),
		mDirection(Math::Vector3::ZAxis()),
		mScale(1.0f,1.0f,1.0f)
	{
	}

	Transform::Transform(const Math::Vector3& position, const Math::Vector3& direction)
		: mPosition(position),
		mDirection(Math::Normalize(direction))
	{
	}

	//translation
	void Transform::Walk(float distance)
	{
		mPosition += mDirection * distance;
	}
	void Transform::Strafe(float distance)
	{
		Math::Vector3 right = Math::Cross(Math::Vector3::YAxis(), mDirection);
		mPosition += right * distance;
	}
	void Transform::Rise(float distance)
	{
		mPosition += Math::Vector3::YAxis() * distance;
	}


	//rotation
	void Transform::Yaw(float rad)
	{
		Math::Matrix rotationY = Math::Matrix::RotationY(rad);
		mDirection = Math::TransformNormal(mDirection, rotationY);
		mRotation = Math::Quaternion::RotationAxis(Math::Vector3::YAxis(), rad);
	}
	void Transform::Pitch(float rad)
	{
		Math::Vector3 right = Math::Cross(Math::Vector3::YAxis(), mDirection);
		Math::Matrix rotationPitch = Math::Matrix::RotationAxis(right, rad);
		mDirection = Math::TransformNormal(mDirection, rotationPitch);
		mRotation = Math::Quaternion::RotationAxis(Math::Vector3::XAxis(), rad);
	}

	//scale
	void Transform::ScaleBy(const Math::Vector3& s)
	{
		mScale += s;
	}

	void Transform::ScaleBy(float s)
	{
		mScale.x += s;
		mScale.y += s;
		mScale.z += s;
	}


	Math::Matrix Transform::GetWorldMatrix() const
	{
		Math::Vector3 forward = Math::Normalize(mDirection);
		Math::Vector3 right = Math::Normalize(Math::Cross(Math::Vector3::YAxis(), forward));
		Math::Vector3 up = Math::Normalize(Math::Cross(forward, right));
		return Math::Matrix::Scaling(mScale) * (Math::Matrix(
			right.x,		right.y,		right.z,		0.0f,
			up.x,			up.y,			up.z,			0.0f,
			forward.x,		forward.y,		forward.z,		0.0f,
			mPosition.x,	mPosition.y,	mPosition.z,	1.0f));
	}

	//mutators
	void Transform::SetPostion(const Math::Vector3& position)
	{
		mPosition = position;
	}

	void Transform::SetDirection(const Math::Vector3& direction)
	{
		mDirection = Math::Normalize(direction);
	}


	void Transform::SetScale(const Math::Vector3& s)
	{
		mScale = s;
	}


}
