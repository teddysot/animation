#include "Precompiled.h"
#include "Sampler.h"

#include "GraphicsSystem.h"



using namespace Graphics;

namespace
{

	D3D11_FILTER GetFilter(Sampler::Filter filter)
	{
		switch (filter)
		{
		case Graphics::Sampler::Filter::Point:
			return D3D11_FILTER_MIN_MAG_MIP_POINT;
			break;
		case Graphics::Sampler::Filter::Linear:
			return D3D11_FILTER_MIN_MAG_MIP_LINEAR;
			break;
		case Graphics::Sampler::Filter::Anisotropic:
			return D3D11_FILTER_ANISOTROPIC;
			break;
		default:
			ASSERT(false, "[Sampler] Invalid filter mode");
			break;
		}
		return D3D11_FILTER_MIN_MAG_MIP_POINT;
	}

	D3D11_TEXTURE_ADDRESS_MODE GetAddressMode(Sampler::AddressMode mode)
	{
		switch (mode)
		{
		case Graphics::Sampler::AddressMode::Border:
			return D3D11_TEXTURE_ADDRESS_BORDER;
			break;
		case Graphics::Sampler::AddressMode::Clamp:
			return D3D11_TEXTURE_ADDRESS_CLAMP;
			break;
		case Graphics::Sampler::AddressMode::Mirror:
			return D3D11_TEXTURE_ADDRESS_MIRROR;
			break;
		case Graphics::Sampler::AddressMode::Wrap:
			return D3D11_TEXTURE_ADDRESS_WRAP;
			break;
		default:
			ASSERT(false, "[Sampler] Invalid address mode");
			break;
		}

		return D3D11_TEXTURE_ADDRESS_WRAP;

	}

}

Sampler::Sampler()
	:mSampler(nullptr)
{

}

Sampler::~Sampler()
{
	ASSERT(mSampler == nullptr, "[Texture] Sampler not released");
}

void Sampler::Initialize(Filter filter, AddressMode address)
{
	D3D11_FILTER d3dFilter = GetFilter(filter);
	D3D11_TEXTURE_ADDRESS_MODE d3dAddressMode = GetAddressMode(address);
	D3D11_SAMPLER_DESC sampDesc = {};
	sampDesc.Filter = d3dFilter;
	sampDesc.AddressU = d3dAddressMode;
	sampDesc.AddressV = d3dAddressMode;
	sampDesc.AddressW = d3dAddressMode;
	sampDesc.BorderColor[0] = 1.0f;
	sampDesc.BorderColor[1] = 1.0f;
	sampDesc.BorderColor[2] = 1.0f;
	sampDesc.BorderColor[3] = 1.0f;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sampDesc.MinLOD = 0;
	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;

	ID3D11Device* device = GraphicsSystem::Get()->GetDevice();
	HRESULT hr = device->CreateSamplerState(&sampDesc, &mSampler);
	
	ASSERT(SUCCEEDED(hr), "[Texture] Failed to create sampler state");

}

void Sampler::Terminate()
{
	SafeRelease(mSampler);
}

void Sampler::BindVS(uint32_t slot)
{
	GraphicsSystem::Get()->GetContext()->VSSetSamplers(slot, 1, &mSampler);
}

void Sampler::BindPS(uint32_t slot)
{
	GraphicsSystem::Get()->GetContext()->PSSetSamplers(slot, 1, &mSampler);
}