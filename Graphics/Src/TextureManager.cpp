#include "Precompiled.h"
#include "TextureManager.h"
#include "Texture.h"

using namespace Graphics;

namespace
{
	TextureManager* sTextureManager = nullptr;
}

void TextureManager::StaticInitialize()
{
	ASSERT(sTextureManager == nullptr, "[Graphics::TextureManager] Manager already initialized!");
	sTextureManager = new TextureManager();
}

void TextureManager::StaticTerminate()
{
	if (sTextureManager != nullptr)
	{
		SafeDelete(sTextureManager);
	}
}

TextureManager * Graphics::TextureManager::Get()
{
	ASSERT(sTextureManager != nullptr, "[Graphics::TextureManager] No instance registered.");
	return sTextureManager;
}

Graphics::TextureManager::TextureManager()
{
}

Graphics::TextureManager::~TextureManager()
{
	for (auto& item : mInventory)
	{
		item.second->Terminate();
		SafeDelete(item.second);
	}
	mInventory.clear();
}

void Graphics::TextureManager::SetRootPath(const char * root)
{
	mRoot = root;
}

TextureId Graphics::TextureManager::Load(const char * fileName)
{
	std::string fullName = mRoot + fileName;
	std::hash<std::string> hasher;
	TextureId hash = hasher(fullName);

	auto result = mInventory.insert({ hash, nullptr });

	if (result.second)
	{
		Texture* texture = new Texture();
		texture->Initialize(fullName.c_str());
		result.first->second = texture;
	}

	return hash;
}

void TextureManager::BindVS(TextureId id, uint32_t slot)
{
	mInventory[id]->BindVS(slot);
}

void TextureManager::BindPS(TextureId id, uint32_t slot)
{
	mInventory[id]->BindPS(slot);
}