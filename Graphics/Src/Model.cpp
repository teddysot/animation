#include "Precompiled.h"
#include "Model.h"
#include "Mesh.h"
#include "MeshBuffer.h"
#include "Texture.h"
#include "TextureManager.h"

using namespace Graphics;

Model::Model()
{

}

Model::~Model()
{
	ASSERT(mMeshes.empty(), "[Model] Unload() must be called to free memory.");
	ASSERT(mMeshBuffers.empty(), "[Model] Unload() must be called to free memory.");
	ASSERT(mTextures.empty(), "[Model] Unload() must be called to free memory.");
}

void Model::Load(const char* filename)
{
	FILE* file = nullptr;
	errno_t error = fopen_s(&file, filename, "r");
	ASSERT(error == 0, "[Model] Error loading model %s.", filename);

	fscanf_s(file, "%*s %d\n", &numMeshes);

	for (uint32_t m = 0; m < numMeshes; ++m)
	{
		Mesh* mesh = new Mesh();

		fscanf(file, "VertexCount: %d\n", &numVertices);
		fscanf(file, "IndexCount: %d\n", &numIndices);
		fscanf(file, "MaterialIndex: %d\n", &materialIndex);

		mesh->Instantiate(numVertices, numIndices);

		for (uint32_t v = 0; v < numVertices; ++v)
		{
			Vertex& mVertex = mesh->GetVertex(v);
			fscanf(file, "%f %f %f %f %f %f %f %f %f %f %f\n",
				&mVertex.position.x, &mVertex.position.y, &mVertex.position.z,
				&mVertex.normal.x, &mVertex.normal.y, &mVertex.normal.z,
				&mVertex.tangent.x, &mVertex.tangent.y, &mVertex.tangent.z,
				&mVertex.uv.x, &mVertex.uv.y
			);
		}

		for (uint32_t i = 0; i < numIndices; i += 3)
		{
			uint32_t mIndex1, mIndex2, mIndex3;
			fscanf(file, "%d %d %d\n", &mIndex1, &mIndex2, &mIndex3);
			mesh->GetIndex(i) = mIndex1;
			mesh->GetIndex(i + 1) = mIndex2;
			mesh->GetIndex(i + 2) = mIndex3;
		}

		MeshBuffer* meshBuffer = new MeshBuffer();

		meshBuffer->Initialize(
			mesh->GetVertices(),
			sizeof(Vertex),
			mesh->GetVertexCount(),
			mesh->GetIndices(),
			mesh->GetIndexCount());

		mMeshes.push_back(mesh);
		mMeshBuffers.push_back(meshBuffer);
		mMaterialIndex.push_back(materialIndex);
	}

	texManager->Get()->SetRootPath("../Assets/Models/Textures/");

	fscanf(file, "MaterialCount: %d\n", &numMaterial);
	for (uint32_t m = 0; m <= numMaterial; ++m)
	{
		char textureName[256];

		fscanf(file, "DiffuseMap: %s\n", textureName);

		mID = texManager->Get()->Load(textureName);
		mTexturesId.push_back(mID);
	}

	fclose(file);
}

void Model::Unload()
{
	for (auto it : mMeshBuffers)
	{
		it->Terminate();
	}
	for (auto it : mTextures)
	{
		it->Terminate();
	}
	for (auto it : mMeshes)
	{
		it->Terminate();
	}
	SafeDeleteVector(mMeshes);
	SafeDeleteVector(mMeshBuffers);
	SafeDeleteVector(mTextures);
}

void Model::Render()
{
	for (size_t i = 0; i < numMeshes; ++i)
	{
		TextureManager::Get()->BindPS(mTexturesId[mMaterialIndex[i]]);
		mMeshBuffers[i]->Render();
	}
}