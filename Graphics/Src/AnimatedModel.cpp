#include "Precompiled.h"
#include "AnimatedModel.h"
#include "Bone.h"
#include "SkinnedMesh.h"
#include "MeshBuffer.h"
#include "Texture.h"
#include "TextureManager.h"

using namespace Graphics;

namespace
{
	void ScanMatrix(FILE* file, Math::Matrix& m)
	{
		fscanf_s(file, "%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n",
			&m._11, &m._12, &m._13, &m._14,
			&m._21, &m._22, &m._23, &m._24,
			&m._31, &m._32, &m._33, &m._34,
			&m._41, &m._42, &m._43, &m._44);
	}
}

AnimatedModel::AnimatedModel()
	: mRoot(nullptr)
{
}

AnimatedModel::~AnimatedModel()
{
	ASSERT(mMeshes.empty(), "[AnimatedModel] Unload() must be called to free memory.");
	ASSERT(mMeshBuffers.empty(), "[AnimatedModel] Unload() must be called to free memory.");
	ASSERT(mTextures.empty(), "[AnimatedModel] Unload() must be called to free memory.");
}

void AnimatedModel::Load(const char* filename)
{
	FILE* file = nullptr;
	errno_t error = fopen_s(&file, filename, "r");
	ASSERT(error == 0, "[AnimatedModel] Error loading model %s.", filename);

	uint32_t numMeshes = 0;
	fscanf_s(file, "%*s %d\n", &numMeshes);

	for (uint32_t m = 0; m < numMeshes; ++m)
	{
		SkinnedMesh* mesh = new SkinnedMesh();

		uint32_t numVertices = 0;
		uint32_t numIndices = 0;
		uint32_t materialIndex = 0;

		fscanf_s(file, "VertexCount: %d\n", &numVertices);
		fscanf_s(file, "IndexCount: %d\n", &numIndices);
		fscanf_s(file, "MaterialIndex: %d\n", &materialIndex);

		mesh->Instantiate(numVertices, numIndices);

		for (uint32_t v = 0; v < numVertices; ++v)
		{
			BoneVertex& mVertex = mesh->GetVertex(v);
			fscanf_s(file, "%f %f %f %f %f %f %f %f %f %f %f\n",
				&mVertex.position.x, &mVertex.position.y, &mVertex.position.z,
				&mVertex.normal.x, &mVertex.normal.y, &mVertex.normal.z,
				&mVertex.tangent.x, &mVertex.tangent.y, &mVertex.tangent.z,
				&mVertex.uv.x, &mVertex.uv.y
			);
		}

		for (uint32_t i = 0; i < numIndices; i += 3)
		{
			uint32_t mIndex1, mIndex2, mIndex3;
			fscanf_s(file, "%d %d %d\n", &mIndex1, &mIndex2, &mIndex3);
			mesh->GetIndex(i) = mIndex1;
			mesh->GetIndex(i + 1) = mIndex2;
			mesh->GetIndex(i + 2) = mIndex3;
		}

		//---------------------------------------------------------------------------------------------------
		// read bone weight here!!!
		for (uint32_t j = 0; j < numVertices; ++j)
		{
			BoneVertex& v = mesh->GetVertex(j);
			uint32_t numBones = 0;
			fscanf_s(file, "%d %d %d %d %d %f %f %f %f\n",
				&numBones,
				&v.boneIndex[0], &v.boneIndex[1], &v.boneIndex[2], &v.boneIndex[3],
				&v.boneWeight[0], &v.boneWeight[1], &v.boneWeight[2], &v.boneWeight[3]
			);
		}

		mMeshes.push_back(mesh);
		mMaterialIndex.push_back(materialIndex);
	}


	TextureManager* texManager;
	texManager->Get()->SetRootPath("../Assets/Models/Textures/");

	uint32_t numMaterial = 0;
	fscanf_s(file, "MaterialCount: %d\n", &numMaterial);
	for (uint32_t m = 0; m <= numMaterial; ++m)
	{
		char textureName[256];
		fscanf(file, "DiffuseMap: %s\n", textureName);

		TextureId mID;
		mID = texManager->Get()->Load(textureName);
		mTexturesId.push_back(mID);
	}


	// Reading bone
	uint32_t boneCount = 0;
	fscanf_s(file, "BoneCount: %d\n", &boneCount);
	for (uint32_t i = 0; i < boneCount; ++i)
	{
		Bone* bone = new Bone();

		char boneName[128];
		fscanf_s(file, "Name: %s\n", boneName, 128);
		bone->name = boneName;

		fscanf_s(file, "Index: %d\n", &bone->index);
		fscanf_s(file, "Parent: %d\n", &bone->parentIndex);

		uint32_t numChild = 0;
		fscanf_s(file, "ChildCount: %d\n", &numChild);
		if (numChild > 0)
		{
			uint32_t childIndex = 0;
			for (uint32_t j = 0; j < numChild; ++j)
			{
				fscanf_s(file, "%d ", &childIndex);
				bone->childrenIndex.push_back(childIndex);
			}
			fscanf_s(file, "\n");
		}
		ScanMatrix(file, bone->transform);
		ScanMatrix(file, bone->offsetTransform);

		mSkeleton.push_back(bone);
	}

	// Re-link skeleton bones
	for (auto bone : mSkeleton)
	{
		if (bone->parentIndex == -1)
		{
			mRoot = bone;
		}
		else
		{
			bone->parent = mSkeleton[bone->parentIndex];
		}

		for (auto childIndex : bone->childrenIndex)
		{
			bone->children.push_back(mSkeleton[childIndex]);
		}
	}

	// Load AnimationClip
	uint32_t numAnimations = 0;
	fscanf_s(file, "AnimationCount: %d\n", &numAnimations);

	mAnimationClips.resize(numAnimations);

	for (uint32_t i = 0; i < numAnimations; ++i)
	{
		auto& animationClip = mAnimationClips[i];

		char animationName[128];
		fscanf_s(file, "Name: %s\n", animationName, 128);
		animationClip.mName = animationName;
		//fgetc(file);

		fscanf_s(file, "Duration: %f\n", &animationClip.mDuration);
		fscanf_s(file, "TicksPerSecond: %f\n", &animationClip.mTicksPerSecond);

		if (animationClip.mTicksPerSecond == 0.0f)
		{

		}

		animationClip.mBoneAnimations.resize(boneCount);

		uint32_t numBoneAnimation = 0;
		fscanf_s(file, "BoneAnimations: %d\n", &numBoneAnimation);

		if (numBoneAnimation > 0)
		{
			for (uint32_t j = 0; j < numBoneAnimation; ++j)
			{
				//uint32_t boneIndex = 0;
				BoneAnimation* boneAnim = new BoneAnimation();
				fscanf_s(file, "BoneIndex: %d\n", &boneAnim->boneIndex);
				uint32_t keyFrames = 0;
				fscanf_s(file, "Keyframes: %d\n", &keyFrames);
				Animation Anim;
				if (keyFrames > 0)
				{
					Graphics::Keyframe keyframe;
					Anim.SetLooping(true);
					for (uint32_t z = 0; z < keyFrames; ++z) // mBoneAnimations array
					{
						float t, px, py, pz, rx, ry, rz, rw, sx, sy, sz;
						fscanf_s(file, "%f %f %f %f %f %f %f %f %f %f %f\n"
							, &t, &px, &py, &pz, &rx, &ry, &rz, &rw, &sx, &sy, &sz);
						keyframe.time = t;
						keyframe.position.x = px;
						keyframe.position.y = py;
						keyframe.position.z = pz;
						keyframe.rotation.x = rx;
						keyframe.rotation.y = ry;
						keyframe.rotation.z = rz;
						keyframe.rotation.w = rw;
						keyframe.scale.x = rx;
						keyframe.scale.y = ry;
						keyframe.scale.z = rz;
						Anim.AddKeyframe(keyframe);
					}
				}

				animationClip.mBoneAnimations[boneAnim->boneIndex] = Anim;
			}
			fscanf_s(file, "\n");
		}
		mAnimationClips.push_back(animationClip);
	}


	fclose(file);

	for (auto m : mMeshes)
	{
		MeshBuffer* meshBuffer = new MeshBuffer();
		meshBuffer->Initialize(*m);
		mMeshBuffers.push_back(meshBuffer);
	}
}

void AnimatedModel::Unload()
{
	for (auto it : mMeshBuffers)
	{
		it->Terminate();
	}
	for (auto it : mTextures)
	{
		it->Terminate();
	}
	for (auto it : mMeshes)
	{
		it->Terminate();
	}
	SafeDeleteVector(mMeshes);
	SafeDeleteVector(mMeshBuffers);
	SafeDeleteVector(mTextures);
}

void Graphics::AnimatedModel::Play(bool looping)
{
	mAnimationClips[0].Play(looping);
}

void Graphics::AnimatedModel::Stop()
{
	mAnimationClips[0].Stop();
}

void Graphics::AnimatedModel::Reset()
{
	mAnimationClips[0].Reset();
}

void AnimatedModel::Render()
{
	for (size_t i = 0; i < mMeshes.size(); ++i)
	{
		TextureManager::Get()->BindPS(mTexturesId[mMaterialIndex[i]]);
		mMeshBuffers[i]->Render();
	}
}

void AnimatedModel::RenderSkeleton()
{
	std::vector<Math::Matrix> mBone;

	for (int i = 0; i < mSkeleton.size(); ++i)
	{
		if (mSkeleton[i]->parent == nullptr)
		{
			mBone.push_back(mSkeleton[i]->transform);
			continue;
		}
		Bone* current = mSkeleton[i]->parent;
		Math::Matrix temp = mSkeleton[i]->transform * current->transform;

		while (current->parent != nullptr)
		{
			temp = temp * current->parent->transform;
			current = current->parent;
		}
		mBone.push_back(temp);
	}

	for (int i = 0; i < mBone.size(); ++i)
	{
		if (mSkeleton[i]->parent != nullptr)
		{
			Math::Vector3 p1(mBone[i]._41, mBone[i]._42, mBone[i]._43);
			Math::Vector3 p2(mBone[mSkeleton[i]->parent->index]._41, mBone[mSkeleton[i]->parent->index]._42, mBone[mSkeleton[i]->parent->index]._43);
			Graphics::SimpleDraw::DrawLine(p1, p2, Math::Vector4::Red());
		}
	}
}

// Render Skeleton Animation
void AnimatedModel::RenderSkeletonAnimation()
{
	std::vector<Math::Matrix> boneTransforms = mAnimationClips[0].GetTransform();

	std::vector<Math::Matrix> boneWorldTransforms;
	boneWorldTransforms.resize(mSkeleton.size(), Math::Matrix::Identity());

	for (int i = 0; i < mSkeleton.size(); ++i)
	{
		Bone* bone = mSkeleton[i];

		Math::Matrix transform = boneTransforms[bone->index];
		while (bone->parent)
		{
			transform = transform * boneTransforms[bone->parentIndex];
			bone = bone->parent;
		}
		boneWorldTransforms[i] = transform;
	}

	for (int i = 0; i < mSkeleton.size(); ++i)
	{
		if (mSkeleton[i]->parent == nullptr)
			continue;

		Math::Vector3 posA = Math::GetTranslation(boneWorldTransforms[i]);
		Math::Vector3 posB = Math::GetTranslation(boneWorldTransforms[mSkeleton[i]->parentIndex]);
		SimpleDraw::DrawLine(posA, posB, Math::Vector4::Green());
	}
}

std::vector<Math::Matrix> Graphics::AnimatedModel::GetBoneWorldTransforms() const
{
	std::vector<Math::Matrix> mBone;

	for (int i = 0; i < mSkeleton.size(); ++i)
	{
		if (mSkeleton[i]->parent == nullptr)
		{
			mBone.push_back(mSkeleton[i]->transform);
			continue;
		}
		Bone* current = mSkeleton[i]->parent;
		Math::Matrix temp = mSkeleton[i]->transform * current->transform;

		while (current->parent != nullptr)
		{
			temp = temp * current->parent->transform;
			current = current->parent;
		}
		mBone.push_back(temp);
	}

	return mBone;
}