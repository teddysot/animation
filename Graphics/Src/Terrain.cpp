#include "Precompiled.h"
#include "Terrain.h"
#include "MeshBuffer.h"

#include <fstream>
#include <iostream>

using namespace Graphics;



Terrain::Terrain()
{

}

//http://www.chadvernon.com/blog/resources/directx9/terrain-generation-with-a-heightmap/

void Terrain::Initialize(const char* heightMap, Math::Vector3 scale)
{
	std::ifstream rawFile(heightMap, std::ios::binary);

	int inputCount = 0;

	mScale = scale;

	rawFile.seekg(0, std::ios::end);
	int numVertices = rawFile.tellg();
	rawFile.seekg(0, std::ios::beg);

	mMapArray = new unsigned char[numVertices];
	rawFile.read((char*)mMapArray, numVertices);
	rawFile.close();

	unsigned int width = (int)sqrt((float)numVertices);

	mWidth = width - 1;

	mTerrainMesh.Instantiate(1000000);

	mTerrainBuffer.SetTopology(MeshBuffer::Topology::TriangleStrip);


	for (int z = 0; z < width; z++)
	{
		for (int x = 0; x < width; x++)
		{
			float halfWidth = ((float)width - 1.0f) / 2.0f;
			float halfLength = ((float)width - 1.0f) / 2.0f;
			Math::Vector3 vert = Math::Vector3(((float)x - halfWidth) * scale.x, ((float)(mMapArray[z * width + x]) * scale.y), ((float)z - halfWidth) * scale.z);
			Math::Vector2 uv = Math::Vector2((float)x / (width - 1), (float)z / (width - 1));
			mTerrainMesh.AddVertex(vert, uv, Math::Vector3::YAxis());
		}
	}

	int index = 0;
	for (int z = 0; z < width - 1; z++)
	{
		// Even rows move left to right, odd moves right to left
		if (z % 2 == 0)
		{
			// Even
			int x;
			for (x = 0; x < width; x++)
			{
				mTerrainMesh.AddIndices(x + (z*width));
				mTerrainMesh.AddIndices(x + (z*width) + width);
			}

			// Insert degenerate vertex if it is not the last row
			if (z != width - 2)
			{
				mTerrainMesh.AddIndices(--x + (z*width));
			}
		}
		else
		{
			// Odd
			int x;
			for (x = width - 1; x >= 0; x--)
			{
				mTerrainMesh.AddIndices(x + (z*width));
				mTerrainMesh.AddIndices(x + (z*width) + width);
			}

			// Insert degenerate vertex if this isn't the last row
			if (z != width - 2)
			{
				mTerrainMesh.AddIndices(++x + (z*width));
			}
		}
	}


	mTerrainBuffer.Initialize(mTerrainMesh);


	//printf("Debug");

}

void Terrain::Terminate()
{
	SafeDeleteArray(mMapArray);
	mTerrainBuffer.Terminate();
	mTerrainMesh.Terminate();
}

void Terrain::Render()
{
	mTerrainBuffer.Render();
}

const Math::Vector3 Terrain::GetScaledDimentions() const
{
	return Math::Vector3(mScale.x * mWidth, mScale.y * mWidth, mScale.z * mWidth);
}