#include "Precompiled.h"
#include "Texture.h"

#include "GraphicsSystem.h"

#include <DirectXTK\Inc\DDSTextureLoader.h>
#include <DirectXTK\Inc\WICTextureLoader.h>


using namespace Graphics;

Texture::Texture()
	:mShaderResourceView(nullptr)
{

}

Texture::~Texture()
{
	ASSERT(mShaderResourceView == nullptr, "[Texture] Texture not released");
}

void Texture::Initialize(const char* fileName)
{
	// MBS - Multi-byte strings
	// WCS - Wide character strings
	// _s - safe version
	wchar_t wbuffer[1024];
	mbstowcs_s(nullptr, wbuffer, fileName, 1024);
	Initialize(wbuffer);
}

void Texture::Initialize(const wchar_t* fileName)
{
	ID3D11Device* device = GraphicsSystem::Get()->GetDevice();
	ID3D11DeviceContext* context = GraphicsSystem::Get()->GetContext();

	if (wcsstr(fileName, L".dds") != nullptr)
	{
		DirectX::CreateDDSTextureFromFile(device, context, fileName, nullptr, &mShaderResourceView);
	}
	else
	{
		DirectX::CreateWICTextureFromFile(device, context, fileName, nullptr, &mShaderResourceView);
	}
}

void Texture::Terminate()
{
	SafeRelease(mShaderResourceView);
}

void Texture::BindVS(uint32_t slot)
{
	GraphicsSystem::Get()->GetContext()->VSSetShaderResources(slot, 1, &mShaderResourceView);
}

void Texture::BindPS(uint32_t slot)
{
	GraphicsSystem::Get()->GetContext()->PSSetShaderResources(slot, 1, &mShaderResourceView);
}