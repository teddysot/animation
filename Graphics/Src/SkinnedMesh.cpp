#include "Precompiled.h"
#include "SkinnedMesh.h"

#include "Math\Inc\EngineMath.h"


using namespace Graphics;

SkinnedMesh::SkinnedMesh()
	:mVertexCount(0)
	, mIndexCount(0)
	, mVertexSize(sizeof(Graphics::BoneVertex))
	, mVertices(nullptr)
	, mIndices(nullptr)
{
}

void SkinnedMesh::Instantiate(int capacity)
{
	mVertices = new Graphics::BoneVertex[capacity];
	mIndices = new uint32_t[capacity];
}

void SkinnedMesh::Instantiate(uint32_t vertexCount, uint32_t indexCount)
{
	mVertices = new Graphics::BoneVertex[vertexCount];
	mIndices = new uint32_t[indexCount];
	mIndexCount = indexCount;
	mVertexCount = vertexCount;
}

void SkinnedMesh::Terminate()
{
	SafeDeleteArray(mIndices);
	SafeDeleteArray(mIndices);
}

void SkinnedMesh::AddVertex(const Math::Vector3& vertex, const Math::Vector2& uv, const Math::Vector3& normal, const Math::Vector3& tangent)
{
	int index = mVertexCount;
	mVertexCount++;

	mVertices[index].position = vertex;
	mVertices[index].uv = uv;
	mVertices[index].normal = normal;
	mVertices[index].tangent = tangent;

}

void SkinnedMesh::AddVertex(const Math::Vector3& vertex, const Math::Vector2& uv, const Math::Vector3& normal)
{
	int index = mVertexCount;
	mVertexCount++;

	mVertices[index].position = vertex;
	mVertices[index].uv = uv;
	mVertices[index].normal = normal;

}

void SkinnedMesh::AddVertex(const Math::Vector3& vertex, const Math::Vector2& uv)
{
	int index = mVertexCount;
	mVertexCount++;

	mVertices[index].position = vertex;
	mVertices[index].uv = uv;
}

void SkinnedMesh::AddIndices(uint32_t index)
{
	mIndices[mIndexCount++] = index;
}

Graphics::BoneVertex& SkinnedMesh::GetVertex(int index)
{
	return mVertices[index];
}

Graphics::BoneVertex& SkinnedMesh::GetVertex()
{
	return mVertices[mVertexCount - 1];
}

uint32_t& SkinnedMesh::GetIndex(uint32_t index)
{
	ASSERT(index < mIndexCount, "[Mesh] Invalid index %d. mNumIndices = %d.", index, mIndexCount);
	return mIndices[index];
}



Graphics::BoneVertex* SkinnedMesh::GetVertices() const
{
	return mVertices;
}

uint32_t SkinnedMesh::GetVertexCount() const
{
	return mVertexCount;
}

const uint32_t* SkinnedMesh::GetIndices() const
{
	return mIndices;
}

uint32_t SkinnedMesh::GetIndexCount() const
{
	return mIndexCount;
}

uint32_t SkinnedMesh::GetVertexSize() const
{
	return mVertexSize;
}

void SkinnedMesh::SetMaterial(const Material& mat)
{
	mMaterial = mat;
}

Material SkinnedMesh::GetMaterial() const
{
	return mMaterial;
}