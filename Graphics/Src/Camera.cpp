#include "Precompiled.h"
#include "Camera.h"

#include "Transform.h"

namespace Graphics
{


	Camera::Camera()
		:mNearPlane(1.0f),
		mFarPlane(10000.0f),
		mFieldOfView(60.0f * Math::kDegToRad),
		mMinFOV(0.0f),
		mMaxFOV(180.0f * Math::kDegToRad)
	{
	}

	void Camera::SetUp(float nearPlane, float farPlane, float fieldOfView)
	{
		ASSERT(nearPlane >= 0.0f, "[Camera] Invalid camera parameter");
		ASSERT(farPlane > 0.0f, "[Camera] Invalid camera parameter");
		ASSERT(nearPlane < farPlane, "[Camera] Invalid camera parameter");
		ASSERT(fieldOfView > 0.0f, "[Camera] Invalid camera parameter");
		mNearPlane = nearPlane;
		mFarPlane = farPlane;
		mFieldOfView = fieldOfView;
	}

	void Camera::SetFOVLimits(float min, float max)
	{
		ASSERT(min > 0.0f, "[Camera] Invalid camera parameter");
		ASSERT(max < 180.0f * Math::kDegToRad, "[Camera] Invalid camera parameter");
		ASSERT(min < max, "[Camera] Invalid camera parameter");
		mMinFOV = min;
		mMaxFOV = max;
	}
	void Camera::ZoomIn(float rad)
	{
		ASSERT(rad > 0.0f, "[Camera] Invalid camera parameter");
		mFieldOfView = Math::Max(mFieldOfView - rad, mMinFOV);
	}
	void Camera::ZoomOut(float rad)
	{
		ASSERT(rad > 0.0f, "[Camera] Invalid camera parameter");
		mFieldOfView = Math::Max(mFieldOfView + rad, mMaxFOV);
	}

	Math::Matrix Camera::GetViewMatrix(const Transform& transform)
	{
		return Math::Inverse(transform.GetWorldMatrix());
	}
	Math::Matrix Camera::GetProjectionMatrix(float aspectRatio)
	{
		const float h = 1 / tan(mFieldOfView * 0.5f);
		const float w = h / aspectRatio;
		const float f = mFarPlane;
		const float n = mNearPlane;
		const float d = f / (f - n);
		return Math::Matrix(
			w, 0.0f, 0.0f, 0.0f,
			0.0f, h, 0.0f, 0.0f,
			0.0f, 0.0f, d, 1.0f,
			0.0f, 0.0f, -n*d, 0.0f);
	}
}