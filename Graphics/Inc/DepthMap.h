#ifndef INCLUDED_GRAPHICS_DEPTHMAP_H
#define INCLUDED_GRAPHICS_DEPTHMAP_H

namespace Graphics
{
	class DepthMap
	{
	public:
		DepthMap();
		~DepthMap();

		void Initialize(uint32_t width, uint32_t height);
		void Terminate();

		void BeginRender();
		void EndRender();

		void BindPS(uint32_t slot);
		void UnbindPS(uint32_t slot);

	private:
		DepthMap(const DepthMap&) = delete;
		DepthMap& operator=(const DepthMap&) = delete;

		ID3D11ShaderResourceView* mShaderResourceView;
		ID3D11DepthStencilView* mDepthStencilView;
		D3D11_VIEWPORT mViewport; // NDC to Screen Space
	};
} // namespace Graphics

#endif INCLUDED_GRAPHICS_DEPTHMAP_H // #ifndef INCLUDED_GRAPHICS_RENDERTARGET_H