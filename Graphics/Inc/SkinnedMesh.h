#ifndef ENGINE_GRAPHICS_SKINNEDMESH_H
#define ENGINE_GRAPHICS_SKINNEDMESH_H

class Vector3;
class Vector2;
class Vertex;

#include "Material.h"
#include "VertexTypes.h"

namespace Graphics
{
	class SkinnedMesh
	{
	public:

		SkinnedMesh();

		void Instantiate(int capacity);
		void Instantiate(uint32_t vertexCount, uint32_t indexCount);
		void Terminate();

		void AddVertex(const Math::Vector3& vertex, const Math::Vector2& uv, const Math::Vector3& normal, const Math::Vector3& tangent);
		void AddVertex(const Math::Vector3& vertex, const Math::Vector2& uv, const Math::Vector3& normal);
		void AddVertex(const Math::Vector3& vertex, const Math::Vector2& uv);
		void AddIndices(uint32_t index);

		Graphics::BoneVertex& GetVertex(int index);
		Graphics::BoneVertex& GetVertex();

		uint32_t& GetIndex(uint32_t index);

		BoneVertex* GetVertices() const;
		uint32_t GetVertexCount() const;

		const uint32_t* GetIndices() const;
		uint32_t GetIndexCount() const;

		uint32_t GetVertexSize() const;

		void SetMaterial(const Material& mat);
		Material GetMaterial() const;

	private:
		SkinnedMesh(const SkinnedMesh&) = delete;
		SkinnedMesh& operator=(const SkinnedMesh&) = delete;

		friend class MeshBuilder;
		Graphics::BoneVertex* mVertices;
		uint32_t mVertexCount;

		uint32_t* mIndices;
		uint32_t mIndexCount;

		uint32_t mVertexSize;

		Material mMaterial;
	};

}

#endif
