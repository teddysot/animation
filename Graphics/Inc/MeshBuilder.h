#ifndef ENGINE_GRAPHICS_MESHBUILDER_H
#define ENGINE_GRAPHICS_MESHBUILDER_H


#include "Mesh.h"
//#include "SkinnedMesh.h"


namespace Graphics
{
	class MeshBuilder
	{
	public:
		// Function to generating a mesh

		static void CreateSphere(float radius, uint32_t rings, uint32_t slices, Mesh& mesh);
		static void CreateCylinder(float radius, float height, uint32_t slices, uint32_t rings, Mesh & mesh);
		static void CreatePlane(const Math::Vector2& size, Mesh& mesh);
		static void CreateSkybox(float size, Mesh& mesh);
		static void CreateCube(Mesh& mesh);
		static void CreateQuad(Mesh& mesh);
	private:
	};

}

#endif
