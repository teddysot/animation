#ifndef INCLUDED_GRAPHICS_VERTEXTYPE_H
#define INCLUDED_GRAPHICS_VERTEXTYPE_H

#define VF_POSITION		0x1 << 0
#define VF_NORMAL		0x1 << 1
#define VF_Tangent		0x1 << 2
#define VF_Color		0x1 << 3
#define VF_UV			0x1 << 4
#define VF_BINDEX		0x1 << 5
#define VF_BWEIGHT		0x1 << 6
//will add more later

namespace Graphics
{

	struct VertexP
	{
		static const uint32_t Format = VF_POSITION;
		Math::Vector3 position;
	};

	struct VertexPC
	{
		static const uint32_t Format = VF_POSITION|VF_Color;
		Math::Vector3 position;
		Math::Vector4 color;
	};

	struct VertexPT
	{
		static const uint32_t Format = VF_POSITION | VF_UV;
		Math::Vector3 position;
		Math::Vector2 uv;
	};

	struct Vertex
	{
		static const uint32_t Format = VF_POSITION | VF_UV | VF_NORMAL | VF_Color | VF_Tangent;
		Math::Vector3 position;
		Math::Vector3 normal;
		Math::Vector3 tangent; 
		Math::Vector4 color;
		Math::Vector2 uv;

	};

	struct BoneVertex
	{
		static const uint32_t Format = 
			VF_POSITION | VF_NORMAL | VF_Tangent | VF_Color | VF_UV | VF_BINDEX | VF_BWEIGHT;
		Math::Vector3 position;
		Math::Vector3 normal;
		Math::Vector3 tangent;
		Math::Vector4 color;
		Math::Vector2 uv;
		int boneIndex[4] = {};
		float boneWeight[4] = {};

	};

}



#endif


/**/

//Bitflag

//to set use |
//to check use &
