#ifndef INCLUDED_GRAPHICS_BONEANIMATION_H
#define INCLUDED_GRAPHICS_BONEANIMATION_H

#include "Animation.h"

namespace Graphics {

	struct Bone;

	class BoneAnimation : public Animation
	{
	public:
		BoneAnimation();
		~BoneAnimation();

		Bone* bone;
		uint32_t boneIndex;
	};

	typedef std::vector<BoneAnimation*> BoneAnimations;

}

#endif // #ifndef INCLUDED_GRAPHICS_BONEANIMATION_H