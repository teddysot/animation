#ifndef INCLUDED_GRAPHICS_BONE_H
#define INCLUDED_GRAPHICS_BONE_H

#include <Math\Inc\Matrix.h>

namespace Graphics {

	struct Bone
	{
		Bone() 
			:	index(0) 
			,	parentIndex(-1)
			,	parent(nullptr)
		{}
			
		std::string name;
		uint32_t index;

		int parentIndex;
		std::vector<uint32_t> childrenIndex;

		Bone* parent;
		std::vector<Bone*> children;

		Math::Matrix transform;
		Math::Matrix offsetTransform;
	};

} // namespace Graphics

#endif // #ifndef INCLUDED_GRAPHICS_BONE_H