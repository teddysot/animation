#ifndef INCLUDED_GRAPHICS_TEXTUREMANAGER_H
#define INCLUDED_GRAPHICS_TEXTUREMANAGER_H

namespace Graphics {
	
	class Texture;

	typedef std::size_t TextureId;

	class TextureManager
	{
	public:
		static void StaticInitialize();
		static void StaticTerminate();
		static TextureManager* Get();

	public:
		TextureManager();
		~TextureManager();

		void SetRootPath(const char* root);

		TextureId Load(const char* fileName);

		void BindVS(TextureId id, uint32_t slot = 0);
		void BindPS(TextureId id, uint32_t slot = 0);

	private:
		std::string mRoot;
		std::unordered_map<std::size_t, Texture*> mInventory;
	};
}

#endif // INCLUDED_GRAPHICS_TEXTUREMANAGER_H