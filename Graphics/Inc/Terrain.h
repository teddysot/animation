#ifndef ENGINE_GRAPHICS_TERRAIN_H
#define ENGINE_GRAPHICS_TERRAIN_H

#include "Mesh.h"
#include "MeshBuffer.h"
#include "Transform.h"

namespace Graphics
{

	class Terrain
	{
	public:
		Terrain();
		void Initialize(const char* heightMap, Math::Vector3 scale);
		void Terminate();
		void Render();

		const Math::Vector3 GetScaledDimentions() const;

		Transform mTransform;
	private:
		Math::Vector3 mScale;
		Mesh mTerrainMesh;
		MeshBuffer mTerrainBuffer;
		unsigned char* mMapArray;
		float mWidth;
	};

}

#endif
