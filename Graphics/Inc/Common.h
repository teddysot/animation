#ifndef INCLUDED_GRAPHICS_COMMON_H
#define INCLUDED_GRAPHICS_COMMON_H

// Engine headers
#include <Core/Inc/Core.h>
#include "Math\Inc\EngineMath.h"

#include "TextureManager.h"


// DirectX headers
#include <d3d11_1.h>
#include <d3dcompiler.h>
#include <DirectXMath.h>

// DirectX libraries
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")
#pragma comment(lib, "../External/Assimp/lib32/assimp.lib")

#endif // #ifndef INCLUDED_GRAPHICS_COMMON_H