#ifndef INCLUDED_GRAPHICS_SIMPLEDRAW_H
#define INCLUDED_GRAPHICS_SIMPLEDRAW_H

namespace Graphics {
namespace SimpleDraw {

void StaticInitialize(uint32_t capacity);
void StaticTerminate();

void DrawLine(const Math::Vector3& p0, const Math::Vector3& p1, const Math::Vector4& color);
void DrawLine(float x0, float y0, float z0, float x1, float y1, float z1, const Math::Vector4& color);
void DrawAABB(const Math::AABB& aabb, const Math::Vector4& color);
void DrawAABB(const Math::Vector3& min, const Math::Vector3& max, const Math::Vector4& color);
void DrawAABB(const Math::Vector3& center, float radius, const Math::Vector4& color);
void DrawAABB(float minX, float minY, float minZ, float maxX, float maxY, float maxZ, const Math::Vector4& color);
void DrawOBB(const Math::OBB& obb, const Math::Vector4& color);
void DrawPlane(const Math::Plane& plane, const Math::Vector3& referencePoint, float size, float spacing, const Math::Vector4& color);
void DrawSphere(const Math::Sphere& sphere, const Math::Vector4& color, uint32_t slices = 8, uint32_t rings = 4);
void DrawSphere(const Math::Vector3& center, float radius, const Math::Vector4& color, uint32_t slices = 8, uint32_t rings = 4);
void DrawSphere(float x, float y, float z, float radius, const Math::Vector4& color, uint32_t slices = 8, uint32_t rings = 4);
void DrawTransform(const Math::Matrix& transform);

void Flush(const Math::Matrix& matViewProj);

}}

#endif // #ifndef INCLUDED_GRAPHICS_SIMPLEDRAW_H