#ifndef INCLUDED_GRAPHICS_CAMERA_H
#define INCLUDED_GRAPHICS_CAMERA_H


namespace Graphics
{
	class Transform;
	
	class Camera
	{
	public:
		Camera();
		
		void SetUp(float nearPlane, float farPlane, float fieldOfView);

		void SetFOVLimits(float min, float max);
		void ZoomIn(float rad);
		void ZoomOut(float rad);

		Math::Matrix GetViewMatrix(const Transform& transform);
		Math::Matrix GetProjectionMatrix(float aspectRatio);

	private:
		float mNearPlane;
		float mFarPlane;
		float mFieldOfView;
		float mMinFOV;
		float mMaxFOV;
	};

}

#endif
