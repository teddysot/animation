#ifndef INCLUDED_GRAPHICS_KEYFRAME_H
#define INCLUDED_GRAPHICS_KEYFRAME_H

namespace Graphics {

	struct Keyframe
	{
		float time;
		Math::Vector3 position;
		Math::Vector3 scale;
		Math::Quaternion rotation;
	};

} // namespace Graphics

#endif // #ifndef INCLUDED_GRAPHICS_KEYFRAME_H