#ifndef INCLUDED_GRAPHICS_ANIMATION_H
#define INCLUDED_GRAPHICS_ANIMATION_H

#include "Keyframe.h"
#include "Bone.h"

namespace Graphics {

	class Animation
	{
	public:
		Animation();
		~Animation();

		void AddKeyframe(Keyframe keyframe);
		void SetLooping(bool loop);

		Math::Matrix GetTransform(float time);
		std::vector<Math::Matrix> GetWorldTransform(std::vector<Bone*> bones);

	private:
		typedef std::vector<Keyframe> Keyframes;
		Keyframes mKeyframes;
		bool mLooping;
	};

} // namespace Graphics

#endif // #ifndef INCLUDED_GRAPHICS_ANIMATION_H