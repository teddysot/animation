#ifndef INCLUDED_GRAPHICS_MeshBuffer_H
#define INCLUDED_GRAPHICS_MeshBuffer_H

#include "Mesh.h"
#include "SkinnedMesh.h"

namespace Graphics
{

	class MeshBuffer
	{
	public:
		enum class Topology
		{
			PointList,
			LineList,
			LineStrip,
			TriangleList,
			TriangleStrip
		};

		MeshBuffer();
		~MeshBuffer();
		void Initialize(const void* vertices, uint32_t vertexSize, uint32_t vertexCount);
		void Initialize(const void* vertices, uint32_t vertexSize, uint32_t vertexCount, const uint32_t* indices, uint32_t indexCount);
		void Initialize(const Mesh& mesh);
		void Initialize(const SkinnedMesh& mesh);
		void InitializeDynamic(uint32_t vertexSize, uint32_t vertexCapacity);
		
		void Terminate();

		void SetVertexBuffer(const void* mVertices, uint32_t vertexCount);
		void SetTopology(Topology topology);


		void Render();
	private:
		ID3D11Buffer* mVertexBuffer;
		ID3D11Buffer* mIndexBuffer;

		uint32_t mVertexSize;
		uint32_t mVertexCount;
		uint32_t mVertexCapacity;
		uint32_t mIndexCount;

		D3D_PRIMITIVE_TOPOLOGY mTopology;
	};

}

#endif