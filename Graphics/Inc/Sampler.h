#ifndef ENGINE_GRAPHICS_SAMPLER_H
#define ENGINE_GRAPHICS_SAMPLER_H

namespace Graphics
{
	class Sampler
	{
	public:
		enum class Filter
		{
			Point,
			Linear,
			Anisotropic
		};

		enum class AddressMode
		{
			Border,
			Clamp,
			Mirror,
			Wrap
		};

		Sampler();
		~Sampler();

		void Initialize(Filter filter, AddressMode address);
		void Terminate();

		void BindVS(uint32_t slot);
		void BindPS(uint32_t slot);

		ID3D11SamplerState* mSampler;

	};
}


#endif