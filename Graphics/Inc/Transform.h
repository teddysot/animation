#ifndef INCLUDED_GRAPHICS_TRANSFORM_H
#define INCLUDED_GRAPHICS_TRANSFORM_H

namespace Graphics
{

	class Transform
	{
	public:
		Transform();
		Transform(const Math::Vector3& position, const Math::Vector3& direction);

		//translation
		void Walk(float distance);
		void Strafe(float distance);
		void Rise(float distance);
		
		//rotation
		void Yaw(float rad);
		void Pitch(float rad);

		//scale
		void ScaleBy(const Math::Vector3& s);
		void ScaleBy(float s);

		Math::Matrix GetWorldMatrix() const;

		//mutators
		void SetPostion(const Math::Vector3& position);
		void SetDirection(const Math::Vector3& direction);
		void SetScale(const Math::Vector3& s);

		const Math::Vector3& GetPosition()const { return mPosition; }
		const Math::Vector3& GetDirection()const { return mDirection; }

	private:
		Math::Vector3 mPosition;
		Math::Vector3 mDirection;
		Math::Quaternion mRotation;
		Math::Vector3 mScale;
	};

}

#endif