#ifndef INCLUDED_GRAPHICS_ANIMATIONCLIP_H
#define INCLUDED_GRAPHICS_ANIMATIONCLIP_H

#include "Animation.h"
#include "BoneAnimation.h"

namespace Graphics {

	class AnimationClip
	{
	public:
		AnimationClip();
		~AnimationClip();

		void Play(bool looping);
		void Stop();
		void Reset();

		void Update(float deltaTime);

		std::vector<Math::Matrix> GetTransform();

	private:
		friend class AnimatedModel;

		typedef std::vector<Animation> Animations;
		Animations mBoneAnimations;

		std::string mName;
		float mTicks;
		float mDuration;
		float mTicksPerSecond;
		bool mPlaying;
		bool mLooping;
	};

} // namespace Graphics

#endif // #ifndef INCLUDED_GRAPHICS_ANIMATIONCLIP_H