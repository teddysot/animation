#ifndef ENGINE_GRAPHICS_TEXTURE_H
#define ENGINE_GRAPHICS_TEXTURE_H

namespace Graphics 
{
	
	class Texture
	{
	public:
		Texture();
		~Texture();

		void Initialize(const char* filename);
		void Initialize(const wchar_t* filename);
		void Terminate();

		void BindVS(uint32_t slot);
		void BindPS(uint32_t slot);

	private:
		ID3D11ShaderResourceView* mShaderResourceView;

	};

}



#endif