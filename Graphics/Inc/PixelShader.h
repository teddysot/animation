#ifndef INCLUDED_GRAPHICS_PIXELSHADER_H
#define INCLUDED_GRAPHICS_PIXELSHADER_H

namespace Graphics
{

	class PixelShader
	{
	public:
		PixelShader();
		~PixelShader();
		void Initialize(const wchar_t* fileName);
		void Terminate();
		void Bind();
	private:
		ID3D11PixelShader* mPixelShader;
	};

}

#endif
