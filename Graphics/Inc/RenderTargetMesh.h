#ifndef INCLUDED_GRAPHICS_RENDERTARGETMESH_H
#define INCLUDED_GRAPHICS_RENDERTARGETMESH_H

namespace Graphics {

class RenderTargetMesh
{
public:
	RenderTargetMesh();
	~RenderTargetMesh();

	void Initialize();
	void Render();

private:
	uint32_t mVertexSize;
	uint32_t mVertexCount;
	uint32_t mVertexCapacity;
	uint32_t mIndexCount;
	ID3D11Buffer* mVertexBuffer;
};

} // namespace Graphics

#endif // #ifndef INCLUDED_GRAPHICS_RENDERTARGETMESH_H