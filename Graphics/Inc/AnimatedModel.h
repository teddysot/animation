#ifndef INCLUDED_GRAPHICS_ANIMATEDMODEL_H
#define INCLUDED_GRAPHICS_ANIMATEDMODEL_H

#include "AnimationClip.h"
#include "SimpleDraw.h"

namespace Graphics
{
	struct Bone;
	class SkinnedMesh;
	class MeshBuffer;
	class Texture;

	class AnimatedModel
	{
	public:

		AnimatedModel();
		~AnimatedModel();

		void Load(const char* filename);
		void Unload();

		void Play(bool looping);
		void Stop();
		void Reset();

		void Render();

		void RenderSkeleton();
		void RenderSkeletonAnimation();

		std::vector<Math::Matrix> GetBoneWorldTransforms() const;

		std::vector<Bone*> mSkeleton;

	private:
		std::vector<SkinnedMesh*> mMeshes;
		std::vector<uint32_t> mMaterialIndex;
		std::vector<MeshBuffer*> mMeshBuffers;
		std::vector<Texture*> mTextures;
		std::vector<TextureId> mTexturesId;
		std::vector<AnimationClip> mAnimationClips;

		Bone* mRoot;
	};
}
#endif // INCLUDED_GRAPHICS_ANIMATEDMODEL_H
