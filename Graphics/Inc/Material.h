#ifndef ENGINE_GRAPHICS_MATERIAL_H
#define ENGINE_GRAPHICS_MATERIAL_H

#include "Math\Inc\Vector4.h"

namespace Graphics
{
	struct Material
	{
		Material()
			: ambient(Math::Vector4::White())
			, diffuse(Math::Vector4::White())
			, specular(Math::Vector4::White())
			, power(1.0f)
		{}

		Math::Vector4 ambient;
		Math::Vector4 diffuse;
		Math::Vector4 specular;
		float power;
	};
}

#endif
