#ifndef INCLUDED_GRAPHICS_MODEL_H
#define INCLUDED_GRAPHICS_MODEL_H

namespace Graphics
{
	class Mesh;
	class MeshBuffer;
	class Texture;

	class Model
	{
	public:

		Model();
		~Model();
		void Load(const char* filename);
		void Unload();

		void Render();

	private:
		std::vector<Mesh*> mMeshes;
		std::vector<uint32_t> mMaterialIndex;
		std::vector<MeshBuffer*> mMeshBuffers;
		std::vector<Texture*> mTextures;
		std::vector<TextureId> mTexturesId;

		uint32_t numMeshes = 0;
		uint32_t numVertices = 0;
		uint32_t numIndices = 0;
		uint32_t numMaterial = 0;
		uint32_t materialIndex = 0;

		TextureId mID;
		TextureManager* texManager;
	};
}
#endif // INCLUDED_GRAPHICS_MODEL_H
