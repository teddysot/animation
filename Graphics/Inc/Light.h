#ifndef ENGINE_GRAPHICS_LIGHT_H
#define ENGINE_GRAPHICS_LIGHT_H

#include <Math\Inc\Vector4.h>
#include <Math\Inc\Vector3.h>

namespace Graphics
{

	struct Light //TODO:: add to graphics (Graphics::Light)
	{
		
		Light() : direction(Math::Vector3::ZAxis())
			, ambient(Math::Vector4::White())
			, diffuse(Math::Vector4::White())
			, specular(Math::Vector4::White())
		{}

		//Math::Vector3 position;
		Math::Vector3 direction;
		float padding;
		Math::Vector4 ambient;
		Math::Vector4 diffuse;
		Math::Vector4 specular;
	};

}

#endif