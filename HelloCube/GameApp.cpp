#include "GameApp.h"

#include "Math\Inc\EngineMath.h"

namespace //internal linkage
{

	const Graphics::VertexPC kVertices[] =
	{
		{ Math::Vector3(-0.5f,+0.5f,-0.5f), Math::Vector4(0.0f, 0.1f, 0.7f, 1.0f) },//00
		{ Math::Vector3(+0.5f,+0.5f,-0.5f), Math::Vector4(0.5f, 0.2f, 0.4f, 1.0f) },//41
		{ Math::Vector3(+0.5f,-0.5f,-0.5f), Math::Vector4(0.7f, 0.3f, 0.3f, 1.0f) },//52
		{ Math::Vector3(-0.5f,-0.5f,-0.5f), Math::Vector4(0.8f, 0.4f, 0.7f, 1.0f) },//63
		{ Math::Vector3(-0.5f,+0.5f,+0.5f), Math::Vector4(0.9f, 0.5f, 0.2f, 1.0f) },//04
		{ Math::Vector3(+0.5f,+0.5f,+0.5f), Math::Vector4(0.3f, 0.6f, 0.1f, 1.0f) },//45
		{ Math::Vector3(+0.5f,-0.5f,+0.5f), Math::Vector4(0.6f, 0.7f, 0.0f, 1.0f) },//56
		{ Math::Vector3(-0.5f,-0.5f,+0.5f), Math::Vector4(0.1f, 0.8f, 0.9f, 1.0f) },//67




	};

	const int kVertexCount = sizeof(kVertices) / sizeof(kVertices[0]);

	const uint32_t kIndices[] =
	{
		0,1,2,2,3,0,0,4,5,0,5,1,2,6,7,2,7,3,4,7,6,4,6,5,0,3,7,4,0,7,1,5,6,1,6,2
	};

	const int kIndexCount = sizeof(kIndices) / sizeof(kIndices[0]);


}


GameApp::GameApp()
{}

GameApp::~GameApp()
{}

void GameApp::OnInitialize(uint32_t width, uint32_t height)
{
	mWindow.Initialize(GetInstance(), GetAppName(), width, height);
	HookWindow(mWindow.GetWindowHandle());

	mTimer.Initialize();

	Graphics::GraphicsSystem::StaticInitialize(mWindow.GetWindowHandle(), false);
	Graphics::SimpleDraw::StaticInitialize(100000);
	Input::InputSystem::StaticInitialize(mWindow.GetWindowHandle());

	mCameraTransform.SetPostion(Math::Vector3(0.0f, 2.0f, -10.0f));
	mCameraTransform.SetDirection(Math::Vector3(0.0f, 0.0f, 1.0f));

	mGlobeConstantBuffer.Initialize();
	mMeshVS.Initialize(L"../Assets/Shaders/Transform.fx", Graphics::VertexPC::Format);
	mMeshPS.Initialize(L"../Assets/Shaders/Transform.fx");

	mMesh.Initialize(kVertices, sizeof(Graphics::VertexPC), kVertexCount, kIndices, kIndexCount);

}

void GameApp::OnTerminate()
{

	Graphics::GraphicsSystem::StaticTerminate();
	Graphics::SimpleDraw::StaticTerminate();
	Input::InputSystem::StaticTerminate();

	mMesh.Terminate();

	mMeshVS.Terminate();
	mMeshPS.Terminate();
	mGlobeConstantBuffer.Terminate();

	UnhookWindow();
	mWindow.Terminate();
}

void GameApp::OnUpdate()
{
	if (mWindow.ProcessMessage())
	{
		Kill();
	}

	mTimer.Update();

	Input::InputSystem* is = Input::InputSystem::Get();
	is->Update();

	if (is->IsKeyPressed(Keys::ESCAPE))
	{
		PostQuitMessage(0);
	}

	Math::Matrix mRotateX;
	Math::Matrix mRotateY;
	Math::Matrix mRotateZ;

	Graphics::GraphicsSystem* gs = Graphics::GraphicsSystem::Get();
	gs->BeginRender(Math::Vector4::Vector4(0.0f, 0.0f, 0.0f, 0.0f));


	float cameraMoveSpeed = 10.0f;
	float cameraRotationSpeed = 0.3f;
	float cameraSpeedValue = 2.0f;

	mCameraTransform.Pitch(((float)is->GetMouseMoveY() * cameraRotationSpeed * mTimer.GetElapsedTime()));
	mCameraTransform.Yaw(((float)is->GetMouseMoveX() * cameraRotationSpeed * mTimer.GetElapsedTime()));

	Math::Vector3 movement;

	if (is->IsKeyDown(Keys::W))
	{
		movement.z = cameraMoveSpeed;
	}
	else if (is->IsKeyDown(Keys::S))
	{
		movement.z = -cameraMoveSpeed;
	}

	if (is->IsKeyDown(Keys::D))
	{
		movement.x = cameraMoveSpeed;
	}
	else if (is->IsKeyDown(Keys::A))
	{
		movement.x = -cameraMoveSpeed;
	}

	if (is->IsKeyDown(Keys::LSHIFT))
	{
		movement *= cameraSpeedValue;
	}

	if (is->IsKeyDown(Keys::NUM_ADD))
	{
		mSphereSize += 0.5f;
	}

	if (is->IsKeyDown(Keys::NUM_SUB))
	{
		if (mSphereSize - 0.5f >= 0)
		{
			mSphereSize -= 0.5f;
		}
	}

	if (is->IsKeyPressed(Keys::NUMPAD7))
	{
		mSphereRings += 2;
	}

	if (is->IsKeyPressed(Keys::NUMPAD4))
	{
		if (mSphereRings - 2 > 0)
		{
			mSphereRings -= 2;
		}
	}

	if (is->IsKeyPressed(Keys::NUMPAD8))
	{
		mSphereSlices += 2;
	}

	if (is->IsKeyPressed(Keys::NUMPAD5))
	{
		if (mSphereSlices - 2 > 0)
		{
			mSphereSlices -= 2;
		}
	}

	if (is->IsKeyPressed(Keys::NUMPAD9))
	{
		mSphereSlices += 2;
		mSphereRings += 2;

	}

	if (is->IsKeyPressed(Keys::NUMPAD6))
	{
		if (mSphereSlices - 2 > 0)
		{
			mSphereSlices -= 2;
		}
		if (mSphereRings - 2 > 0)
		{
			mSphereRings -= 2;
		}
	}


	mCameraTransform.Walk(movement.z * mTimer.GetElapsedTime());
	mCameraTransform.Strafe(movement.x * mTimer.GetElapsedTime());



	//stay the same
	Math::Matrix viewMatrix = mCamera.GetViewMatrix(mCameraTransform);
	Math::Matrix projectionMatrix = mCamera.GetProjectionMatrix(gs->GetAspectRatio());
	mMeshVS.Bind();
	mMeshPS.Bind();
	//


	//RENDER STUFF

	Math::Matrix translation;
	Math::Matrix rotation;
	Math::Matrix scale;

	for (int i = 0; i < 1000; i++)
	{


		Math::Matrix worldMatrix;

		//rotation = Math::Matrix::RotationY(mTimer.GetTotalTime() * i * 0.001f);
		rotation = Math::Matrix::RotationZ(mTimer.GetTotalTime() * i * 0.001f);
		//rotation += Math::Matrix::RotationY(mTimer.GetTotalTime() * i * 0.05f);
		//rotation += Math::Matrix::RotationY(mTimer.GetTotalTime() * i * 0.005f);
		translation = Math::Matrix::Translation(-0.001 * i, i * 0.1f, 0.3*i);
		//scale = Math::Matrix::Scaling(0.01*i);
		worldMatrix = (rotation * translation * rotation);

		ConstantData data;
		data.wvp = Math::Transpose(worldMatrix * viewMatrix * projectionMatrix);
		mGlobeConstantBuffer.Set(data);
		mGlobeConstantBuffer.BindVS();

		mMesh.Render();
	}


	for (int z = 0; z <= 100; z++)
	{
		Graphics::SimpleDraw::DrawLine(Math::Vector3(-50.0f, -0.1f, -50.0f + z), Math::Vector3(50.0f, -0.1f, -50.0f + z), Math::Vector4(0.5f, 0.5f, 0.5f, 1.0f));
	}

	for (int x = 0; x <= 100; x++)
	{
		Graphics::SimpleDraw::DrawLine(Math::Vector3(-50.0f + x, -0.1f, -50.0f), Math::Vector3(-50.0f + x, -0.1f, 50.0f), Math::Vector4(0.5f, 0.5f, 0.5f, 1.0f));
	}

	Graphics::SimpleDraw::DrawLine(Math::Vector3(0.0f, 0.0f, 0.0f), Math::Vector3(1.0f, 0.0f, 0.0f), Math::Vector4(1.0f, 0.0f, 0.0f, 1.0f));
	Graphics::SimpleDraw::DrawLine(Math::Vector3(0.0f, 0.0f, 0.0f), Math::Vector3(0.0f, 1.0f, 0.0f), Math::Vector4(0.0f, 1.0f, 0.0f, 1.0f));
	Graphics::SimpleDraw::DrawLine(Math::Vector3(0.0f, 0.0f, 0.0f), Math::Vector3(0.0f, 0.0f, 1.0f), Math::Vector4(0.0f, 0.0f, 1.0f, 1.0f));

	Graphics::SimpleDraw::DrawSphere(Math::Vector3(5, 10, 5), 1, 18, 18, Math::Vector4::White());
	Graphics::SimpleDraw::DrawSphere(Math::Vector3(0, 10, 0), 2.73f, 36, 36, Math::Vector4::Green());

	Graphics::SimpleDraw::DrawSphere(Math::Vector3(10, 5, 10), mSphereSize, mSphereRings, mSphereSlices, Math::Vector4::Red());

	Graphics::SimpleDraw::DrawSquare(Math::Vector3(-10, 10, 0), Math::Vector3(3, 3, 5), Math::Vector4::Blue());

	Graphics::SimpleDraw::Flush(viewMatrix * projectionMatrix);


	gs->EndRender();

}
