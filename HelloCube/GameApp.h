#ifndef INCLUDED_GAMEAPP
#define INCLUDED_GAMEAPP


#include <Core\Inc\Core.h>
#include <Graphics\Inc\Graphics.h>
#include <Input\Inc\Input.h>

class GameApp : public Core::Application
{
public:
	GameApp();
	~GameApp() override;

private:
	
	struct ConstantData
	{
		Math::Matrix wvp;
	};

	void OnInitialize(uint32_t width, uint32_t height) override;
	void OnTerminate() override;
	void OnUpdate() override;
	
	Core::Window mWindow;
	Core::Timer mTimer;

	Graphics::Camera mCamera;
	Graphics::Transform mCameraTransform;
	
	Graphics::TypedConstantBuffer<ConstantData> mGlobeConstantBuffer;
	Graphics::VertexShader mMeshVS;
	Graphics::PixelShader mMeshPS;

	Graphics::MeshBuffer mMesh; //make it mesh buffer later
	
	Math::Vector3 mRotation;

	float mSphereSize = 1.0f;
	float mSphereRings = 3.0f;
	float mSphereSlices = 3.0f;

};

#endif