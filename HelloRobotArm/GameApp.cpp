#include "GameApp.h"

#include "Math\Inc\EngineMath.h"
#include <fstream>
#include <iostream>

GameApp::GameApp() :
	mCurrentCamera(&mCamera),
	mCurrentCameraTransform(&mCameraTransform)
{}

GameApp::~GameApp()
{}

void GameApp::OnInitialize(uint32_t width, uint32_t height)
{
	mWindow.Initialize(GetInstance(), GetAppName(), width, height);
	HookWindow(mWindow.GetWindowHandle());

	mTimer.Initialize();

	Graphics::GraphicsSystem::StaticInitialize(mWindow.GetWindowHandle(), false);
	Graphics::TextureManager::StaticInitialize();
	Graphics::SimpleDraw::StaticInitialize(100000);
	Input::InputSystem::StaticInitialize(mWindow.GetWindowHandle());

	mCameraTransform.SetPostion(Math::Vector3(0.0f, 5.0f, -10.0f));
	mCameraTransform.SetDirection(Math::Vector3(0.0f, 0.0f, 1.0f));

	mLightPosition = Math::Vector3(0.0f, 500.0f, -100.0f);
	mLightTarget = Math::Vector3(0.0f, 0.0f, 0.0f);

	Math::Vector3 lightDirection = Math::Normalize(mLightTarget - mLightPosition);
	mLightCameraTransform.SetPostion(Math::Vector3(mLightPosition));
	mLightCameraTransform.SetDirection(Math::Normalize(Math::Vector3(lightDirection)));

	mLight.direction = Math::Normalize(Math::Vector3(lightDirection));
	mLight.ambient = Math::Vector4(0.1f, 0.1f, 0.1f, 1.0f);
	mLight.diffuse = Math::Vector4(1.0f, 1.0f, 1.0f, 1.0f);
	mLight.specular = Math::Vector4(1.0f, 1.0f, 1.0f, 1.0f);

	mMaterial.ambient = Math::Vector4(1.0f, 1.0f, 1.0f, 1.0f);
	mMaterial.diffuse = Math::Vector4(0.5f, 0.5f, 0.5f, 1.0f);
	mMaterial.specular = Math::Vector4(0.5f, 0.5f, 0.5f, 1.0f);
	mMaterial.power = 20;

	mSampler.Initialize(Graphics::Sampler::Filter::Anisotropic, Graphics::Sampler::AddressMode::Clamp);
	mShadowSampler.Initialize(Graphics::Sampler::Filter::Anisotropic, Graphics::Sampler::AddressMode::Border);

	// Mesh Shader
	mMeshVS.Initialize(L"../Assets/Shaders/ShadowMapping.fx", Graphics::Vertex::Format);
	mMeshPS.Initialize(L"../Assets/Shaders/ShadowMapping.fx");
	mConstantBuffer.Initialize();

	// Model
	mModelVS.Initialize(L"../Assets/Shaders/Model.fx", Graphics::Vertex::Format);
	mModelPS.Initialize(L"../Assets/Shaders/Model.fx");

	// Load Models
	duckModel.Load("../Assets/Models/duck.txt");
	houseModel.Load("../Assets/Models/Castle.txt");

	mDuckAnimation.SetLooping(true);

	keyframe.time = 0.0f;
	keyframe.position = Math::Vector3(10, 0, -5);
	keyframe.scale = Math::Vector3(0.1f);
	keyframe.rotation = Math::Quaternion::RotationAxis(Math::Vector3::YAxis(), Math::kDegToRad * 10);
	mDuckAnimation.AddKeyframe(keyframe);

	keyframe.time = 1.0f;
	keyframe.position = Math::Vector3(20, 0, 0);
	keyframe.scale = Math::Vector3(0.2f);
	keyframe.rotation = Math::Quaternion::RotationAxis(Math::Vector3::YAxis(), Math::kDegToRad * 90);
	mDuckAnimation.AddKeyframe(keyframe);

	keyframe.time = 2.0f;
	keyframe.position = Math::Vector3(30, 0, 5);
	keyframe.scale = Math::Vector3(0.1f);
	keyframe.rotation = Math::Quaternion::RotationAxis(Math::Vector3::YAxis(), Math::kDegToRad * 10);
	mDuckAnimation.AddKeyframe(keyframe);

	keyframe.time = 3.0f;
	keyframe.position = Math::Vector3(40, 0, 0);
	keyframe.scale = Math::Vector3(0.2f);
	keyframe.rotation = Math::Quaternion::RotationAxis(Math::Vector3::YAxis(), Math::kDegToRad * 90);
	mDuckAnimation.AddKeyframe(keyframe);

	keyframe.time = 4.0f;
	keyframe.position = Math::Vector3(50, 0, 5);
	keyframe.scale = Math::Vector3(0.1f);
	keyframe.rotation = Math::Quaternion::RotationAxis(Math::Vector3::YAxis(), Math::kDegToRad * 10);
	mDuckAnimation.AddKeyframe(keyframe);

	// Cube
	mBuilder.CreateCube(mCubeMesh);
	mCubeMeshBuffer.Initialize(mCubeMesh);

	for (int i = 0; i < 3; ++i)
	{
		Graphics::Bone* bone = new Graphics::Bone();
		bone->parent = (i == 0) ? nullptr : mBones[i - 1];

		mBones.push_back(bone);
		mBoneOffsets.push_back((i == 0) ? 0.0f : 2.0f);
		mBoneAngles.push_back(0.1f * i);
		mBoneWorldTransform.push_back(Math::Matrix::Identity());
	}

	Math::Matrix matScale = Math::Matrix::Scaling(0.2f, 1.0f, 0.2f);
	Math::Matrix matTrans = Math::Matrix::Translation(0.0f, 1.0f, 0.0f);
	mBoneOffsetTransform = matScale * matTrans;

	// Globe
	mGlobeDiffuse.Initialize(L"../Assets/Images/earth.jpg");
	mGlobeSpecular.Initialize(L"../Assets/Images/earth_spec.jpg");
	mGlobeNormal.Initialize(L"../Assets/Images/earth_normal.jpg");
	mGlobeBump.Initialize(L"../Assets/Images/earth_bump.jpg");

	mGlobeMesh.Instantiate(10000);
	mBuilder.CreateSphere(20.0f, 40, 40, mGlobeMesh);
	mGlobeBuffer.SetTopology(Graphics::MeshBuffer::Topology::TriangleList);
	mGlobeBuffer.Initialize(mGlobeMesh);

	// Moon
	mMoonDiffuse.Initialize(L"../Assets/Images/moon.jpg");
	mMoonNormal.Initialize(L"../Assets/Images/moon_normal.jpg");

	mMoonMesh.Instantiate(10000);
	mBuilder.CreateSphere(5.0f, 40, 40, mMoonMesh);
	mMoonBuffer.SetTopology(Graphics::MeshBuffer::Topology::TriangleList);
	mMoonBuffer.Initialize(mMoonMesh);

	mPlaneDiffuse.Initialize(L"../Assets/Images/brickwork.jpg");
	mPlaneSpecular.Initialize(L"../Assets/Images/brickwork_spec.jpg");
	mPlaneNormal.Initialize(L"../Assets/Images/brickwork_normal.jpg");
	mPlaneBump.Initialize(L"../Assets/Images/brickwork_bump.jpg");
	mPlaneMesh.Instantiate(10000);
	mBuilder.CreatePlane(Math::Vector2(100, 100), mPlaneMesh);
	mPlaneBuffer.SetTopology(Graphics::MeshBuffer::Topology::TriangleList);
	mPlaneBuffer.Initialize(mPlaneMesh);

	// Skybox
	//mSkyboxVS.Initialize(L"../Assets/Shaders/Skybox.fx", Graphics::Vertex::Format);
	//mSkyboxPS.Initialize(L"../Assets/Shaders/Skybox.fx");

	// DepthMap
	mDepthVS.Initialize(L"../Assets/Shaders/DepthMap.fx", Graphics::Vertex::Format);
	mDepthMap.Initialize(width, height);
	mDepthConstantBuffer.Initialize();

	// RenderTarget
	mRenderTargetVS.Initialize(L"../Assets/Shaders/PostProcessing.fx", Graphics::Vertex::Format);
	mRenderTargetPS.Initialize(L"../Assets/Shaders/PostProcessing.fx");
	mRenderTargetConstantBuffer.Initialize();
	mRenderTargetMesh.Instantiate(10000);
	mBuilder.CreateQuad(mRenderTargetMesh);
	mRenderTargetBuffer.SetTopology(Graphics::MeshBuffer::Topology::TriangleList);
	mRenderTargetBuffer.Initialize(mRenderTargetMesh);

	mRenderTarget.Initialize(width, height, Graphics::RenderTarget::Format::RGBA_U8);

	//mRenderTargetMesh.Initialize();
}

void GameApp::OnTerminate()
{

	Graphics::GraphicsSystem::StaticTerminate();
	Graphics::TextureManager::StaticTerminate();
	Graphics::SimpleDraw::StaticTerminate();
	Input::InputSystem::StaticTerminate();

	// Terrain
	//mTerrainTexture.Terminate();
	//mTerrainSpecular.Terminate();
	//mTerrain.Terminate();

	// Plane
	mPlaneDiffuse.Terminate();
	mPlaneSpecular.Terminate();
	mPlaneNormal.Terminate();
	mPlaneBump.Terminate();
	mPlaneMesh.Terminate();

	// Arm
	SafeDeleteVector(mBones);

	mCubeMeshBuffer.Terminate();
	mCubeMesh.Terminate();

	// Globe
	mGlobeDiffuse.Terminate();
	mGlobeSpecular.Terminate();
	mGlobeNormal.Terminate();
	mGlobeBump.Terminate();
	mGlobeMesh.Terminate();

	// Moon
	mMoonDiffuse.Terminate();
	mMoonNormal.Terminate();
	mMoonMesh.Terminate();

	// Skybox
	mSkyboxTexture.Terminate();

	// Quad
	mRenderTargetMesh.Terminate();

	// Shader
	mSampler.Terminate();
	mShadowSampler.Terminate();

	mMeshVS.Terminate();
	mMeshPS.Terminate();

	mModelVS.Terminate();
	mModelPS.Terminate();

	//mTerrainVS.Terminate();
	//mTerrainPS.Terminate();

	mSkyboxVS.Terminate();
	mSkyboxPS.Terminate();

	mRenderTargetVS.Terminate();
	mRenderTargetPS.Terminate();

	mConstantBuffer.Terminate();
	mDepthConstantBuffer.Terminate();
	mSkyboxConstantBuffer.Terminate();
	mRenderTargetConstantBuffer.Terminate();

	duckModel.Unload();
	houseModel.Unload();
	mModelTexture.Terminate();

	mDepthMap.Terminate();
	mDepthVS.Terminate();

	// Render Target
	mRenderTarget.Terminate();

	UnhookWindow();
	mWindow.Terminate();
}

void GameApp::OnUpdate()
{
	if (mWindow.ProcessMessage())
	{
		Kill();
	}

	mTimer.Update();

	Input::InputSystem* is = Input::InputSystem::Get();
	is->Update();

	if (is->IsKeyPressed(Keys::ESCAPE))
	{
		PostQuitMessage(0);
	}

	float cameraMoveSpeed = 20.0f;
	float cameraRotationSpeed = 0.3f;
	float cameraSpeedValue = 10.0f;
	float cameraRiseValue = 10.0f;

	if (is->IsMouseDown(1))
	{
		mCameraTransform.Pitch(((float)is->GetMouseMoveY() * cameraRotationSpeed * mTimer.GetElapsedTime()));
		mCameraTransform.Yaw(((float)is->GetMouseMoveX() * cameraRotationSpeed * mTimer.GetElapsedTime()));
	}

	Math::Vector3 movement;

	if (is->IsKeyDown(Keys::W))
	{
		movement.z = cameraMoveSpeed;
	}
	else if (is->IsKeyDown(Keys::S))
	{
		movement.z = -cameraMoveSpeed;
	}

	if (is->IsKeyDown(Keys::D))
	{
		movement.x = cameraMoveSpeed;
	}
	else if (is->IsKeyDown(Keys::A))
	{
		movement.x = -cameraMoveSpeed;
	}

	if (is->IsKeyDown(Keys::Q))
	{
		cameraRiseValue = -cameraSpeedValue;
	}
	else if (is->IsKeyDown(Keys::E))
	{
		cameraRiseValue = cameraSpeedValue;
	}
	else
	{
		cameraRiseValue = 0.0f;
	}

	if (is->IsKeyDown(Keys::LSHIFT))
	{
		movement *= cameraSpeedValue;
		cameraRiseValue *= cameraSpeedValue;
	}

	if (is->IsKeyDown(Keys::MINUS))
	{
		mCurrentCamera = &mCamera;
		mCurrentCameraTransform = &mCameraTransform;
	}

	if (is->IsKeyDown(Keys::EQUALS))
	{
		mCurrentCamera = &mLightCamera;
		mCurrentCameraTransform = &mLightCameraTransform;
	}

	mCurrentCameraTransform->Walk(movement.z * mTimer.GetElapsedTime());
	mCurrentCameraTransform->Strafe(movement.x * mTimer.GetElapsedTime());
	mCurrentCameraTransform->Rise(cameraRiseValue * mTimer.GetElapsedTime());

	if (is->IsKeyDown(Keys::ZERO))
	{
		postNumber = 0;
	}
	if (is->IsKeyDown(Keys::ONE))
	{
		postNumber = 1;
	}
	if (is->IsKeyDown(Keys::TWO))
	{
		postNumber = 2;
	}
	if (is->IsKeyDown(Keys::THREE))
	{
		postNumber = 3;
	}
	if (is->IsKeyDown(Keys::FOUR))
	{
		postNumber = 4;
	}
	if (is->IsKeyDown(Keys::FIVE))
	{
		postNumber = 5;
	}
	if (is->IsKeyDown(Keys::SIX))
	{
		postNumber = 6;
	}
	if (is->IsKeyDown(Keys::SEVEN))
	{
		postNumber = 7;
	}
	if (is->IsKeyDown(Keys::EIGHT))
	{
		postNumber = 8;
	}
	if (is->IsKeyDown(Keys::NINE))
	{
		postNumber = 9;
	}

	// Arm Control
	if (is->IsKeyDown(Keys::I))
	{
		mBoneAngles[2] += mTimer.GetElapsedTime();
	}
	if (is->IsKeyDown(Keys::O))
	{
		mBoneAngles[2] -= mTimer.GetElapsedTime();
	}
	if (is->IsKeyDown(Keys::J))
	{
		mBoneAngles[1] += mTimer.GetElapsedTime();
	}
	if (is->IsKeyDown(Keys::K))
	{
		mBoneAngles[1] -= mTimer.GetElapsedTime();
	}
	if (is->IsKeyDown(Keys::N))
	{
		mBoneAngles[0] += mTimer.GetElapsedTime();
	}
	if (is->IsKeyDown(Keys::M))
	{
		mBoneAngles[0] -= mTimer.GetElapsedTime();
	}

	Graphics::GraphicsSystem* gs = Graphics::GraphicsSystem::Get();

	// Update bone transofrms
	for (int i = 0; i < 3; ++i)
	{
		Math::Matrix matRot = Math::Matrix::RotationZ(mBoneAngles[i]);
		Math::Matrix matTrans = Math::Matrix::Translation(0.0f, mBoneOffsets[i], 0.0f);
		mBones[i]->transform = matRot * matTrans;
	}
	for (int i = 0; i < 3; ++i)
	{
		Math::Matrix parentTransform = Math::Matrix::Identity();
		Graphics::Bone* parent = mBones[i]->parent;
		while (parent)
		{
			parentTransform = parentTransform * parent->transform;
			parent = parent->parent;
		}
		mBoneWorldTransform[i] = mBones[i]->transform * parentTransform;
	}

	GenerateDepthMap();

	mRenderTarget.BeginRender();
	DrawScene();
	mRenderTarget.EndRender();

	gs->BeginRender();

	PostProcessingData postData;
	postData.fnum = postNumber;
	mRenderTargetConstantBuffer.Set(postData);
	mRenderTargetConstantBuffer.BindVS();
	mRenderTargetConstantBuffer.BindPS();
	mRenderTargetVS.Bind();
	mRenderTargetPS.Bind();

	mRenderTarget.BindPS(0);

	//mRenderTargetMesh.Render();
	mRenderTargetBuffer.Render();

	// Use post-processing shaders
	// Set sampler, constant buufer
	// Draw a quad, quad need position and uv

	mRenderTarget.UnbindPS(0);

	gs->EndRender();
}

void GameApp::GenerateDepthMap()
{
	mDepthMap.BeginRender();

	Math::Matrix matLightView = Math::Inverse(mLightCameraTransform.GetWorldMatrix());
	Math::Matrix matLightProj = mLightCamera.GetProjectionMatrix(1.0f);

	mSampler.BindVS(0);
	mShadowSampler.BindVS(1);

	mDepthVS.Bind();
	mDepthPS.Bind();

	Math::Matrix globeRY = Math::Matrix::RotationY(mTimer.GetTotalTime() * 0.25f);
	Math::Matrix globeTran = Math::Matrix::Translation(0.0f, 50.0f, 0.0f);
	Math::Matrix globeScale = Math::Matrix::Scaling(1.0f);
	Math::Matrix globeWorldMatrix = globeScale * globeRY * globeTran;
	ConstantData depthData;
	depthData.world = Math::Transpose(globeWorldMatrix);
	depthData.wvp = Math::Transpose(globeWorldMatrix * matLightView * matLightProj);
	depthData.wvpLight = Math::Transpose(globeWorldMatrix * matLightView * matLightProj);
	mDepthConstantBuffer.Set(depthData);
	mDepthConstantBuffer.BindVS();

	mGlobeBuffer.Render();

	Math::Matrix moonRY = Math::Matrix::RotationY(mTimer.GetTotalTime() * 0.1f);
	Math::Matrix moonTran = Math::Matrix::Translation(0.0f, 50.0f, 0.0f);
	Math::Matrix moonOrbit = Math::Matrix::Translation(cos(mTimer.GetTotalTime() * 0.1f) * 40.0f, 0.0f, sin(mTimer.GetTotalTime() * 0.1f) * 40.0f); // Synchronous
	Math::Matrix moonWorldMatrix = moonRY * moonOrbit * moonTran;
	depthData.world = Math::Transpose(moonWorldMatrix);
	depthData.wvp = Math::Transpose(moonWorldMatrix * matLightView * matLightProj);
	depthData.wvpLight = Math::Transpose(moonWorldMatrix * matLightView * matLightProj);
	mDepthConstantBuffer.Set(depthData);
	mDepthConstantBuffer.BindVS();

	mMoonBuffer.Render();

	Math::Matrix planeScaling = Math::Matrix::Scaling(5.0f);
	Math::Matrix planeWorldMatrix = planeScaling;
	depthData.world = Math::Transpose(planeWorldMatrix);
	depthData.wvp = Math::Transpose(planeWorldMatrix * matLightView * matLightProj);
	depthData.wvpLight = Math::Transpose(planeWorldMatrix * matLightView * matLightProj);
	depthData.displacementScale = 2.0f;
	mDepthConstantBuffer.Set(depthData);
	mDepthConstantBuffer.BindVS();

	mPlaneBuffer.Render();

	//Math::Matrix duckRY = Math::Matrix::RotationY(mTimer.GetTotalTime() * 0.25f);
	//Math::Matrix duckTran = Math::Matrix::Translation(0.0f, 50.0f, 0.0f);
	//Math::Matrix duckScale = Math::Matrix::Scaling(0.01f);
	//Math::Matrix duckWorldMatrix = duckScale * duckRY * duckTran;
	Math::Matrix duckWorldMatrix = mDuckAnimation.GetTransform(mTimer.GetTotalTime() * 0.25f);
	depthData.world = Math::Transpose(duckWorldMatrix);
	depthData.wvp = Math::Transpose(duckWorldMatrix * matLightView * matLightProj);
	depthData.wvpLight = Math::Transpose(duckWorldMatrix * matLightView * matLightProj);
	mDepthConstantBuffer.Set(depthData);
	mDepthConstantBuffer.BindVS();

	duckModel.Render();

	Math::Matrix houseRY = Math::Matrix::RotationY(mTimer.GetTotalTime() * 0.25f);
	Math::Matrix houseTran = Math::Matrix::Translation(0.0f, 50.0f, 0.0f);
	Math::Matrix houseScale = Math::Matrix::Scaling(0.01f);
	Math::Matrix houseWorldMatrix = houseScale * houseRY * houseTran;
	depthData.world = Math::Transpose(houseWorldMatrix);
	depthData.wvp = Math::Transpose(houseWorldMatrix * matLightView * matLightProj);
	depthData.wvpLight = Math::Transpose(houseWorldMatrix * matLightView * matLightProj);
	mDepthConstantBuffer.Set(depthData);
	mDepthConstantBuffer.BindVS();

	houseModel.Render();

	// Draw arm
	for (int i = 0; i < 3; ++i)
	{
		Math::Matrix armWorldMatrix = mBoneOffsetTransform * mBoneWorldTransform[i];
		depthData.wvp = Math::Transpose(armWorldMatrix * matLightView * matLightProj);
		depthData.wvpLight = Math::Transpose(armWorldMatrix * matLightView * matLightProj);
		mDepthConstantBuffer.Set(depthData);
		mDepthConstantBuffer.BindVS();
		mCubeMeshBuffer.Render();
	}

	mDepthMap.EndRender();
}

void GameApp::DrawScene()
{
	Graphics::GraphicsSystem* gs = Graphics::GraphicsSystem::Get();

	// Rendering
	Math::Matrix viewMatrix = mCurrentCamera->GetViewMatrix(*mCurrentCameraTransform);
	Math::Matrix projectionMatrix = mCurrentCamera->GetProjectionMatrix(gs->GetAspectRatio());

	mMeshVS.Bind();
	mMeshPS.Bind();

	mSampler.BindPS(0);
	mShadowSampler.BindPS(1);

	//------------------------------
	ConstantData data;
	data.viewPosition = mCurrentCameraTransform->GetPosition();
	data.light = mLight;
	data.material = mMaterial;
	mConstantBuffer.BindVS();
	mConstantBuffer.BindPS();

	Math::Matrix matLightView = mLightCamera.GetViewMatrix(mLightCameraTransform);
	Math::Matrix matLightProj = mLightCamera.GetProjectionMatrix(1);

	// Arm
	for (int i = 0; i < 3; ++i)
	{
		Math::Matrix armWorldMatrix = mBoneOffsetTransform * mBoneWorldTransform[i];
		data.world = Math::Transpose(armWorldMatrix);
		data.wvp = Math::Transpose(armWorldMatrix * viewMatrix * projectionMatrix);
		data.wvpLight = Math::Transpose(armWorldMatrix * matLightView * matLightProj);
		mConstantBuffer.Set(data);
		//mDepthMap.BindPS(4);

		mCubeMeshBuffer.Render();

		//mDepthMap.UnbindPS(4);
	}

	// Globe
	Math::Matrix globeRY = Math::Matrix::RotationY(mTimer.GetTotalTime() * 0.25f);
	Math::Matrix globeTran = Math::Matrix::Translation(-70.0f, 200.0f, -100.0f);
	Math::Matrix globeWorldMatrix = globeRY * globeTran;


	data.world = Math::Transpose(globeWorldMatrix);
	data.wvp = Math::Transpose(globeWorldMatrix * viewMatrix * projectionMatrix);
	data.wvpLight = Math::Transpose(globeWorldMatrix * matLightView * matLightProj);
	mConstantBuffer.Set(data);

	mGlobeDiffuse.BindPS(0);
	mGlobeSpecular.BindPS(1);
	mGlobeNormal.BindPS(2);
	mGlobeBump.BindVS(3);
	mDepthMap.BindPS(4);

	mGlobeBuffer.Render();

	mDepthMap.UnbindPS(4);

	// Moon
	Math::Matrix moonRY = Math::Matrix::RotationY(mTimer.GetTotalTime() * 0.1f);
	Math::Matrix moonTran = Math::Matrix::Translation(-70.0f, 200.0f, -100.0f);
	Math::Matrix moonOrbit = Math::Matrix::Translation(cos(mTimer.GetTotalTime() * 0.1f) * 40.0f, 0.0f, sin(mTimer.GetTotalTime() * 0.1f) * 40.0f); // Synchronous
	Math::Matrix moonWorldMatrix = moonRY * moonOrbit * moonTran;
	data.world = Math::Transpose(moonWorldMatrix);
	data.wvp = Math::Transpose(moonWorldMatrix * viewMatrix * projectionMatrix);
	data.wvpLight = Math::Transpose(moonWorldMatrix * matLightView * matLightProj);
	mConstantBuffer.Set(data);

	mMoonDiffuse.BindPS(0);
	mMoonSpecular.BindPS(1);
	mMoonNormal.BindPS(2);
	mDepthMap.BindPS(4);

	mMoonBuffer.Render();
	mDepthMap.UnbindPS(4);

	// Draw plane
	Math::Matrix planeScaling = Math::Matrix::Translation(0.0f,0.0f,0.0f);
	Math::Matrix planeWorldMatrix = planeScaling;

	data.world = Math::Transpose(planeWorldMatrix);
	data.wvp = Math::Transpose(planeWorldMatrix * viewMatrix * projectionMatrix);
	data.wvpLight = Math::Transpose(planeWorldMatrix * matLightView * matLightProj);
	data.displacementScale = 2.0f;
	mConstantBuffer.Set(data);

	mPlaneDiffuse.BindPS(0);
	mPlaneSpecular.BindPS(1);
	mPlaneNormal.BindPS(2);
	mPlaneBump.BindVS(3);
	mDepthMap.BindPS(4);

	mPlaneBuffer.Render();
	mDepthMap.UnbindPS(4);

	mModelVS.Bind();
	mModelPS.Bind();

	// Model
	//Math::Matrix duckRotY = Math::Matrix::RotationY(-90.0f);
	//Math::Matrix duckTran = Math::Matrix::Translation(10.0f, 55.0f, 100.0f);
	//Math::Matrix duckScale = Math::Matrix::Scaling(0.3f);
	//Math::Matrix duckWorldMatrix = duckRotY * duckScale * duckTran;
	Math::Matrix duckWorldMatrix = mDuckAnimation.GetTransform(mTimer.GetTotalTime() * 0.25f);

	data.world = Math::Transpose(duckWorldMatrix);
	data.wvp = Math::Transpose(duckWorldMatrix * viewMatrix * projectionMatrix);
	data.wvpLight = Math::Transpose(duckWorldMatrix * matLightView * matLightProj);
	mConstantBuffer.Set(data);

	mDepthMap.BindPS(4);

	//duckModel.Render();

	mDepthMap.UnbindPS(4);

	Math::Matrix houseRotY = Math::Matrix::RotationY(0.0f);
	Math::Matrix houseTran = Math::Matrix::Translation(-100.0f, 0.0f, 0.0f);
	Math::Matrix houseScale = Math::Matrix::Scaling(5.0f);
	Math::Matrix houseWorldMatrix = houseRotY * houseScale * houseTran;

	data.world = Math::Transpose(houseWorldMatrix);
	data.wvp = Math::Transpose(houseWorldMatrix * viewMatrix * projectionMatrix);
	data.wvpLight = Math::Transpose(houseWorldMatrix * matLightView * matLightProj);
	mConstantBuffer.Set(data);

	mDepthMap.BindPS(4);

	//houseModel.Render();

	mDepthMap.UnbindPS(4);

	for (int z = 0; z <= 100; z++)
	{
		Graphics::SimpleDraw::DrawLine(Math::Vector3(-50.0f, -0.1f, -50.0f + z), Math::Vector3(50.0f, -0.1f, -50.0f + z), Math::Vector4(0.5f, 0.5f, 0.5f, 1.0f));
	}

	for (int x = 0; x <= 100; x++)
	{
		Graphics::SimpleDraw::DrawLine(Math::Vector3(-50.0f + x, -0.1f, -50.0f), Math::Vector3(-50.0f + x, -0.1f, 50.0f), Math::Vector4(0.5f, 0.5f, 0.5f, 1.0f));
	}
}
