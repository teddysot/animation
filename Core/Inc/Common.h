#ifndef INCLUDED_CORE_COMMON_H
#define INCLUDED_CORE_COMMON_H

#define NOMINMAX

// Standard headers
#include <cstdio>
#include <cstdint>
#include <Windows.h>

// STL headers
#include <algorithm>
#include <list>
#include <map>
#include <string>
#include <unordered_map>
#include <vector>

#endif // #ifndef INCLUDED_CORE_COMMON_H