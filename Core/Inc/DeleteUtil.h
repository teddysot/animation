#ifndef INCLUDED_CORE_DELETEUTIL_H
#define INCLUDED_CORE_DELETEUTIL_H

template <class T>
inline void SafeDelete(T*& ptr)
{
	delete ptr;
	ptr = nullptr;
}

template <class T>
inline void SafeDeleteArray(T*& ptr)
{
	delete[] ptr;
	ptr = nullptr;
}

template <typename T>
inline void SafeDeleteVector(std::vector<T*>& vec)
{
	for (auto element : vec)
	{
		SafeDelete(element);
	}

	vec.clear();
}

template <class T>
inline void SafeRelease(T*& ptr)
{
	if (ptr)
	{
		ptr->Release();
		ptr = nullptr;
	}
}

#endif // #ifndef INCLUDED_CORE_DELETEUTIL_H