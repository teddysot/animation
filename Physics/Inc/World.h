#ifndef INCLUDED_PHYSICS_WORLD_H
#define INCLUDED_PHYSICS_WORLD_H

namespace Physics {

	struct Settings 
	{
		Settings()
			: gravity(0.0f, -9.8f, 0.0f)
			, timeStep(1.0f / 60.0f)
			, drag(0.0f)
			, iterations(1)
		{}

		Math::Vector3 gravity;
		float timeStep;
		float drag;
		uint32_t iterations;
	};

	class World
	{
	public:
		World();
		~World();

		void Setup(const Settings& settings);

		void Update(float deltaTime);

		void AddParticle(Particle* p);
		void AddConstraint(Constraint* c);
		void AddPlane(const Math::Plane& p);
		void AddOBB(const Math::OBB& obb);
		void ClearDynamic();

		void DebugDraw() const;

	private:
		void AccumulaForce();
		void Integrate();
		void SatisfyConstraints();

		Settings mSettings;
		ParticleVec mParticles;
		ConstraintVec mConstraints;
		PlaneVec mPlanes;
		OBBVec mOBBs;
		float mTimer;
	};
} // namespace Physics

#endif // INCLUDED_PHYSICS_WORLD_H