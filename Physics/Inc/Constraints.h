#ifndef INCLUDED_PHYSICS_CONSTRAINTS_H
#define INCLUDED_PHYSICS_CONSTRAINTS_H

namespace Physics{

	class Constraint
	{
	public:
		virtual ~Constraint() {}

		virtual void Apply() const = 0;
		virtual void DebugDraw() const {}
	};

	class Spring : public Constraint
	{
	public:
		Spring(Particle* a, Particle* b, float restLength);

		void Apply() const override;
		void DebugDraw() const override;

	protected:
		Particle* mParticleA;
		Particle* mParticleB;
		float mRestLength;
	};

	class Fixed : public Constraint
	{
	public:
		Fixed(Particle* a);

		void Apply()const override;
		//void DebugDraw() const override;

	protected:
		Particle* mParticleA;
		Math::Vector3 mLocation;
	};
} // namspace Physics

#endif // INCLUDED_PHYSICS_CONSTRAINTS_H