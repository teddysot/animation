#ifndef INCLUDED_PHYSICS_FORWARD_H
#define INCLUDED_PHYSICS_FORWARD_H

namespace Physics {

	class Constraint;
	class Particle;

	typedef std::vector<Particle*>		ParticleVec;
	typedef std::vector<Constraint*>	ConstraintVec;
	typedef std::vector<Math::Plane>	PlaneVec;
	typedef std::vector<Math::OBB>		OBBVec;
} // namespace Physics

#endif // INCLUDED_PHYSICS_FORWARD_H