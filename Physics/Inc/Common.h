#ifndef INCLUDED_PHYSICS_COMMON_H
#define INCLUDED_PHYSICS_COMMON_H

// Engine headers
#include <Core\Inc\Core.h>
#include <Graphics\Inc\Graphics.h>
#include <Math\Inc\EngineMath.h>

// Forward Declarations
#include "Forward.h"

#endif // INCLUDED_PHYSICS_COMMON_H