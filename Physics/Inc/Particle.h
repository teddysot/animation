#ifndef INCLUDED_PHYSICS_PARTICLE_H
#define INCLUDED_PHYSICS_PARTICLE_H

namespace Physics {

	class Particle
	{
	public:
		Particle();
		Particle(const Math::Vector3& position, float radius = 1.0f, float invMass = 1.0f);

		void DebugDraw() const;

		void SetPosition(const Math::Vector3& position);
		void SetVelocity(const Math::Vector3& velocity);
		void SetRadius(float radius);
		void SetInvMass(float invMass);
		void SetBounce(float bounce);

		const Math::Vector3& GetPosition() const { return mPosition; }

	private:
		friend class World;
		friend class Spring;
		friend class Fixed;
		Math::Vector3 mPosition;
		Math::Vector3 mPositionOld;
		Math::Vector3 mAcceleration;

		float mRadius;
		float mInvMass;
		bool mSleep;
		float mBounce;
	};
} // namespace Physics

#endif // INCLUDED_PHYSICS_PARTICLE_H