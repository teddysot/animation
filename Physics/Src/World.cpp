#include "Precompiled.h"
#include "World.h"

#include "Constraints.h"
#include "Particle.h"

using namespace Physics;

World::World()
	: mTimer(0.0f)
{
}

World::~World() 
{
}

void World::Setup(const Settings& settings) 
{
	mSettings = settings;
}

void World::Update(float deltaTime) 
{
	mTimer += deltaTime;
	while (mTimer >= mSettings.timeStep)
	{
		mTimer -= mSettings.timeStep;
		AccumulaForce();
		Integrate();
		SatisfyConstraints();
	}
}

void World::AddParticle(Particle* p) 
{
	mParticles.push_back(p);
}

void World::AddConstraint(Constraint* c)
{
	mConstraints.push_back(c);
}

void World::AddPlane(const Math::Plane& plane)
{
	mPlanes.push_back(plane);
}

void World::AddOBB(const Math::OBB& obb)
{
	mOBBs.push_back(obb);
}

void World::ClearDynamic() 
{
	SafeDeleteVector(mParticles);
	SafeDeleteVector(mConstraints);
}

void World::DebugDraw() const 
{
	for (const auto p : mParticles)
	{
		p->DebugDraw();
	}

	for (const auto c : mConstraints)
	{
		c->DebugDraw();
	}

	for (const auto& plane : mPlanes)
	{
		Graphics::SimpleDraw::DrawPlane(plane, Math::Vector3::Zero(), 10.0f, 1.0f, Math::Vector4::Gray());
	}

	for (const auto& obb : mOBBs)
	{
		Graphics::SimpleDraw::DrawOBB(obb, Math::Vector4::Blue());
	}
}

void World::AccumulaForce() 
{
	for (auto p : mParticles)
	{
		p->mAcceleration = mSettings.gravity;
	}
}

void World::Integrate() 
{
	const float timeStepSqr = Math::Sqr(mSettings.timeStep);
	for (auto p : mParticles)
	{
		Math::Vector3 displacement = p->mPosition - p->mPositionOld + (p->mAcceleration * timeStepSqr);
		p->mPositionOld = p->mPosition;
		p->mPosition = p->mPosition + displacement;
	}
}

void World::SatisfyConstraints() 
{
	for (uint32_t n = 0; n < mSettings.iterations; ++n)
	{
		for (const auto c : mConstraints)
		{
			c->Apply();
		}

		// Check plane collision
		for (const auto& plane : mPlanes)
		{
			for (auto p : mParticles)
			{
				float dist = Math::Dot(p->mPosition, plane.n) - plane.d;
				if (dist >= 0.0f)
				{
					continue;
				}

				Math::Vector3 pointOnPlane = p->mPosition - (plane.n * dist);
				p->mPosition = pointOnPlane + (plane.n * mSettings.timeStep);

				Math::Vector3 vel = p->mPosition - p->mPositionOld;
				p->mPositionOld = p->mPosition - Math::Reflect(vel, plane.n);
			}
		}

		const uint32_t kNumParticles = mParticles.size();
		const uint32_t kNumOBBs = mOBBs.size();
		for (uint32_t i = 0; i < kNumParticles; ++i)
		{
			Particle* p = mParticles[i];
			if (p->mSleep)
			{
				continue;
			}

			for (uint32_t j = 0; j < kNumOBBs; ++j)
			{
				Math::OBB obb = mOBBs[j];
				obb.extend += Math::Vector3::One() * p->mRadius;

				if (Intersect(p->mPosition, obb))
				{
					Math::Vector3 vel(p->mPosition - p->mPositionOld);
					Math::Vector3 dir = Math::Normalize(vel);

					Math::Ray ray(p->mPositionOld, dir);
					Math::Vector3 point, normal;
					GetContactPoint(ray, obb, point, normal);

					Math::Vector3 velN = normal * Math::Dot(normal, vel);
					Math::Vector3 velT = vel - velN;

					p->mPosition = point + (normal * 0.005f);
					p->mPositionOld = p->mPosition - (velT - velN * p->mBounce);
				}
			}
		}
	}
}