#include "Precompiled.h"
#include "Constraints.h"

#include "Particle.h"

using namespace Physics;

Spring::Spring(Particle* a, Particle* b, float restLength)
	: mParticleA(a)
	, mParticleB(b)
	, mRestLength(restLength)
{
}

Fixed::Fixed(Particle* a)
	: mParticleA(a)
	, mLocation(a->mPosition)
{
}

void Physics::Fixed::Apply() const
{
	mParticleA->SetPosition(mLocation);
}

void Spring::Apply() const
{
	if (mRestLength == 0.0f)
	{
		mParticleA->mPosition = mParticleB->mPosition;
		return;
	}
	Math::Vector3 delta = mParticleB->mPosition - mParticleA->mPosition;
	float dist = Math::Magnitude(delta);
	float diff = (dist - mRestLength) / (dist * (mParticleA->mInvMass + mParticleB->mInvMass));
	mParticleA->mPosition += delta * diff * mParticleA->mInvMass;
	mParticleB->mPosition -= delta * diff * mParticleB->mInvMass;
}

void Spring::DebugDraw() const
{
	Graphics::SimpleDraw::DrawLine(
		mParticleA->mPosition,
		mParticleB->mPosition,
		Math::Vector4::Green());
}