#include "Precompiled.h"
#include "Particle.h"

using namespace Physics;

Particle::Particle()
	: mPosition(0.0f, 0.0f, 0.0f)
	, mPositionOld(0.0f, 0.0f, 0.0f)
	, mAcceleration(0.0f, 0.0f, 0.0f)
	, mRadius(1.0f)
	, mInvMass(1.0f)
	, mBounce(1.0f)
	, mSleep(false)
{}

Particle::Particle(const Math::Vector3& pos, float radius, float invMass)
	: mPosition(pos)
	, mPositionOld(pos)
	, mAcceleration(0.0f, 0.0f, 0.0f)
	, mRadius(radius)
	, mInvMass(invMass)
	, mBounce(1.0f)
	, mSleep(false)
{}

void Particle::DebugDraw() const
{
	Graphics::SimpleDraw::DrawSphere(mPosition, mRadius, Math::Vector4::Cyan(), 4, 2);
}

void Particle::SetPosition(const Math::Vector3 & position)
{
	mPosition = position;
	mPositionOld = position;
}

void Particle::SetVelocity(const Math::Vector3 & velocity)
{
	mPositionOld = mPosition - velocity;
}

void Particle::SetRadius(float radius)
{
	mRadius = radius;

}

void Particle::SetInvMass(float invMass)
{
	mInvMass = invMass;
}

void Particle::SetBounce(float bounce)
{
	mBounce = bounce;
}
