#include <Graphics\Inc\Graphics.h>
#include <assimp\Importer.hpp>	// C++ importer interface
#include <assimp\scene.h>		// Output data structure
#include <assimp\postprocess.h> // Post processing flags
#include <cstdlib>
#include <cstdio>
#include <map>
#include <vector>

#pragma comment(lib, "assimp.lib")

using namespace Graphics;

struct Params
{
	Params()
		: inputFileName(nullptr)
		, outputFileName(nullptr)
	{}

	const char* inputFileName;
	const char* outputFileName;
};

struct BoneWeights
{
	uint32_t count = 0;
	uint32_t index[4];
	float weight[4];
};

typedef std::map<std::string, uint32_t> BoneIndexMap;
typedef std::vector<Bone*> Bones;

const char* StripPath(const char* filePath)
{
	// Copy from the last '\\' if there is one
	const char* fileName = strrchr(filePath, '\\');
	if (fileName != nullptr)
	{
		return fileName + 1; // Found '\\', +1 to skip
	}

	// If no '\\' found, copy from the last '/' if there is one
	fileName = strrchr(filePath, '/');
	if (fileName != nullptr)
	{
		return fileName + 1; // Found '/', +1 to skip
	}

	// If neither is found, then file path is just the name
	return filePath;
}

void PrintHelp()
{
	printf(
		"== ModelImporter Help ==\n"
		"\n"
		"Usage:\n"
		"\n"
		"\tModelImporter.exe <InputFile> <OutputFile>\n"
		"\n"
	);
}

Math::Matrix Convert(const aiMatrix4x4& aiMatrix)
{
	Math::Matrix mat = *(Math::Matrix*)&aiMatrix;
	mat = Math::Transpose(mat);
	return mat;
}

// This function search and see if 'bones' has already been added,
// If not, it will add it to 'bones' array and a lookup entry to the index map
uint32_t GetBoneIndex(aiBone* bone, Bones& bones, BoneIndexMap& boneIndexMap)
{
	// See if we have already added to this bone
	auto it = boneIndexMap.find(bone->mName.C_Str());
	if (it != boneIndexMap.end())
	{
		return it->second;
	}

	// We need to add a new bone
	uint32_t boneIndex = bones.size();

	Bone* newBone = new Bone();
	ASSERT(bone->mName.length > 0, "Error: Bone %d doesn't have a name!", boneIndex);

	newBone->name = bone->mName.C_Str();
	newBone->index = boneIndex;
	newBone->offsetTransform = Convert(bone->mOffsetMatrix);

	bones.push_back(newBone);
	boneIndexMap.insert(std::make_pair(newBone->name.c_str(), boneIndex));

	return boneIndex;
}

Bone* BuildSkeleton(aiNode& aiNode, Bone* parent, Bones& bones, BoneIndexMap& boneIndexMap)
{
	Bone* bone = nullptr;

	auto it = boneIndexMap.find(aiNode.mName.C_Str());
	if (it == boneIndexMap.end())
	{
		const uint32_t boneIndex = bones.size();

		// We need to add a new bone
		bone = new Bone();
		bone->index = boneIndex;
		bone->offsetTransform = Math::Matrix::Identity();

		if (aiNode.mName.length > 0)
		{
			bone->name = aiNode.mName.C_Str();
		}
		else
		{
			char buffer[128];
			sprintf_s(buffer, 128, "unnamed_%u", boneIndex);
			printf("Warning: Bone %u has no name, renamed to %s\n", boneIndex, buffer);
			bone->name = buffer;
		}

		bones.push_back(bone);
		boneIndexMap.insert(std::make_pair(bone->name, bone->index));
	}
	else
	{
		bone = bones[it->second];
	}

	bone->transform = Convert(aiNode.mTransformation);
	bone->parent = parent;
	bone->parentIndex = parent ? parent->index : -1;

	for (uint32_t i = 0; i < aiNode.mNumChildren; ++i)
	{
		Bone* child = BuildSkeleton(*(aiNode.mChildren[i]), bone, bones, boneIndexMap);
		bone->children.push_back(child);
		bone->childrenIndex.push_back(child->index);
	}

	return bone;
}

void PrintMatrix(FILE* file, const Math::Matrix& m)
{
	fprintf(file, "%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n",
		m._11, m._12, m._13, m._14,
		m._21, m._22, m._23, m._24,
		m._31, m._32, m._33, m._34,
		m._41, m._42, m._43, m._44);
}

bool ImportModel(const Params& params)
{
	// Create an instance of the Importer class
	Assimp::Importer importer;

	// And have it read the given file with some example postprocessing
	// Usually - if speed is not the most important aspect for you - you'll
	// probably to request more postprocessing than we do in this example
	const uint32_t flags =
		aiProcess_CalcTangentSpace |
		aiProcess_JoinIdenticalVertices |
		aiProcess_Triangulate |
		aiProcess_GenNormals |
		aiProcess_FlipUVs;
	const aiScene* scene = importer.ReadFile(params.inputFileName, flags);

	// If the import failed, report it
	if (scene == nullptr)
	{
		printf("ModelImporter -- Error : %s\n", importer.GetErrorString());
		return false;
	}

	FILE* file = nullptr;
	errno_t error = fopen_s(&file, params.outputFileName, "w");
	if (error != 0)
	{
		printf("Error: Failed to write to output file.\n");
		return false;
	}

	BoneIndexMap boneIndexMap;
	Bones bones;

	// Read mesh data
	if (scene->HasMeshes())
	{
		printf("Reading mesh data..\n");

		fprintf(file, "MeshCount: %d\n", scene->mNumMeshes);

		for (uint32_t meshIndex = 0; meshIndex < scene->mNumMeshes; ++meshIndex)
		{
			const aiMesh* aiMesh = scene->mMeshes[meshIndex];

			fprintf(file, "VertexCount: %d\n", aiMesh->mNumVertices);
			fprintf(file, "IndexCount: %d\n", aiMesh->mNumFaces * 3);
			fprintf(file, "MaterialIndex: %d\n", aiMesh->mMaterialIndex);

			printf("Reading vertices... \n");

			const aiVector3D* positions = aiMesh->mVertices;
			const aiVector3D* normals = aiMesh->mNormals;
			const aiVector3D* tangents = aiMesh->mTangents;
			const aiVector3D* texcoords = aiMesh->HasTextureCoords(0) ? aiMesh->mTextureCoords[0] : nullptr;

			for (uint32_t i = 0; i < aiMesh->mNumVertices; ++i)
			{
				fprintf(file, "%f %f %f %f %f %f %f %f %f %f %f\n",
					positions[i].x, positions[i].y, positions[i].z,
					normals[i].x, normals[i].y, normals[i].z,
					tangents[i].x, tangents[i].y, tangents[i].z,
					texcoords ? texcoords[i].x : 0.0f,
					texcoords ? texcoords[i].y : 0.0f);
			}

			printf("Reading indices... \n");
			for (uint32_t i = 0; i < aiMesh->mNumFaces; ++i)
			{
				fprintf(file, "%d %d %d\n",
					aiMesh->mFaces[i].mIndices[0],
					aiMesh->mFaces[i].mIndices[1],
					aiMesh->mFaces[i].mIndices[2]);
			}

			// Bones
			if (aiMesh->HasBones())
			{
				printf("Reading bone weights...\n");

				std::vector<BoneWeights> boneWeights;
				boneWeights.resize(aiMesh->mNumVertices);

				for (uint32_t i = 0; i < aiMesh->mNumBones; ++i)
				{
					aiBone* aiBone = aiMesh->mBones[i];
					uint32_t boneIndex = GetBoneIndex(aiBone, bones, boneIndexMap);

					for (uint32_t j = 0; j < aiBone->mNumWeights; ++j)
					{
						const aiVertexWeight& aiVertexWeight = aiBone->mWeights[j];
						const uint32_t vertexIndex = aiVertexWeight.mVertexId;
						
						// Only support up to 4 weights
						if (boneWeights[vertexIndex].count < 4)
						{
							int weightIndex = boneWeights[vertexIndex].count++;
							boneWeights[vertexIndex].index[weightIndex] = boneIndex;
							boneWeights[vertexIndex].weight[weightIndex] = aiVertexWeight.mWeight;
						}
					}
				}

				for (auto boneWeight : boneWeights)
				{
					fprintf(file, "%d %d %d %d %d %f %f %f %f\n",
						boneWeight.count,
						boneWeight.index[0],
						boneWeight.index[1],
						boneWeight.index[2],
						boneWeight.index[3],
						boneWeight.weight[0],
						boneWeight.weight[1],
						boneWeight.weight[2],
						boneWeight.weight[3]);
				}
			}
		}

		// Read material data
		if (scene->HasMaterials())
		{
			printf("Reading materials...\n");

			fprintf(file, "MaterialCount: %d\n", scene->mNumMaterials);
			for (uint32_t i = 0; i < scene->mNumMaterials; ++i)
			{
				const aiMaterial* material = scene->mMaterials[i];
				const uint32_t diffuseMapCount = material->GetTextureCount(aiTextureType_DIFFUSE);
				if (diffuseMapCount > 0)
				{
					aiString texturePath;
					if (material->GetTexture(aiTextureType_DIFFUSE, 0, &texturePath) == AI_SUCCESS)
					{
						// Save the file name
						fprintf(file, "DiffuseMap: %s\n", StripPath(texturePath.C_Str()));
					}
					else
					{
						// No diffuse map found
						fprintf(file, "DiffuseMap: none\n");
					}
				}
			}
		}
	}

	// Read animation data
	if (scene->HasAnimations())
	{
		printf("Reading skeleton...\n");
		
		Bone* root = BuildSkeleton(*scene->mRootNode, nullptr, bones, boneIndexMap);

		fprintf(file, "BoneCount: %d\n", bones.size());
		for (auto bone : bones)
		{
			fprintf(file, "Name: %s\n", bone->name.c_str());
			fprintf(file, "Index: %d\n", bone->index);
			fprintf(file, "Parent: %d\n", bone->parentIndex);
			fprintf(file, "ChildCount: %d\n", bone->children.size());
			if (!bone->children.empty())
			{
				for (auto childIndex : bone->childrenIndex)
				{
					fprintf(file, "%d ", childIndex);
				}
				fprintf(file, "\n");
			}

			PrintMatrix(file, bone->transform);
			PrintMatrix(file, bone->offsetTransform);
		}

		fprintf(file, "AnimationCount: %d\n", scene->mNumAnimations);

		for (uint32_t animIndex = 0; animIndex < scene->mNumAnimations; ++animIndex)
		{
			aiAnimation* aiAnim = scene->mAnimations[animIndex];

			if (aiAnim->mName.length > 0)
			{
				fprintf(file, "Name: %s\n", aiAnim->mName.C_Str());
			}
			else
			{
				char buffer[128];
				sprintf_s(buffer, 128, "Anim%d", animIndex);
				printf("Warning: Animation %u has no name, renamed to %s\n", animIndex, buffer);
				fprintf(file, "Name: %s\n", buffer);
			}

			fprintf(file, "Duration: %f\n", (float)aiAnim->mDuration);
			fprintf(file, "TicksPerSecond: %f\n", (float)aiAnim->mTicksPerSecond);

			fprintf(file, "BoneAnimation: %d\n", aiAnim->mNumChannels);

			for (uint32_t boneAnimIndex = 0; boneAnimIndex < aiAnim->mNumChannels; ++boneAnimIndex)
			{
				aiNodeAnim* aiNodeAnim = aiAnim->mChannels[boneAnimIndex];

				auto it = boneIndexMap.find(aiNodeAnim->mNodeName.C_Str());
				if (it == boneIndexMap.end())
				{
					printf("Warning: Failed to find bone %s. Ignoring...\n", aiNodeAnim->mNodeName.C_Str());
					continue;
				}

				fprintf(file, "BoneIndex: %d\n", it->second);

				if (aiNodeAnim->mNumPositionKeys != aiNodeAnim->mNumRotationKeys ||
					aiNodeAnim->mNumPositionKeys != aiNodeAnim->mNumScalingKeys)
				{
					printf("Error: Mismatched number of animation keys. Format not supported.\n");
					exit(-1);
				}

				fprintf(file, "Keyframes: %d\n", aiNodeAnim->mNumPositionKeys);

				for (uint32_t keyIndex = 0; keyIndex < aiNodeAnim->mNumPositionKeys; ++keyIndex)
				{
					fprintf(file, "%f %f %f %f %f %f %f %f %f %f %f\n",
						aiNodeAnim->mPositionKeys[keyIndex].mTime,
						aiNodeAnim->mPositionKeys[keyIndex].mValue.x,
						aiNodeAnim->mPositionKeys[keyIndex].mValue.y,
						aiNodeAnim->mPositionKeys[keyIndex].mValue.z,
						aiNodeAnim->mRotationKeys[keyIndex].mValue.x,
						aiNodeAnim->mRotationKeys[keyIndex].mValue.y,
						aiNodeAnim->mRotationKeys[keyIndex].mValue.z,
						aiNodeAnim->mRotationKeys[keyIndex].mValue.w,
						aiNodeAnim->mScalingKeys[keyIndex].mValue.x,
						aiNodeAnim->mScalingKeys[keyIndex].mValue.y,
						aiNodeAnim->mScalingKeys[keyIndex].mValue.z);
				}
			}
		}
	}

	// All done
	printf("All done!\n");
	fclose(file);
	return true;
}

bool ParseArg(int argc, char* argv[], Params& params)
{
	if (argc < 3)
	{
		return false;
	}

	params.inputFileName = argv[argc - 2];
	params.outputFileName = argv[argc - 1];

	return true;
}

int main(int argc, char* argv[])
{
	Params params;
	if (!ParseArg(argc, argv, params))
	{
		PrintHelp();
		return -1;
	}

	if (!ImportModel(params))
	{
		printf("Failed to import model %s.\n", params.inputFileName);
		return -1;
	}

	return 0;
}