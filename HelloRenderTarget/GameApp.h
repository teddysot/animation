#ifndef INCLUDED_GAMEAPP
#define INCLUDED_GAMEAPP


#include <Core\Inc\Core.h>
#include <Graphics\Inc\Graphics.h>
#include <Input\Inc\Input.h>

class GameApp : public Core::Application
{
public:
	GameApp();
	~GameApp() override;

private:

	struct Light //TODO:: add to graphics (Graphics::Light)
	{
		Light()
			:direction(Math::Vector3::ZAxis())
			, ambient(Math::Vector4::White())
			, diffuse(Math::Vector4::White())
			, specular(Math::Vector4::White())
		{}

		Math::Vector3 direction;
		float padding;
		Math::Vector4 ambient;
		Math::Vector4 diffuse;
		Math::Vector4 specular;
	};

	struct Material //TODO:: add to graphics (Graphics::Material)
	{
		Material()
			: ambient(Math::Vector4::White())
			, diffuse(Math::Vector4::White())
			, specular(Math::Vector4::White())
			, power(1.0f)
		{}

		Math::Vector4 ambient;
		Math::Vector4 diffuse;
		Math::Vector4 specular;
		float power;
	};


	struct ConstantData
	{
		Math::Matrix wvp;
		Math::Matrix world;
		Math::Vector3 viewPosition;
		float padding;
		Graphics::Light light;
		Graphics::Material material;
		float displacementScale;
	};


	void OnInitialize(uint32_t width, uint32_t height) override;
	void OnTerminate() override;
	void OnUpdate() override;

	Core::Window mWindow;
	Core::Timer mTimer;

	Graphics::Camera mCamera;
	Graphics::Transform mCameraTransform;

	Graphics::Light mLight;
	Graphics::Material mMaterial;

	Graphics::Sampler mSampler;

	Graphics::VertexShader mMeshVS;
	Graphics::PixelShader mMeshPS;

	Graphics::VertexShader mTerrainVS;
	Graphics::PixelShader mTerrainPS;

	Graphics::VertexShader mSkyboxVS;
	Graphics::PixelShader mSkyboxPS;

	Graphics::VertexShader mRenderTargetVS;
	Graphics::PixelShader mRenderTargetPS;

	Graphics::RenderTarget mRenderTarget;

	// Globe
	Graphics::TypedConstantBuffer<ConstantData> mGlobeConstantBuffer;
	Graphics::Texture mGlobeDiffuse;
	Graphics::Texture mGlobeSpecular;
	Graphics::Texture mGlobeDarkSide;
	Graphics::Texture mGlobeNormal;
	Graphics::Texture mGlobeBump;
	Graphics::Mesh mGlobeMesh;
	Graphics::MeshBuffer mGlobeBuffer; //make it mesh buffer later
	Graphics::Transform mGlobeTransform;

	// Moon
	Graphics::TypedConstantBuffer<ConstantData> mMoonConstantBuffer;
	Graphics::Texture mMoonDiffuse;
	Graphics::Texture mMoonSpecular;
	Graphics::Texture mMoonDarkSide;
	Graphics::Texture mMoonNormal;
	Graphics::Mesh mMoonMesh;
	Graphics::MeshBuffer mMoonBuffer; //make it mesh buffer later
	Graphics::Transform mMoonTransform;

	// Skybox
	Graphics::TypedConstantBuffer<ConstantData> mSkyboxConstantBuffer;
	Graphics::Texture mSkyboxTexture;
	Graphics::Mesh mSkyboxMesh;
	Graphics::MeshBuffer mSkyboxBuffer;

	// Terrain
	Graphics::TypedConstantBuffer<ConstantData> mTerrainConstantBuffer;
	Graphics::Terrain mTerrain;
	Graphics::Texture mTerrainTexture;
	Graphics::Texture mTerrainSpecular;

	// Quad
	Graphics::TypedConstantBuffer<ConstantData> mRenderTargetConstantBuffer;
	Graphics::Mesh mRenderTargetMesh;
	Graphics::MeshBuffer mRenderTargetBuffer;
	Graphics::MeshBuilder mBuilder;


	float mSphereSize = 1.0f;
	float mSphereRings = 3.0f;
	float mSphereSlices = 3.0f;

};

#endif