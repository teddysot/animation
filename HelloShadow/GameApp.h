#ifndef INCLUDED_GAMEAPP
#define INCLUDED_GAMEAPP


#include <Core\Inc\Core.h>
#include <Graphics\Inc\Graphics.h>
#include <Input\Inc\Input.h>

class GameApp : public Core::Application
{
public:
	GameApp();
	void GenerateDepthMap();
	void DrawScene();
	~GameApp() override;

private:

	struct Light //TODO:: add to graphics (Graphics::Light)
	{
		Light()
			:direction(Math::Vector3::ZAxis())
			, ambient(Math::Vector4::White())
			, diffuse(Math::Vector4::White())
			, specular(Math::Vector4::White())
		{}

		Math::Vector3 direction;
		float padding;
		Math::Vector4 ambient;
		Math::Vector4 diffuse;
		Math::Vector4 specular;
	};

	struct Material //TODO:: add to graphics (Graphics::Material)
	{
		Material()
			: ambient(Math::Vector4::White())
			, diffuse(Math::Vector4::White())
			, specular(Math::Vector4::White())
			, power(1.0f)
		{}

		Math::Vector4 ambient;
		Math::Vector4 diffuse;
		Math::Vector4 specular;
		float power;
	};


	struct ConstantData
	{
		Math::Matrix world;
		Math::Matrix wvp;
		Math::Matrix wvpLight;
		Math::Vector3 viewPosition;
		float padding;
		Graphics::Light light;
		Graphics::Material material;
		float displacementScale = 1.0f;
	};

	struct PostProcessingData
	{
		int fnum;
	};

	int postNumber;


	void OnInitialize(uint32_t width, uint32_t height) override;
	void OnTerminate() override;
	void OnUpdate() override;

	Core::Window mWindow;
	Core::Timer mTimer;

	Graphics::Camera* mCurrentCamera;
	Graphics::Transform* mCurrentCameraTransform;

	Graphics::Camera mCamera;
	Graphics::Transform mCameraTransform;

	Graphics::Camera mLightCamera;
	Graphics::Transform mLightCameraTransform;

	Graphics::Light mLight;
	Math::Vector3 mLightPosition;
	Math::Vector3 mLightTarget;
	Graphics::Material mMaterial;

	Graphics::Sampler mSampler;
	Graphics::Sampler mShadowSampler;

	Graphics::VertexShader mDepthVS;
	Graphics::PixelShader mDepthPS;

	Graphics::VertexShader mMeshVS;
	Graphics::PixelShader mMeshPS;

	Graphics::VertexShader mTerrainVS;
	Graphics::PixelShader mTerrainPS;

	Graphics::VertexShader mSkyboxVS;
	Graphics::PixelShader mSkyboxPS;

	Graphics::VertexShader mRenderTargetVS;
	Graphics::PixelShader mRenderTargetPS;

	Graphics::VertexShader mModelVS;
	Graphics::PixelShader mModelPS;

	Graphics::DepthMap mDepthMap;
	Graphics::RenderTarget mRenderTarget;

	// ConstantBuffer
	Graphics::TypedConstantBuffer<ConstantData> mConstantBuffer;
	Graphics::TypedConstantBuffer<ConstantData> mDepthConstantBuffer;
	Graphics::TypedConstantBuffer<ConstantData> mSkyboxConstantBuffer;
	Graphics::TypedConstantBuffer<ConstantData> mTerrainConstantBuffer;
	Graphics::TypedConstantBuffer<ConstantData> mMoonConstantBuffer;
	Graphics::TypedConstantBuffer<ConstantData> mGlobeConstantBuffer;
	Graphics::TypedConstantBuffer<ConstantData> mPlaneConstantBuffer;

	Graphics::TypedConstantBuffer<PostProcessingData> mRenderTargetConstantBuffer;

	// Globe
	Graphics::Texture mGlobeDiffuse;
	Graphics::Texture mGlobeSpecular;
	Graphics::Texture mGlobeNormal;
	Graphics::Texture mGlobeBump;
	Graphics::Mesh mGlobeMesh;
	Graphics::MeshBuffer mGlobeBuffer; //make it mesh buffer later
	Graphics::Transform mGlobeTransform;

	// Moon
	Graphics::Texture mMoonDiffuse;
	Graphics::Texture mMoonSpecular;
	Graphics::Texture mMoonDarkSide;
	Graphics::Texture mMoonNormal;
	Graphics::Mesh mMoonMesh;
	Graphics::MeshBuffer mMoonBuffer; //make it mesh buffer later
	Graphics::Transform mMoonTransform;

	// Plane
	Graphics::Mesh mPlaneMesh;
	Graphics::MeshBuffer mPlaneBuffer;
	Graphics::Texture mPlaneDiffuse;
	Graphics::Texture mPlaneSpecular;
	Graphics::Texture mPlaneNormal;
	Graphics::Texture mPlaneBump;

	// Skybox
	Graphics::Texture mSkyboxTexture;
	Graphics::Mesh mSkyboxMesh;
	Graphics::MeshBuffer mSkyboxBuffer;

	// Terrain
	Graphics::Terrain mTerrain;
	Graphics::Texture mTerrainTexture;
	Graphics::Texture mTerrainSpecular;

	// Quad
	Graphics::Mesh mRenderTargetMesh;
	Graphics::MeshBuffer mRenderTargetBuffer;

	Graphics::Model duckModel;
	Graphics::Model houseModel;
	Graphics::MeshBuffer mModelBuffer;
	Graphics::Texture mModelTexture;

	Graphics::MeshBuilder mBuilder;

};

#endif