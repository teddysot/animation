#include "GameApp.h"

#include "Math\Inc\EngineMath.h"
#include <fstream>
#include <iostream>
/*
Update Vertex to contain tangent
Update Mesh to generate tangent
Load/Bind normal map
In Pixel Shader, build Tangent Binormal Normal (TBN), convert RGB => XYZ,
replace normal with XYZ
*/
/*
terrain class
Init(filename)
Terminate()
Draw()


Sphere
terrain

*/

GameApp::GameApp() :
	mCurrentCamera(&mCamera),
	mCurrentCameraTransform(&mCameraTransform)
{}

GameApp::~GameApp()
{}

void GameApp::GenerateDepthMap()
{
	/*mDepthMap.BeginRender();

	Math::Matrix matView = mLightCamera.GetViewMatrix();
	Math::Matrix matProj = mLightCamera.GetProjectionMatrix(1, 1);

	mDepthVS.Bind();
	mDepthPS.Bind();


	mDepthMap.EndRender();*/
}

void GameApp::DrawScene()
{

}

void GameApp::OnInitialize(uint32_t width, uint32_t height)
{
	mWindow.Initialize(GetInstance(), GetAppName(), width, height);
	HookWindow(mWindow.GetWindowHandle());

	mTimer.Initialize();


	Graphics::GraphicsSystem::StaticInitialize(mWindow.GetWindowHandle(), false);
	Graphics::SimpleDraw::StaticInitialize(100000);
	Input::InputSystem::StaticInitialize(mWindow.GetWindowHandle());

	mCameraTransform.SetPostion(Math::Vector3(0.0f, 30.0f, -10.0f));
	mCameraTransform.SetDirection(Math::Vector3(0.0f, 0.0f, 1.0f));

	mLightPosition = Math::Vector3(0.0f, 200.0f, -100.0f);
	mLightTarget = Math::Vector3(0.0f, 0.0f, 0.0f);

	Math::Vector3 lightDirection = Math::Normalize(mLightTarget - mLightPosition);
	mLightCameraTransform.SetPostion(Math::Vector3(mLightPosition));
	mLightCameraTransform.SetDirection(Math::Normalize(Math::Vector3(lightDirection)));

	mLight.direction = Math::Normalize(Math::Vector3(1.0f, -1.0f, 1.0f));
	mLight.ambient = Math::Vector4(0.1f, 0.1f, 0.1f, 1.0f);
	mLight.diffuse = Math::Vector4(2.0f, 2.0f, 2.0f, 1.0f);
	mLight.specular = Math::Vector4(1.0f, 1.0f, 1.0f, 1.0f);

	mMaterial.ambient = Math::Vector4(1.0f, 1.0f, 1.0f, 1.0f);
	mMaterial.diffuse = Math::Vector4(0.5f, 0.5f, 0.5f, 1.0f);
	mMaterial.specular = Math::Vector4(0.5f, 0.5f, 0.5f, 1.0f);
	mMaterial.power = 20;

	mSampler.Initialize(Graphics::Sampler::Filter::Anisotropic, Graphics::Sampler::AddressMode::Clamp);

	// Terrain
	//mTerrainVS.Initialize(L"../Assets/Shaders/Terrain.fx", Graphics::Vertex::Format);
	//mTerrainPS.Initialize(L"../Assets/Shaders/Terrain.fx");

	//mTerrainConstantBuffer.Initialize();
	//mTerrainTexture.Initialize(L"../Assets/Images/RockySeamless.jpg");
	//mTerrain.Initialize("../Assets/HeightMaps/TestMap.raw", Math::Vector3(1.0f, 0.1f, 1.0f));
	//mTerrainSpecular.Initialize(L"../Assets/Images/RockySeamless_spec.jpg");

	// Plane
	mPlaneVS.Initialize(L"../Assets/Shaders/Terrain.fx", Graphics::Vertex::Format);
	mPlanePS.Initialize(L"../Assets/Shaders/Terrain.fx");

	mPlaneConstantBuffer.Initialize();
	mPlaneTexture.Initialize(L"../Assets/Images/brickwork.jpg");
	mPlaneMesh.Instantiate(10000);
	mBuilder.CreatePlane(Math::Vector2(100, 100), mPlaneMesh);
	mPlaneBuffer.SetTopology(Graphics::MeshBuffer::Topology::TriangleList);
	mPlaneBuffer.Initialize(mPlaneMesh);

	// Skybox
	mSkyboxVS.Initialize(L"../Assets/Shaders/Skybox.fx", Graphics::Vertex::Format);
	mSkyboxPS.Initialize(L"../Assets/Shaders/Skybox.fx");

	mSkyboxConstantBuffer.Initialize();
	mSkyboxTexture.Initialize(L"../Assets/Images/space_cube.dds");
	mSkyboxMesh.Instantiate(100);
	//mBuilder.CreateCube(Math::Vector3(100, 100, 100), mSkyboxMesh);
	mBuilder.CreateSkybox(1000, mSkyboxMesh);
	mSkyboxBuffer.SetTopology(Graphics::MeshBuffer::Topology::TriangleList);
	mSkyboxBuffer.Initialize(mSkyboxMesh);

	// Mesh Shader
	mMeshVS.Initialize(L"../Assets/Shaders/Standard.fx", Graphics::Vertex::Format);
	mMeshPS.Initialize(L"../Assets/Shaders/Standard.fx");

	// Globe
	mGlobeConstantBuffer.Initialize();
	mGlobeTexture.Initialize(L"../Assets/Images/earth.jpg");
	mGlobeSpecular.Initialize(L"../Assets/Images/earth_spec.jpg");
	mGlobeNormal.Initialize(L"../Assets/Images/earth_normal.jpg");
	mGlobeBump.Initialize(L"../Assets/Images/earth_bump.jpg");
	mGlobeMesh.Instantiate(10000);
	mBuilder.CreateSphere(20.0f, 40, 40, mGlobeMesh);
	mGlobeBuffer.SetTopology(Graphics::MeshBuffer::Topology::TriangleList);
	mGlobeBuffer.Initialize(mGlobeMesh);

	// Moon
	mMoonConstantBuffer.Initialize();
	mMoonTexture.Initialize(L"../Assets/Images/moon.jpg");
	mMoonNormal.Initialize(L"../Assets/Images/moon_normal.jpg");

	mMoonMesh.Instantiate(10000);
	mBuilder.CreateSphere(5.0f, 40, 40, mMoonMesh);
	mMoonBuffer.SetTopology(Graphics::MeshBuffer::Topology::TriangleList);
	mMoonBuffer.Initialize(mMoonMesh);

	// Quad
	mRenderTargetVS.Initialize(L"../Assets/Shaders/PostProcessing.fx", Graphics::Vertex::Format);
	mRenderTargetPS.Initialize(L"../Assets/Shaders/PostProcessing.fx");
	mRenderTargetConstantBuffer.Initialize();
	mRenderTargetMesh.Instantiate(10000);
	mBuilder.CreateQuad(mRenderTargetMesh);
	mRenderTargetBuffer.SetTopology(Graphics::MeshBuffer::Topology::TriangleList);
	mRenderTargetBuffer.Initialize(mRenderTargetMesh);

	mDepthVS.Initialize(L"../Assets/Shaders/DepthMap.fx", Graphics::Vertex::Format);
	mDepthMap.Initialize(width, height);
	mRenderTarget.Initialize(width, height, Graphics::RenderTarget::Format::RGBA_U32);
}

void GameApp::OnTerminate()
{

	Graphics::GraphicsSystem::StaticTerminate();
	Graphics::SimpleDraw::StaticTerminate();
	Input::InputSystem::StaticTerminate();

	// Terrain
	//mTerrainTexture.Terminate();
	//mTerrainSpecular.Terminate();
	//mTerrain.Terminate();

	// Plane
	mPlaneTexture.Terminate();
	mPlaneMesh.Terminate();

	// Globe
	mGlobeTexture.Terminate();
	mGlobeSpecular.Terminate();
	mGlobeNormal.Terminate();
	mGlobeBump.Terminate();
	mGlobeMesh.Terminate();

	// Moon
	mMoonTexture.Terminate();
	mMoonNormal.Terminate();
	mMoonMesh.Terminate();

	// Skybox
	mSkyboxTexture.Terminate();

	// Quad
	mRenderTargetMesh.Terminate();

	// Shader
	mSampler.Terminate();

	mMeshVS.Terminate();
	mMeshPS.Terminate();

	//mTerrainVS.Terminate();
	//mTerrainPS.Terminate();

	mPlaneVS.Terminate();
	mPlanePS.Terminate();

	mSkyboxVS.Terminate();
	mSkyboxPS.Terminate();

	mRenderTargetVS.Terminate();
	mRenderTargetPS.Terminate();

	mGlobeConstantBuffer.Terminate();
	mMoonConstantBuffer.Terminate();
	mSkyboxConstantBuffer.Terminate();
	mTerrainConstantBuffer.Terminate();
	mPlaneConstantBuffer.Terminate();
	mRenderTargetConstantBuffer.Terminate();

	// Render Target
	mRenderTarget.Terminate();

	UnhookWindow();
	mWindow.Terminate();
}

void GameApp::OnUpdate()
{
	if (mWindow.ProcessMessage())
	{
		Kill();
	}

	mTimer.Update();

	Input::InputSystem* is = Input::InputSystem::Get();
	is->Update();

	if (is->IsKeyPressed(Keys::ESCAPE))
	{
		PostQuitMessage(0);
	}

	float cameraMoveSpeed = 20.0f;
	float cameraRotationSpeed = 0.3f;
	float cameraSpeedValue = 10.0f;
	float cameraRiseValue = 10.0f;

	if (is->IsMouseDown(1))
	{
		mCameraTransform.Pitch(((float)is->GetMouseMoveY() * cameraRotationSpeed * mTimer.GetElapsedTime()));
		mCameraTransform.Yaw(((float)is->GetMouseMoveX() * cameraRotationSpeed * mTimer.GetElapsedTime()));
	}

	Math::Vector3 movement;

	if (is->IsKeyDown(Keys::W))
	{
		movement.z = cameraMoveSpeed;
	}
	else if (is->IsKeyDown(Keys::S))
	{
		movement.z = -cameraMoveSpeed;
	}

	if (is->IsKeyDown(Keys::D))
	{
		movement.x = cameraMoveSpeed;
	}
	else if (is->IsKeyDown(Keys::A))
	{
		movement.x = -cameraMoveSpeed;
	}

	if (is->IsKeyDown(Keys::Q))
	{
		cameraRiseValue = -cameraSpeedValue;
	}
	else if (is->IsKeyDown(Keys::E))
	{
		cameraRiseValue = cameraSpeedValue;
	}
	else
	{
		cameraRiseValue = 0.0f;
	}

	if (is->IsKeyPressed(Keys::ONE))
	{
		mCurrentCamera = &mCamera;
		mCurrentCameraTransform = &mCameraTransform;
	}

	if (is->IsKeyPressed(Keys::TWO))
	{
		mCurrentCamera = &mLightCamera;
		mCurrentCameraTransform = &mLightCameraTransform;
	}

	if (is->IsKeyDown(Keys::LSHIFT))
	{
		movement *= cameraSpeedValue;
		cameraRiseValue *= cameraSpeedValue;
	}

	if (is->IsKeyPressed(Keys::R))
	{
		mRenderTargetVS.Initialize(L"../Assets/Shaders/PostProcessing.fx", Graphics::Vertex::Format);
		mRenderTargetPS.Initialize(L"../Assets/Shaders/PostProcessing.fx");
	}

	if (is->IsKeyPressed(Keys::Z))
	{
		mRenderTargetVS.Initialize(L"../Assets/Shaders/Contrast.fx", Graphics::Vertex::Format);
		mRenderTargetPS.Initialize(L"../Assets/Shaders/Contrast.fx");
	}

	if (is->IsKeyPressed(Keys::X))
	{
		mRenderTargetVS.Initialize(L"../Assets/Shaders/Dissolve.fx", Graphics::Vertex::Format);
		mRenderTargetPS.Initialize(L"../Assets/Shaders/Dissolve.fx");
	}

	if (is->IsKeyPressed(Keys::C))
	{
		mRenderTargetVS.Initialize(L"../Assets/Shaders/HDR.fx", Graphics::Vertex::Format);
		mRenderTargetPS.Initialize(L"../Assets/Shaders/HDR.fx");
	}

	if (is->IsKeyPressed(Keys::V))
	{
		mRenderTargetVS.Initialize(L"../Assets/Shaders/InvertColor.fx", Graphics::Vertex::Format);
		mRenderTargetPS.Initialize(L"../Assets/Shaders/InvertColor.fx");
	}

	if (is->IsKeyPressed(Keys::B))
	{
		mRenderTargetVS.Initialize(L"../Assets/Shaders/LowFrequency.fx", Graphics::Vertex::Format);
		mRenderTargetPS.Initialize(L"../Assets/Shaders/LowFrequency.fx");
	}

	if (is->IsKeyPressed(Keys::N))
	{
		mRenderTargetVS.Initialize(L"../Assets/Shaders/NightVision.fx", Graphics::Vertex::Format);
		mRenderTargetPS.Initialize(L"../Assets/Shaders/NightVision.fx");
	}

	Graphics::GraphicsSystem* gs = Graphics::GraphicsSystem::Get();


	mRenderTarget.BeginRender();

	mCurrentCameraTransform->Walk(movement.z * mTimer.GetElapsedTime());
	mCurrentCameraTransform->Strafe(movement.x * mTimer.GetElapsedTime());
	mCurrentCameraTransform->Rise(cameraRiseValue * mTimer.GetElapsedTime());
	//ConstantData terrainData;
	ConstantData planeData;
	ConstantData skyboxData;
	ConstantData quadData;

	// Rendering
	Math::Matrix viewMatrix = mCurrentCamera->GetViewMatrix(*mCurrentCameraTransform);
	Math::Matrix projectionMatrix = mCurrentCamera->GetProjectionMatrix(gs->GetAspectRatio());
	mSampler.BindPS(0);

	// Terrain
	//terrainData.wvp = Math::Transpose(viewMatrix * projectionMatrix);
	//terrainData.light = mLight;
	//terrainData.material = mMaterial;
	//mTerrainVS.Bind();
	//mTerrainPS.Bind();
	//mTerrainTexture.BindPS(0);
	//mTerrainConstantBuffer.Set(terrainData);
	//mTerrainConstantBuffer.BindVS();
	//mTerrainConstantBuffer.BindPS();
	//mTerrain.Render();

	// Plane
	Math::Matrix planeScaling = Math::Matrix::Scaling(5.0f);
	Math::Matrix planeWorldMatrix = planeScaling;
	planeData.wvp = Math::Transpose(planeWorldMatrix * viewMatrix * projectionMatrix);
	mPlaneVS.Bind();
	mPlanePS.Bind();
	mPlaneTexture.BindPS(0);
	mPlaneConstantBuffer.Set(planeData);
	mPlaneConstantBuffer.BindVS();
	mPlaneConstantBuffer.BindPS();
	mPlaneBuffer.Render();

	// Skybox
	skyboxData.wvp = Math::Transpose(Math::Matrix::Identity() * viewMatrix * projectionMatrix);
	mSkyboxVS.Bind();
	mSkyboxPS.Bind();
	mSkyboxTexture.BindPS(0);
	mSkyboxConstantBuffer.Set(skyboxData);
	mSkyboxBuffer.Render();

	//GenerateDepthMap();

	// Draw Mesh
	ConstantData globeData;
	ConstantData moonData;

	// Mesh
	mMeshVS.Bind();
	mMeshPS.Bind();

	// Globe
	Math::Matrix globeRY = Math::Matrix::RotationY(mTimer.GetTotalTime() * 0.25f);
	Math::Matrix globeTran = Math::Matrix::Translation(0.0f, 50.0f, 0.0f);
	Math::Matrix globeWorldMatrix = globeRY * globeTran;
	//globeData.wvp = Math::Transpose(mGlobeTransform.GetWorldMatrix() * viewMatrix * projectionMatrix);
	//globeData.world = Math::Transpose(mGlobeTransform.GetWorldMatrix());
	globeData.wvp = Math::Transpose(globeWorldMatrix * viewMatrix * projectionMatrix);
	globeData.world = Math::Transpose(globeWorldMatrix);
	globeData.viewPosition = mCurrentCameraTransform->GetPosition();
	globeData.light = mLight;
	globeData.material = mMaterial;
	globeData.displacementScale = 2.0f;

	mGlobeTexture.BindPS(0);
	mGlobeSpecular.BindPS(1);
	mGlobeNormal.BindPS(2);
	mGlobeBump.BindVS(3);
	mGlobeConstantBuffer.Set(globeData);
	mGlobeConstantBuffer.BindVS();
	mGlobeConstantBuffer.BindPS();
	mGlobeBuffer.Render();
	mGlobeTransform.Yaw((1.0f * mTimer.GetElapsedTime()));

	// Moon
	Math::Matrix moonRY = Math::Matrix::RotationY(mTimer.GetTotalTime() * 0.1f);
	Math::Matrix moonTran = Math::Matrix::Translation(0.0f, 50.0f, 0.0f);
	Math::Matrix moonOrbit = Math::Matrix::Translation(cos(mTimer.GetTotalTime() * 0.1f) * 40.0f, 0.0f, sin(mTimer.GetTotalTime() * 0.1f) * 40.0f); // Synchronous
	Math::Matrix moonWorldMatrix = moonRY * moonOrbit * moonTran;
	moonData.wvp = Math::Transpose(moonWorldMatrix * viewMatrix * projectionMatrix);
	moonData.world = Math::Transpose(moonWorldMatrix);
	moonData.viewPosition = mCurrentCameraTransform->GetPosition();
	moonData.light = mLight;
	moonData.material = mMaterial;
	moonData.displacementScale = 0.6f;

	mMoonTexture.BindPS(0);
	mMoonSpecular.BindPS(1);
	mMoonNormal.BindPS(2);
	mMoonConstantBuffer.Set(moonData);
	mMoonConstantBuffer.BindVS();
	mMoonConstantBuffer.BindPS();

	mMoonBuffer.Render();

	Graphics::SimpleDraw::Flush(viewMatrix * projectionMatrix);

	mRenderTarget.EndRender();

	gs->BeginRender();

	mRenderTargetVS.Bind();
	mRenderTargetPS.Bind();

	mRenderTarget.BindPS(0);

	mRenderTargetBuffer.Render();

	// Use post-processing shaders
	// Set sampler, constant buufer
	// Draw a quad, quad need position and uv

	mRenderTarget.UnbindPS(0);


	gs->EndRender();
}
