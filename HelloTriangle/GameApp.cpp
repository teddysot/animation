#include "GameApp.h"

#include "Math\Inc\EngineMath.h"

namespace //internal linkage
{

	const Graphics::VertexPC kVertices[] =
	{
		{ Math::Vector3(-0.5f,+0.25f,0.0f), Math::Vector4( 0.3f, 0.2f, 0.0f, 1.0f ) },//0
		{ Math::Vector3(-0.25f,+0.50f,0.0f), Math::Vector4( 0.3f, 0.2f, 0.0f, 1.0f ) },//1
		{ Math::Vector3(+0.0f,+0.50f,0.0f), Math::Vector4( 0.3f, 0.2f, 0.0f, 1.0f ) },//2
		{ Math::Vector3(+0.25f,+0.5f,0.0f), Math::Vector4( 0.3f, 0.2f, 0.0f, 1.0f ) },//3
		{ Math::Vector3(+0.5f,+0.25f,0.0f), Math::Vector4( 0.9f, 0.8f, 0.0f, 1.0f ) },//4
		{ Math::Vector3(+0.5f,-0.25f,0.0f), Math::Vector4( 0.3f, 0.2f, 0.0f, 1.0f ) },//5
		{ Math::Vector3(-0.5f,-0.25f,0.0f), Math::Vector4( 0.4f, 0.3f, 0.0f, 1.0f ) },//6
		{ Math::Vector3(+0.0f,-0.0f,0.0f), Math::Vector4( 0.3f, 0.2f, 0.0f, 1.0f ) },//7

	};

	const int kVertexCount = sizeof(kVertices) / sizeof(kVertices[0]);

	const uint32_t kIndices[] =
	{
		0,1,2,2,3,4,0,4,5,5,6,0,6,5,7,0,2,4
	};

	const int kIndexCount = sizeof(kIndices) / sizeof(kIndices[0]);

}


GameApp::GameApp()
{}

GameApp::~GameApp()
{}

void GameApp::OnInitialize(uint32_t width, uint32_t height)
{
	mWindow.Initialize(GetInstance(), GetAppName(), width, height);
	HookWindow(mWindow.GetWindowHandle());

	Graphics::GraphicsSystem::StaticInitialize(mWindow.GetWindowHandle(), false);
	Input::InputSystem::StaticInitialize(mWindow.GetWindowHandle());

	mMesh.Initialize(kVertices, sizeof(Graphics::VertexPC), kVertexCount, kIndices, kIndexCount);

	mMeshVS.Initialize(L"../Assets/Shaders/DoSomething.fx", Graphics::VertexPC::Format);

	mMeshPS.Initialize(L"../Assets/Shaders/DoSomething.fx");
	


}

void GameApp::OnTerminate()
{

	Graphics::GraphicsSystem::StaticTerminate();
	Input::InputSystem::StaticTerminate();
	
	mMesh.Terminate();

	mMeshVS.Terminate();
	mMeshPS.Terminate();

	UnhookWindow();
	mWindow.Terminate();
}

void GameApp::OnUpdate()
{
	if (mWindow.ProcessMessage())
	{
		Kill();
	}

	Input::InputSystem* is = Input::InputSystem::Get();
	is->Update();

	if (is->IsKeyPressed(Keys::ESCAPE))
	{
		PostQuitMessage(0);
	}

	Graphics::GraphicsSystem* gs = Graphics::GraphicsSystem::Get();
	gs->BeginRender(Math::Vector4::Vector4(1.0f, 0.0f, 1.0f, 1.0f));

	//do render stuff
	mMeshPS.Bind();
	mMeshVS.Bind();
	mMesh.Render();
	


	gs->EndRender();

}
