#ifndef INCLUDED_GAMEAPP
#define INCLUDED_GAMEAPP


#include <Core\Inc\Core.h>
#include <Graphics\Inc\Graphics.h>
#include <Input\Inc\Input.h>

class GameApp : public Core::Application
{
public:
	GameApp();
	~GameApp() override;

private:
	
	void OnInitialize(uint32_t width, uint32_t height) override;
	void OnTerminate() override;
	void OnUpdate() override;
	
	Core::Window mWindow;
	
	
	Graphics::MeshBuffer mMesh;
	
	Graphics::VertexShader mMeshVS;

	Graphics::PixelShader mMeshPS;

};

#endif