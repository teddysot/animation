#ifndef INCLUDED_GAMEAPP
#define INCLUDED_GAMEAPP


#include <Core\Inc\Core.h>
#include <Input\Inc\Input.h>
#include <Graphics\Inc\Graphics.h>
#include <Physics\Inc\Physics.h>

class GameApp : public Core::Application
{
public:
	GameApp();
	~GameApp() override;

private:
	void OnInitialize(uint32_t width, uint32_t height) override;
	void OnTerminate() override;
	void OnUpdate() override;

	Core::Window						mWindow;
	Core::Timer							mTimer;

	Graphics::Camera					mCamera;
	Graphics::Transform					mCameraTransform;

	Physics::World						mPhysicsWorld;

	Graphics::Sampler					mSampler;

	Graphics::AnimatedModel				marineModel;
	Graphics::VertexShader				marineVS;
	Graphics::PixelShader				marinePS;
	bool isStand;
};

#endif