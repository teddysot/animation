#include "GameApp.h"

#include "Math\Inc\EngineMath.h"
#include <fstream>
#include <iostream>

GameApp::GameApp()
{}

GameApp::~GameApp()
{}

namespace
{
	const int kGroundSize = 25;
	const Math::Vector4 kGroundColor(0.5f, 0.5f, 0.5f, 1.0f);

	const int kNumParticles = 10;

	const int kClothSize = 25;
	const Math::Vector3 kClothPos(5.0f, 7.0f, 0.0f);
	const Math::Vector3 kClothSpacing(0.2f, 0.2f, 0.1f);

	void DrawGroundPlane()
	{
		for (int x = -kGroundSize; x <= kGroundSize; ++x)
		{
			Math::Vector3 a((float)x, 0.0f, -(float)kGroundSize);
			Math::Vector3 b((float)x, 0.0f, (float)kGroundSize);
			Graphics::SimpleDraw::DrawLine(a, b, kGroundColor);
		}
		for (int z = -kGroundSize; z <= kGroundSize; ++z)
		{
			Math::Vector3 a(-(float)kGroundSize, 0.0f, (float)z);
			Math::Vector3 b((float)kGroundSize, 0.0f, (float)z);
			Graphics::SimpleDraw::DrawLine(a, b, kGroundColor);
		}
	}

	void DrawOrigin()
	{
		Graphics::SimpleDraw::DrawLine(0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, Math::Vector4::Red());
		Graphics::SimpleDraw::DrawLine(0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, Math::Vector4::Green());
		Graphics::SimpleDraw::DrawLine(0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, Math::Vector4::Blue());
	}

	float RandomF()
	{
		return (float)(rand()) / (float)RAND_MAX;
	}

	float RandomF(float min, float max)
	{
		return min + (RandomF() * (max - min));
	}
}

void GameApp::OnInitialize(uint32_t width, uint32_t height)
{
	mWindow.Initialize(GetInstance(), GetAppName(), width, height);
	HookWindow(mWindow.GetWindowHandle());

	mTimer.Initialize();

	Graphics::GraphicsSystem::StaticInitialize(mWindow.GetWindowHandle(), false);
	Graphics::TextureManager::StaticInitialize();
	Graphics::SimpleDraw::StaticInitialize(100000);
	Input::InputSystem::StaticInitialize(mWindow.GetWindowHandle());

	mCameraTransform.SetPostion(Math::Vector3(0.0f, 5.0f, -10.0f));
	mCameraTransform.SetDirection(Math::Vector3(0.0f, 0.0f, 1.0f));

	mSampler.Initialize(Graphics::Sampler::Filter::Anisotropic, Graphics::Sampler::AddressMode::Clamp);

	// Marine model
	marineVS.Initialize(L"../Assets/Shaders/ShadowMapping2.fx", Graphics::BoneVertex::Format);
	marinePS.Initialize(L"../Assets/Shaders/ShadowMapping2.fx");
	marineModel.Load("../Assets//Models/marine2.txt");
	marineModel.Play(true);

	Physics::Settings settings;
	settings.gravity = Math::Vector3(0.0f, -9.8f, 0.0f);
	settings.timeStep = 1.0f / 60.0f;
	settings.drag = 0.01f;
	mPhysicsWorld.Setup(settings);

	Math::OBB ground;
	ground.center = Math::Vector3(0.0f, 0.0f, 0.0f);
	ground.extend = Math::Vector3((float)kGroundSize, 1.0f, (float)kGroundSize);
	ground.rot = Math::Quaternion();
	//mPhysicsWorld.AddOBB(ground);

	mPhysicsWorld.AddPlane(Math::Plane());
}

void GameApp::OnTerminate()
{
	mPhysicsWorld.ClearDynamic();

	Graphics::GraphicsSystem::StaticTerminate();
	Graphics::TextureManager::StaticTerminate();
	Graphics::SimpleDraw::StaticTerminate();
	Input::InputSystem::StaticTerminate();

	// Marine models
	marineModel.Unload();
	marineVS.Terminate();
	marinePS.Terminate();

	mSampler.Terminate();

	UnhookWindow();
	mWindow.Terminate();
}

void GameApp::OnUpdate()
{
	if (mWindow.ProcessMessage())
	{
		Kill();
	}

	mTimer.Update();

	Input::InputSystem* is = Input::InputSystem::Get();

	is->Update();

	if (GetAsyncKeyState(VK_F1))
	{
		Graphics::GraphicsSystem::Get()->ToggleFullscreen();
	}
	if (is->IsKeyPressed(Keys::ESCAPE))
	{
		PostQuitMessage(0);
	}

	const float cameraMoveSpeed = is->IsKeyDown(Keys::LSHIFT) ? 10.0f : 5.0f;
	const float cameraTurnSpeed = 0.3f;

	if (is->IsKeyDown(Keys::W))
	{
		mCameraTransform.Walk(cameraMoveSpeed * mTimer.GetElapsedTime());
	}
	if (is->IsKeyDown(Keys::S))
	{
		mCameraTransform.Walk(-cameraMoveSpeed * mTimer.GetElapsedTime());
	}
	if (is->IsKeyDown(Keys::D))
	{
		mCameraTransform.Strafe(cameraMoveSpeed * mTimer.GetElapsedTime());
	}
	if (is->IsKeyDown(Keys::A))
	{
		mCameraTransform.Strafe(-cameraMoveSpeed * mTimer.GetElapsedTime());
	}

	mCameraTransform.Yaw(is->GetMouseMoveX() * cameraTurnSpeed * mTimer.GetElapsedTime());
	mCameraTransform.Pitch(is->GetMouseMoveY() * cameraTurnSpeed * mTimer.GetElapsedTime());

	if (is->IsKeyPressed(Keys::ZERO))
	{
		mPhysicsWorld.ClearDynamic();
	}

	if (is->IsKeyDown(Keys::ONE))
	{

		auto p = new Physics::Particle();
		p->SetRadius(0.1f);
		p->SetPosition({ 0.0f, 0.0f, 0.0f });
		p->SetVelocity({
			Math::Random::GetF(-0.1f, 0.1f),
			Math::Random::GetF(0.05f, 0.25f),
			Math::Random::GetF(-0.1f, 0.1f)
		});
		mPhysicsWorld.AddParticle(p);
	}

	if (is->IsKeyDown(Keys::TWO))
	{
		auto p0 = new Physics::Particle();
		p0->SetRadius(0.1f);
		p0->SetPosition({ 0.0f, 0.0f, 0.0f });
		p0->SetVelocity({
			Math::Random::GetF(-0.1f, 0.1f),
			Math::Random::GetF(0.05f, 0.25f),
			Math::Random::GetF(-0.1f, 0.1f)
		});

		auto p1 = new Physics::Particle();
		p1->SetRadius(0.1f);
		p1->SetPosition({ 0.0f, 0.0f, 0.0f });
		p1->SetVelocity({
			Math::Random::GetF(-0.1f, 0.1f),
			Math::Random::GetF(0.05f, 0.25f),
			Math::Random::GetF(-0.1f, 0.1f)
		});

		auto c = new Physics::Spring(p0, p1, 0.5f);

		mPhysicsWorld.AddParticle(p0);
		mPhysicsWorld.AddParticle(p1);
		mPhysicsWorld.AddConstraint(c);
	}

	if (is->IsKeyPressed(Keys::THREE))
	{
		mPhysicsWorld.ClearDynamic();

		int row = 1;
		int col = 4;

		//for (int i = 0; i < row; ++i)
		//{
		/*	for (int j = 0; j < col; ++j)
		{
		std::vector<Physics::Particle*> particles;
		Physics::Particle* p0 = new Physics::Particle();
		Physics::Particle* temp = new Physics::Particle();
		p0->SetRadius(0.1f);
		p0->SetPosition(Math::Vector3(j, 0.5f, 0.0f));
		mPhysicsWorld.AddParticle(p0);
		if (j == 0)
		{
		mPhysicsWorld.AddConstraint(new Physics::Fixed(p0));
		}
		else
		{
		mPhysicsWorld.AddConstraint(new Physics::Spring(temp, p0, 0.5f));
		}
		temp = p0;
		particles.push_back(p0);
		}*/
		//}

		std::vector<Physics::Particle*> particles;
		Physics::Particle* p0 = new Physics::Particle();
		p0->SetRadius(0.1f);
		p0->SetPosition(Math::Vector3(0.0f, 0.5f, 0.0f));
		mPhysicsWorld.AddParticle(p0);
		mPhysicsWorld.AddConstraint(new Physics::Fixed(p0));
		particles.push_back(p0);

		Physics::Particle* p1 = new Physics::Particle();
		p1->SetRadius(0.1f);
		p1->SetPosition(Math::Vector3(0.5f, 0.5f, 0.0f));
		mPhysicsWorld.AddParticle(p1);
		mPhysicsWorld.AddConstraint(new Physics::Spring(p0, p1, 0.5f));
		particles.push_back(p1);

		Physics::Particle* p2 = new Physics::Particle();
		p2->SetRadius(0.1f);
		p2->SetPosition(Math::Vector3(1.0f, 0.5f, 0.0f));
		mPhysicsWorld.AddParticle(p2);
		mPhysicsWorld.AddConstraint(new Physics::Spring(p1, p2, 0.5f));
		particles.push_back(p2);

		Physics::Particle* p3 = new Physics::Particle();
		p3->SetRadius(0.1f);
		p3->SetPosition(Math::Vector3(1.5f, 0.5f, 0.0f));
		mPhysicsWorld.AddParticle(p3);
		mPhysicsWorld.AddConstraint(new Physics::Spring(p2, p3, 0.5f));
		particles.push_back(p3);

	}

	if (is->IsKeyPressed(Keys::FOUR))
	{
		mPhysicsWorld.ClearDynamic();

		std::vector<Physics::Particle*> particles;
		Physics::Particle* p0 = new Physics::Particle();
		p0->SetRadius(0.1f);
		p0->SetPosition(Math::Vector3(0.0f, 0.5f, 0.0f));
		mPhysicsWorld.AddParticle(p0);
		mPhysicsWorld.AddConstraint(new Physics::Fixed(p0));
		particles.push_back(p0);

		Physics::Particle* p1 = new Physics::Particle();
		p1->SetRadius(0.1f);
		p1->SetPosition(Math::Vector3(0.5f, 0.5f, 0.0f));
		mPhysicsWorld.AddParticle(p1);
		mPhysicsWorld.AddConstraint(new Physics::Spring(p0, p1, 0.5f));
		particles.push_back(p1);

		Physics::Particle* p2 = new Physics::Particle();
		p2->SetRadius(0.1f);
		p2->SetPosition(Math::Vector3(1.0f, 0.5f, 0.0f));
		mPhysicsWorld.AddParticle(p2);
		mPhysicsWorld.AddConstraint(new Physics::Spring(p1, p2, 0.5f));
		particles.push_back(p2);

		Physics::Particle* p3 = new Physics::Particle();
		p3->SetRadius(0.1f);
		p3->SetPosition(Math::Vector3(1.5f, 0.5f, 0.0f));
		mPhysicsWorld.AddParticle(p3);
		mPhysicsWorld.AddConstraint(new Physics::Fixed(p3));
		mPhysicsWorld.AddConstraint(new Physics::Spring(p2, p3, 0.5f));
		particles.push_back(p3);
	}

	if (is->IsKeyPressed(Keys::FIVE))
	{
		mPhysicsWorld.ClearDynamic();

		std::vector<Physics::Particle*> particles;
		std::vector<Math::Matrix> mBone = marineModel.GetBoneWorldTransforms();
		for (int i = 0; i < mBone.size(); ++i)
		{
			Math::Vector3 p1(mBone[i]._41 + 1, mBone[i]._42, mBone[i]._43);

			Physics::Particle* p3 = new Physics::Particle();
			p3->SetRadius(0.01f);
			p3->SetPosition(p1);
			p3->SetVelocity({ 0,0,0 });
			particles.push_back(p3);
			mPhysicsWorld.AddParticle(p3);
		}

		for (int i = 0; i < mBone.size(); ++i)
		{
			if (marineModel.mSkeleton[i]->parent != nullptr)
			{
				Math::Vector3 p1(mBone[i]._41 + 1, mBone[i]._42, mBone[i]._43);
				Math::Vector3 p2(mBone[marineModel.mSkeleton[i]->parent->index]._41 + 1, mBone[marineModel.mSkeleton[i]->parent->index]._42, mBone[marineModel.mSkeleton[i]->parent->index]._43);

				float distance = Math::Distance(p1, p2);

				mPhysicsWorld.AddConstraint(new Physics::Spring(particles[i], particles[marineModel.mSkeleton[i]->parent->index], distance));
			}
		}
	}

	if (is->IsKeyDown(Keys::SPACE))
	{
		if (isStand)
		{
			isStand = false;
		}
		else
		{
			isStand = true;
		}
	}

	mPhysicsWorld.Update(mTimer.GetElapsedTime());

	Graphics::GraphicsSystem* gs = Graphics::GraphicsSystem::Get();
	gs->BeginRender();

	DrawGroundPlane();
	DrawOrigin();

	marineVS.Bind();
	marinePS.Bind();

	marineModel.RenderSkeleton();

	mPhysicsWorld.DebugDraw();

	/*for (int z = 0; z <= 100; ++z)
	{
		Math::Vector3 p0(-50.0f, -0.01f, -50.0f + z);
		Math::Vector3 p1(+50.0f, -0.01f, -50.0f + z);
		Graphics::SimpleDraw::DrawLine(p0, p1, Math::Vector4::Gray());
	}

	for (int x = 0; x <= 100; ++x)
	{
		Math::Vector3 p0(-50.0f + x, -0.01f, -50.0f);
		Math::Vector3 p1(-50.0f + x, -0.01f, +50.0f);
		Graphics::SimpleDraw::DrawLine(p0, p1, Math::Vector4::Gray());
	}*/

	Graphics::SimpleDraw::DrawTransform(Math::Matrix::Identity());

	Math::Matrix viewMatrix = mCamera.GetViewMatrix(mCameraTransform);
	Math::Matrix projectionMatrix = mCamera.GetProjectionMatrix(gs->GetAspectRatio());

	Graphics::SimpleDraw::Flush(viewMatrix * projectionMatrix);

	gs->EndRender();
}
