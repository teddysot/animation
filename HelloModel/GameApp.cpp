#include "GameApp.h"

#include "Math\Inc\EngineMath.h"

#include "External\Assimp\include\assimp\cimport.h"
#include "External\Assimp\include\assimp\scene.h"
#include "External\Assimp\include\assimp\postprocess.h"

//#include "material.h"




/*
terrain class
Init(filename)
Terminate()
Draw()


Sphere
terrain

*/

GameApp::GameApp()
{}

GameApp::~GameApp()
{}

void GameApp::OnInitialize(uint32_t width, uint32_t height)
{
	mWindow.Initialize(GetInstance(), GetAppName(), width, height);
	HookWindow(mWindow.GetWindowHandle());

	mTimer.Initialize();


	Graphics::GraphicsSystem::StaticInitialize(mWindow.GetWindowHandle(), false);
	Graphics::SimpleDraw::StaticInitialize(100000);
	Input::InputSystem::StaticInitialize(mWindow.GetWindowHandle());

	mSampler.Initialize(Graphics::Sampler::Filter::Anisotropic, Graphics::Sampler::AddressMode::Clamp);
	mSkyBoxTexture.Initialize(L"../Assets/Images/AnotherSkybox.jpg");



	mCameraTransform.SetPostion(Math::Vector3(0.0f, 2.0f, -10.0f));
	mCameraTransform.SetDirection(Math::Vector3(0.0f, 0.0f, 1.0f));

	mGlobeConstantBuffer.Initialize();
	mMeshVS.Initialize(L"../Assets/Shaders/Texturing.fx", Graphics::Vertex::Format);
	mMeshPS.Initialize(L"../Assets/Shaders/Texturing.fx");

	mSkyboxMesh.Instantiate(100);
	mBuilder.CreateSkybox(10000, mSkyboxMesh);

	mSkyBuffer.SetTopology(Graphics::MeshBuffer::Topology::TriangleList);
	mSkyBuffer.Initialize(mSkyboxMesh);

	mSpiderModel.Initialize("../Assets/Models/duck.fbx", 100000);

	mSpiderTexture.Initialize(L"../Assets/Models/duckTex.jpg");


}

void GameApp::OnTerminate()
{

	Graphics::GraphicsSystem::StaticTerminate();
	Graphics::SimpleDraw::StaticTerminate();
	Input::InputSystem::StaticTerminate();

	mSampler.Terminate();
	mSkyBoxTexture.Terminate();


	mSpiderModel.Terminate();

	mSpiderTexture.Terminate();

	mMeshVS.Terminate();
	mMeshPS.Terminate();
	mGlobeConstantBuffer.Terminate();


	UnhookWindow();
	mWindow.Terminate();
}

void GameApp::OnUpdate()
{
	if (mWindow.ProcessMessage())
	{
		Kill();
	}

	mTimer.Update();

	Input::InputSystem* is = Input::InputSystem::Get();
	is->Update();

	if (is->IsKeyPressed(Keys::ESCAPE))
	{
		PostQuitMessage(0);
	}

	Math::Matrix4 mRotateX;
	Math::Matrix4 mRotateY;
	Math::Matrix4 mRotateZ;

	Graphics::GraphicsSystem* gs = Graphics::GraphicsSystem::Get();
	gs->BeginRender(Math::Vector4::Vector4(0.0f, 0.0f, 0.0f, 0.0f));


	float cameraMoveSpeed = 20.0f;
	float cameraRotationSpeed = 0.3f;
	float cameraSpeedValue = 5.0f;
	float cameraRiseValue = 10.0f;

	if (is->IsMouseDown(1))
	{
		mCameraTransform.Pitch(((float)is->GetMouseMoveY() * cameraRotationSpeed * mTimer.GetElapsedTime()));
		mCameraTransform.Yaw(((float)is->GetMouseMoveX() * cameraRotationSpeed * mTimer.GetElapsedTime()));
	}



	Math::Vector3 movement;

	if (is->IsKeyDown(Keys::W))
	{
		movement.z = cameraMoveSpeed;
	}
	else if (is->IsKeyDown(Keys::S))
	{
		movement.z = -cameraMoveSpeed;
	}

	if (is->IsKeyDown(Keys::D))
	{
		movement.x = cameraMoveSpeed;
	}
	else if (is->IsKeyDown(Keys::A))
	{
		movement.x = -cameraMoveSpeed;
	}

	if (is->IsKeyDown(Keys::Q))
	{
		cameraRiseValue = -cameraSpeedValue;
	}
	else if (is->IsKeyDown(Keys::E))
	{
		cameraRiseValue = cameraSpeedValue;
	}
	else
	{
		cameraRiseValue = 0.0f;
	}

	if (is->IsKeyDown(Keys::LSHIFT))
	{
		movement *= cameraSpeedValue;
		cameraRiseValue *= cameraSpeedValue;
	}


	if (is->IsKeyDown(Keys::NUM_ADD))
	{
		mSphereSize += 0.5f;
	}

	if (is->IsKeyDown(Keys::NUM_SUB))
	{
		if (mSphereSize - 0.5f >= 0)
		{
			mSphereSize -= 0.5f;
		}
	}

	if (is->IsKeyPressed(Keys::NUMPAD7))
	{
		mSphereRings += 2;
	}

	if (is->IsKeyPressed(Keys::NUMPAD4))
	{
		if (mSphereRings - 2 > 0)
		{
			mSphereRings -= 2;
		}
	}

	if (is->IsKeyPressed(Keys::NUMPAD8))
	{
		mSphereSlices += 2;
	}

	if (is->IsKeyPressed(Keys::NUMPAD5))
	{
		if (mSphereSlices - 2 > 0)
		{
			mSphereSlices -= 2;
		}
	}

	if (is->IsKeyPressed(Keys::NUMPAD9))
	{
		mSphereSlices += 2;
		mSphereRings += 2;

	}

	if (is->IsKeyPressed(Keys::NUMPAD6))
	{
		if (mSphereSlices - 2 > 0)
		{
			mSphereSlices -= 2;
		}
		if (mSphereRings - 2 > 0)
		{
			mSphereRings -= 2;
		}
	}


	mCameraTransform.Walk(movement.z * mTimer.GetElapsedTime());
	mCameraTransform.Strafe(movement.x * mTimer.GetElapsedTime());
	mCameraTransform.Rise(cameraRiseValue * mTimer.GetElapsedTime());



	//stay the same
	Math::Matrix4 viewMatrix = mCamera.GetViewMatrix(mCameraTransform);
	Math::Matrix4 projectionMatrix = mCamera.GetProjectionMatrix(gs->GetAspectRatio());
	mMeshVS.Bind();
	mMeshPS.Bind();
	mGlobeConstantBuffer.BindVS();
	mGlobeConstantBuffer.BindPS();
	mSampler.BindPS(0);
	ConstantData data;
	//



	//RENDER STUFF

	data.wvp = Math::Matrix4::Transpose(Math::Matrix4::Identity() * viewMatrix * projectionMatrix);
	mGlobeConstantBuffer.Set(data);

	mSkyBoxTexture.BindVS(0);
	mSkyBoxTexture.BindPS(0);
	mSkyBuffer.Render();

	mSpiderTexture.BindVS(0);
	mSpiderTexture.BindPS(0);
	mSpiderModel.Render();

	/*data.wvp = Math::Matrix4::Transpose(Math::Matrix4::Identity() * viewMatrix * projectionMatrix);
	mConstantBuffer.Set(data);*/

	/*for (int i = 0; i < mSpiderMeshSize; i++)
	{
		mSpiderBuffer[i].Render();
	}*/

	Graphics::SimpleDraw::Flush(viewMatrix * projectionMatrix);


	gs->EndRender();

}
