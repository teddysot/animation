#ifndef INCLUDED_GAMEAPP
#define INCLUDED_GAMEAPP


#include <Core\Inc\Core.h>
#include <Graphics\Inc\Graphics.h>
#include <Input\Inc\Input.h>

class GameApp : public Core::Application
{
public:
	GameApp();
	~GameApp() override;

private:

	struct Material //TODO:: add to graphics (Graphics::Material)
	{
		Material()
			: ambient(Math::Vector4::White())
			, diffuse(Math::Vector4::White())
			, specular(Math::Vector4::White())
			, power(1.0f)
		{}

		Math::Vector4 ambient;
		Math::Vector4 diffuse;
		Math::Vector4 specular;
		float power;
	};


	struct ConstantData
	{
		Math::Matrix wvp;
		Math::Matrix world;
		Math::Vector3 viewPosition;
		float padding;
		Graphics::Light light;
		Material material;
	};


	void OnInitialize(uint32_t width, uint32_t height) override;
	void OnTerminate() override;
	void OnUpdate() override;

	Core::Window mWindow;
	Core::Timer mTimer;

	Graphics::Camera mCamera;
	Graphics::Transform mCameraTransform;

	Graphics::Light mLight;
	Material mMaterial;

	Graphics::TypedConstantBuffer<ConstantData> mGlobeConstantBuffer;
	Graphics::VertexShader mMeshVS;
	Graphics::PixelShader mMeshPS;

	Graphics::Sampler mSampler;

	Graphics::Model duckModel;

	Graphics::Texture mGlobeDiffuse;
	Graphics::Texture mGlobeSpecular;
	Graphics::Texture mGlobeDarkSide;
	Graphics::Texture mGlobeNormal;
	Graphics::Texture mGlobeBumpmap;
	Graphics::Mesh mGlobeMesh;
	Graphics::MeshBuffer mGlobeBuffer; //make it mesh buffer later
	Graphics::Transform mGlobeTransform;

	Graphics::MeshBuilder mBuilder;


	float mSphereSize = 1.0f;
	float mSphereRings = 3.0f;
	float mSphereSlices = 3.0f;

};

#endif