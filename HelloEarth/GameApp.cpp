#include "GameApp.h"

#include "Math\Inc\EngineMath.h"

/*
terrain class
Init(filename)
Terminate()
Draw()


Sphere
terrain

*/

GameApp::GameApp()
{}

GameApp::~GameApp()
{}

void GameApp::OnInitialize(uint32_t width, uint32_t height)
{
	mWindow.Initialize(GetInstance(), GetAppName(), width, height);
	HookWindow(mWindow.GetWindowHandle());

	mTimer.Initialize();


	Graphics::GraphicsSystem::StaticInitialize(mWindow.GetWindowHandle(), false);
	Graphics::SimpleDraw::StaticInitialize(100000);
	Input::InputSystem::StaticInitialize(mWindow.GetWindowHandle());

	mSampler.Initialize(Graphics::Sampler::Filter::Anisotropic, Graphics::Sampler::AddressMode::Clamp);
	
	mGlobeDiffuse.Initialize(L"../Assets/Images/earth.jpg");
	mGlobeSpecular.Initialize(L"../Assets/Images/earth_spec.jpg");
	mGlobeDarkSide.Initialize(L"../Assets/Images/earth_lights.jpg");
	mGlobeNormal.Initialize(L"../Assets/Images/earth_normal.jpg");
	mGlobeBumpmap.Initialize(L"../Assets/Images/earth_bump.jpg");

	//mModel.Initialize("../Assets/Models/box.obj", 10000);

	mCameraTransform.SetPostion(Math::Vector3(0.0f, 2.0f, -10.0f));
	mCameraTransform.SetDirection(Math::Vector3(0.0f, 0.0f, 1.0f));

	mLight.direction = Math::Normalize(Math::Vector3(1.0f, -1.0f, 1.0f));
	mLight.ambient = Math::Vector4(0.5f, 0.5f, 0.5f, 1.0f);
	mLight.diffuse = Math::Vector4(2.0f, 2.0f, 2.0f, 0.5f);
	mLight.specular = Math::Vector4(0.3f, 0.3f, 0.3f, 1.0f);

	mMaterial.ambient = Math::Vector4(0.5f, 0.5f, 0.5f, 1.0f);
	mMaterial.diffuse = Math::Vector4(0.7f, 0.7f, 0.7f, 1.0f);
	mMaterial.specular = Math::Vector4(0.3f, 0.3f, 0.3f, 1.0f);
	mMaterial.power = 15;

	mGlobeConstantBuffer.Initialize();
	mMeshVS.Initialize(L"../Assets/Shaders/DayAndNight.fx", Graphics::Vertex::Format);
	mMeshPS.Initialize(L"../Assets/Shaders/DayAndNight.fx");

	mGlobeMesh.Instantiate(100000);
	mBuilder.CreateSphere(3.0, 60, 60, mGlobeMesh);

	//mBuilder.CreatePlane(Math::Vector2(10, 10), mGlobeMesh);

	mGlobeBuffer.SetTopology(Graphics::MeshBuffer::Topology::TriangleList);
	mGlobeBuffer.Initialize(mGlobeMesh);

	mGlobeTransform.Pitch(Math::kDegToRad * -90.0f);
	mGlobeTransform.Rise(10.0f);
	mGlobeTransform.ScaleBy(10);
}



void GameApp::OnTerminate()
{

	Graphics::GraphicsSystem::StaticTerminate();
	Graphics::SimpleDraw::StaticTerminate();
	Input::InputSystem::StaticTerminate();

	mSampler.Terminate();
	mGlobeDiffuse.Terminate();
	mGlobeSpecular.Terminate();
	mGlobeDarkSide.Terminate();
	mGlobeNormal.Terminate();
	mGlobeBumpmap.Terminate();

	//mModel.Terminate();

	mGlobeMesh.Terminate();

	mMeshVS.Terminate();
	mMeshPS.Terminate();
	mGlobeConstantBuffer.Terminate();

	UnhookWindow();
	mWindow.Terminate();
}

void GameApp::OnUpdate()
{
	if (mWindow.ProcessMessage())
	{
		Kill();
	}

	mTimer.Update();

	Input::InputSystem* is = Input::InputSystem::Get();
	is->Update();

	if (is->IsKeyPressed(Keys::ESCAPE))
	{
		PostQuitMessage(0);
	}

	Math::Matrix mRotateX;
	Math::Matrix mRotateY;
	Math::Matrix mRotateZ;

	Graphics::GraphicsSystem* gs = Graphics::GraphicsSystem::Get();
	gs->BeginRender(Math::Vector4::Vector4(0.0f, 0.0f, 0.0f, 0.0f));


	float cameraMoveSpeed = 20.0f;
	float cameraRotationSpeed = 0.3f;
	float cameraSpeedValue = 10.0f;
	float cameraRiseValue = 10.0f;

	if (is->IsMouseDown(1))
	{
		mCameraTransform.Pitch(((float)is->GetMouseMoveY() * cameraRotationSpeed * mTimer.GetElapsedTime()));
		mCameraTransform.Yaw(((float)is->GetMouseMoveX() * cameraRotationSpeed * mTimer.GetElapsedTime()));
	}

	Math::Vector3 movement;

	if (is->IsKeyDown(Keys::W))
	{
		movement.z = cameraMoveSpeed;
	}
	else if (is->IsKeyDown(Keys::S))
	{
		movement.z = -cameraMoveSpeed;
	}

	if (is->IsKeyDown(Keys::D))
	{
		movement.x = cameraMoveSpeed;
	}
	else if (is->IsKeyDown(Keys::A))
	{
		movement.x = -cameraMoveSpeed;
	}

	if (is->IsKeyDown(Keys::Q))
	{
		cameraRiseValue = -cameraSpeedValue;
	}
	else if (is->IsKeyDown(Keys::E))
	{
		cameraRiseValue = cameraSpeedValue;
	}
	else
	{
		cameraRiseValue = 0.0f;
	}

	if (is->IsKeyDown(Keys::LSHIFT))
	{
		movement *= cameraSpeedValue;
		cameraRiseValue *= cameraSpeedValue;
	}


	if (is->IsKeyDown(Keys::NUM_ADD))
	{
		mSphereSize += 0.5f;
	}

	if (is->IsKeyDown(Keys::NUM_SUB))
	{
		if (mSphereSize - 0.5f >= 0)
		{
			mSphereSize -= 0.5f;
		}
	}

	if (is->IsKeyPressed(Keys::NUMPAD7))
	{
		mSphereRings += 2;
	}

	if (is->IsKeyPressed(Keys::NUMPAD4))
	{
		if (mSphereRings - 2 > 0)
		{
			mSphereRings -= 2;
		}
	}

	if (is->IsKeyPressed(Keys::NUMPAD8))
	{
		mSphereSlices += 2;
	}

	if (is->IsKeyPressed(Keys::NUMPAD5))
	{
		if (mSphereSlices - 2 > 0)
		{
			mSphereSlices -= 2;
		}
	}

	if (is->IsKeyPressed(Keys::NUMPAD9))
	{
		mSphereSlices += 2;
		mSphereRings += 2;

	}

	if (is->IsKeyPressed(Keys::NUMPAD6))
	{
		if (mSphereSlices - 2 > 0)
		{
			mSphereSlices -= 2;
		}
		if (mSphereRings - 2 > 0)
		{
			mSphereRings -= 2;
		}
	}


	mCameraTransform.Walk(movement.z * mTimer.GetElapsedTime());
	mCameraTransform.Strafe(movement.x * mTimer.GetElapsedTime());
	mCameraTransform.Rise(cameraRiseValue * mTimer.GetElapsedTime());



	//stay the same
	Math::Matrix viewMatrix = mCamera.GetViewMatrix(mCameraTransform);
	Math::Matrix projectionMatrix = mCamera.GetProjectionMatrix(gs->GetAspectRatio());
	mMeshVS.Bind();
	mMeshPS.Bind();
	mGlobeConstantBuffer.BindVS();
	mGlobeConstantBuffer.BindPS();
	mSampler.BindPS(0);
	ConstantData data;
	//


	//RENDER STUFF


	//Globe
	mGlobeTransform.Yaw((0.5f * mTimer.GetElapsedTime()));
	mGlobeDiffuse.BindPS(0);
	mGlobeSpecular.BindPS(1);
	mGlobeDarkSide.BindPS(2);
	mGlobeNormal.BindPS(3);
	mGlobeBumpmap.BindPS(4);
	
	data.wvp = Math::Transpose(mGlobeTransform.GetWorldMatrix() * viewMatrix * projectionMatrix);
	
	data.world = Math::Transpose(mGlobeTransform.GetWorldMatrix());
	
	data.viewPosition = mCameraTransform.GetPosition();
	
	data.light = mLight;
	data.material = mMaterial;
	

	
	mGlobeConstantBuffer.Set(data);
	duckModel.Render();
	mGlobeBuffer.Render();


	Graphics::SimpleDraw::Flush(viewMatrix * projectionMatrix);


	gs->EndRender();

}
