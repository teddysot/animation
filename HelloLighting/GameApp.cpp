#include "GameApp.h"

#include "Math\Inc\EngineMath.h"
#include <fstream>
#include <iostream>
/*
Update Vertex to contain tangent
Update Mesh to generate tangent
Load/Bind normal map
In Pixel Shader, build Tangent Binormal Normal (TBN), convert RGB => XYZ,
replace normal with XYZ
*/
/*
terrain class
Init(filename)
Terminate()
Draw()


Sphere
terrain

*/

GameApp::GameApp()
{}

GameApp::~GameApp()
{}

void GameApp::OnInitialize(uint32_t width, uint32_t height)
{
	mWindow.Initialize(GetInstance(), GetAppName(), width, height);
	HookWindow(mWindow.GetWindowHandle());

	mTimer.Initialize();


	Graphics::GraphicsSystem::StaticInitialize(mWindow.GetWindowHandle(), false);
	Graphics::SimpleDraw::StaticInitialize(100000);
	Input::InputSystem::StaticInitialize(mWindow.GetWindowHandle());

	mCameraTransform.SetPostion(Math::Vector3(0.0f, 30.0f, -10.0f));
	mCameraTransform.SetDirection(Math::Vector3(0.0f, 0.0f, 1.0f));

	mLight.direction = Math::Normalize(Math::Vector3(1.0f, -1.0f, 1.0f));
	mLight.ambient = Math::Vector4(0.1f, 0.1f, 0.1f, 1.0f);
	mLight.diffuse = Math::Vector4(2.0f, 2.0f, 2.0f, 1.0f);
	mLight.specular = Math::Vector4(1.0f, 1.0f, 1.0f, 1.0f);

	mMaterial.ambient = Math::Vector4(1.0f, 1.0f, 1.0f, 1.0f);
	mMaterial.diffuse = Math::Vector4(0.5f, 0.5f, 0.5f, 1.0f);
	mMaterial.specular = Math::Vector4(0.5f, 0.5f, 0.5f, 1.0f);
	mMaterial.power = 20;

	mSampler.Initialize(Graphics::Sampler::Filter::Anisotropic, Graphics::Sampler::AddressMode::Clamp);

	// Terrain
	mTerrainVS.Initialize(L"../Assets/Shaders/Terrain.fx", Graphics::Vertex::Format);
	mTerrainPS.Initialize(L"../Assets/Shaders/Terrain.fx");

	mTerrainConstantBuffer.Initialize();
	mTerrainTexture.Initialize(L"../Assets/Images/RockySeamless.jpg");
	mTerrain.Initialize("../Assets/HeightMaps/TestMap.raw", Math::Vector3(1.0f, 0.1f, 1.0f));
	//mTerrainSpecular.Initialize(L"../Assets/Images/RockySeamless_spec.jpg");

	// Skybox
	mSkyboxVS.Initialize(L"../Assets/Shaders/Skybox.fx", Graphics::Vertex::Format);
	mSkyboxPS.Initialize(L"../Assets/Shaders/Skybox.fx");

	mSkyboxConstantBuffer.Initialize();
	mSkyboxTexture.Initialize(L"../Assets/Images/space_cube.dds");
	mSkyboxMesh.Instantiate(100);
	//mBuilder.CreateCube(Math::Vector3(100, 100, 100), mSkyboxMesh);
	mBuilder.CreateSkybox(100, mSkyboxMesh);
	mSkyboxBuffer.SetTopology(Graphics::MeshBuffer::Topology::TriangleList);
	mSkyboxBuffer.Initialize(mSkyboxMesh);

	// Mesh Shader
	mMeshVS.Initialize(L"../Assets/Shaders/Standard.fx", Graphics::Vertex::Format);
	mMeshPS.Initialize(L"../Assets/Shaders/Standard.fx");

	// Globe
	mGlobeConstantBuffer.Initialize();
	mGlobeDiffuse.Initialize(L"../Assets/Images/earth.jpg");
	mGlobeSpecular.Initialize(L"../Assets/Images/earth_spec.jpg");
	mGlobeNormal.Initialize(L"../Assets/Images/earth_normal.jpg");
	mGlobeBump.Initialize(L"../Assets/Images/earth_bump.jpg");
	mGlobeMesh.Instantiate(10000);
	mBuilder.CreateSphere(20.0f, 40, 40, mGlobeMesh);
	mGlobeBuffer.SetTopology(Graphics::MeshBuffer::Topology::TriangleList);
	mGlobeBuffer.Initialize(mGlobeMesh);

	// Moon
	mMoonConstantBuffer.Initialize();
	mMoonDiffuse.Initialize(L"../Assets/Images/moon.jpg");
	mMoonNormal.Initialize(L"../Assets/Images/moon_normal.jpg");
	mMoonMesh.Instantiate(10000);
	mBuilder.CreateSphere(5.0f, 40, 40, mMoonMesh);
	mMoonBuffer.SetTopology(Graphics::MeshBuffer::Topology::TriangleList);
	mMoonBuffer.Initialize(mMoonMesh);
}

void GameApp::OnTerminate()
{

	Graphics::GraphicsSystem::StaticTerminate();
	Graphics::SimpleDraw::StaticTerminate();
	Input::InputSystem::StaticTerminate();

	// Terrain
	mTerrainTexture.Terminate();
	mTerrainSpecular.Terminate();
	mTerrain.Terminate();

	// Globe
	mGlobeDiffuse.Terminate();
	mGlobeSpecular.Terminate();
	mGlobeNormal.Terminate();
	mGlobeBump.Terminate();
	mGlobeMesh.Terminate();

	// Moon
	mMoonDiffuse.Terminate();
	mMoonNormal.Terminate();
	mMoonMesh.Terminate();

	// Skybox
	mSkyboxTexture.Terminate();

	// Shader
	mSampler.Terminate();
	mMeshVS.Terminate();
	mMeshPS.Terminate();
	mTerrainVS.Terminate();
	mTerrainPS.Terminate();
	mSkyboxVS.Terminate();
	mSkyboxPS.Terminate();
	mGlobeConstantBuffer.Terminate();
	mMoonConstantBuffer.Terminate();
	mSkyboxConstantBuffer.Terminate();
	mTerrainConstantBuffer.Terminate();

	UnhookWindow();
	mWindow.Terminate();
}

void GameApp::OnUpdate()
{
	if (mWindow.ProcessMessage())
	{
		Kill();
	}

	mTimer.Update();

	Input::InputSystem* is = Input::InputSystem::Get();
	is->Update();

	if (is->IsKeyPressed(Keys::ESCAPE))
	{
		PostQuitMessage(0);
	}

	float cameraMoveSpeed = 20.0f;
	float cameraRotationSpeed = 0.3f;
	float cameraSpeedValue = 10.0f;
	float cameraRiseValue = 10.0f;

	if (is->IsMouseDown(1))
	{
		mCameraTransform.Pitch(((float)is->GetMouseMoveY() * cameraRotationSpeed * mTimer.GetElapsedTime()));
		mCameraTransform.Yaw(((float)is->GetMouseMoveX() * cameraRotationSpeed * mTimer.GetElapsedTime()));
	}

	Math::Vector3 movement;

	if (is->IsKeyDown(Keys::W))
	{
		movement.z = cameraMoveSpeed;
	}
	else if (is->IsKeyDown(Keys::S))
	{
		movement.z = -cameraMoveSpeed;
	}

	if (is->IsKeyDown(Keys::D))
	{
		movement.x = cameraMoveSpeed;
	}
	else if (is->IsKeyDown(Keys::A))
	{
		movement.x = -cameraMoveSpeed;
	}

	if (is->IsKeyDown(Keys::Q))
	{
		cameraRiseValue = -cameraSpeedValue;
	}
	else if (is->IsKeyDown(Keys::E))
	{
		cameraRiseValue = cameraSpeedValue;
	}
	else
	{
		cameraRiseValue = 0.0f;
	}

	if (is->IsKeyDown(Keys::LSHIFT))
	{
		movement *= cameraSpeedValue;
		cameraRiseValue *= cameraSpeedValue;
	}


	Graphics::GraphicsSystem* gs = Graphics::GraphicsSystem::Get();
	gs->BeginRender();
	mCameraTransform.Walk(movement.z * mTimer.GetElapsedTime());
	mCameraTransform.Strafe(movement.x * mTimer.GetElapsedTime());
	mCameraTransform.Rise(cameraRiseValue * mTimer.GetElapsedTime());
	ConstantData terrainData;
	ConstantData skyboxData;
	ConstantData globeData;
	ConstantData moonData;

	// Rendering
	Math::Matrix viewMatrix = mCamera.GetViewMatrix(mCameraTransform);
	Math::Matrix projectionMatrix = mCamera.GetProjectionMatrix(gs->GetAspectRatio());
	mSampler.BindPS(0);

	// Terrain
	terrainData.wvp = Math::Transpose(viewMatrix * projectionMatrix);
	//terrainData.light = mLight;
	//terrainData.material = mMaterial;
	mTerrainVS.Bind();
	mTerrainPS.Bind();
	mTerrainTexture.BindPS(0);
	mTerrainConstantBuffer.Set(terrainData);
	mTerrainConstantBuffer.BindVS();
	mTerrainConstantBuffer.BindPS();

	mTerrain.Render();

	// Skybox
	skyboxData.wvp = Math::Transpose(Math::Matrix::Identity() * viewMatrix * projectionMatrix);
	mSkyboxVS.Bind();
	mSkyboxPS.Bind();
	mSkyboxTexture.BindPS(0);
	mSkyboxConstantBuffer.Set(skyboxData);
	mSkyboxBuffer.Render();

	// Mesh
	mMeshVS.Bind();
	mMeshPS.Bind();

	// Globe
	Math::Matrix globeRY = Math::Matrix::RotationY(mTimer.GetTotalTime() * 0.25f);
	Math::Matrix globeTran = Math::Matrix::Translation(0.0f, 50.0f, 0.0f);
	Math::Matrix globeWorldMatrix = globeRY * globeTran;
	//globeData.wvp = Math::Transpose(mGlobeTransform.GetWorldMatrix() * viewMatrix * projectionMatrix);
	//globeData.world = Math::Transpose(mGlobeTransform.GetWorldMatrix());
	globeData.wvp = Math::Transpose(globeWorldMatrix * viewMatrix * projectionMatrix);
	globeData.world = Math::Transpose(globeWorldMatrix);
	globeData.viewPosition = mCameraTransform.GetPosition();
	globeData.light = mLight;
	globeData.material = mMaterial;
	globeData.displacementScale = 2.0f;

	mGlobeDiffuse.BindPS(0);
	mGlobeSpecular.BindPS(1);
	mGlobeNormal.BindPS(2);
	mGlobeBump.BindVS(3);
	mGlobeConstantBuffer.Set(globeData);
	mGlobeConstantBuffer.BindVS();
	mGlobeConstantBuffer.BindPS();

	mGlobeBuffer.Render();
	mGlobeTransform.Yaw((1.0f * mTimer.GetElapsedTime()));

	// Moon
	Math::Matrix moonRY = Math::Matrix::RotationY(mTimer.GetTotalTime() * 0.1f);
	Math::Matrix moonTran = Math::Matrix::Translation(0.0f, 50.0f, 0.0f);
	Math::Matrix moonOrbit = Math::Matrix::Translation(cos(mTimer.GetTotalTime() * 0.1f) * 40.0f, 0.0f, sin(mTimer.GetTotalTime() * 0.1f) * 40.0f); // Synchronous
	Math::Matrix moonWorldMatrix = moonRY * moonOrbit * moonTran;
	moonData.wvp = Math::Transpose(moonWorldMatrix * viewMatrix * projectionMatrix);
	moonData.world = Math::Transpose(moonWorldMatrix);
	moonData.viewPosition = mCameraTransform.GetPosition();
	moonData.light = mLight;
	moonData.material = mMaterial;
	moonData.displacementScale = 0.6f;

	mMoonDiffuse.BindPS(0);
	mMoonSpecular.BindPS(1);
	mMoonNormal.BindPS(2);
	mMoonConstantBuffer.Set(moonData);
	mMoonConstantBuffer.BindVS();
	mMoonConstantBuffer.BindPS();

	mMoonBuffer.Render();

	Graphics::SimpleDraw::Flush(viewMatrix * projectionMatrix);


	gs->EndRender();

}
